#!/bin/bash

yarn proto-loader-gen-types --grpcLib=@grpc/grpc-js --outDir=proto/ proto/pando/api/*.proto
yarn proto-loader-gen-types --grpcLib=@grpc/grpc-js --outDir=proto/ proto/google/api/*.proto
yarn proto-loader-gen-types --grpcLib=@grpc/grpc-js --outDir=proto/ proto/pando/api/esign/v1/*.proto
yarn proto-loader-gen-types --grpcLib=@grpc/grpc-js --outDir=proto/ proto/pando/api/survey/v1/*.proto
yarn proto-loader-gen-types --grpcLib=@grpc/grpc-js --outDir=proto/ proto/pando/api/vault/v1/*.proto
yarn proto-loader-gen-types --grpcLib=@grpc/grpc-js --outDir=proto/ proto/pando/api/template/v1/*.proto