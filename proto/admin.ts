import type * as grpc from '@grpc/grpc-js';
import type { EnumTypeDefinition, MessageTypeDefinition } from '@grpc/proto-loader';

import type { SurveyAdminServiceClient as _pando_api_survey_v1_SurveyAdminServiceClient, SurveyAdminServiceDefinition as _pando_api_survey_v1_SurveyAdminServiceDefinition } from './pando/api/survey/v1/SurveyAdminService';

type SubtypeConstructor<Constructor extends new (...args: any) => any, Subtype> = {
  new(...args: ConstructorParameters<Constructor>): Subtype;
};

export interface ProtoGrpcType {
  google: {
    api: {
      CustomHttpPattern: MessageTypeDefinition
      Http: MessageTypeDefinition
      HttpRule: MessageTypeDefinition
    }
    protobuf: {
      DescriptorProto: MessageTypeDefinition
      EnumDescriptorProto: MessageTypeDefinition
      EnumOptions: MessageTypeDefinition
      EnumValueDescriptorProto: MessageTypeDefinition
      EnumValueOptions: MessageTypeDefinition
      FieldDescriptorProto: MessageTypeDefinition
      FieldMask: MessageTypeDefinition
      FieldOptions: MessageTypeDefinition
      FileDescriptorProto: MessageTypeDefinition
      FileDescriptorSet: MessageTypeDefinition
      FileOptions: MessageTypeDefinition
      GeneratedCodeInfo: MessageTypeDefinition
      MessageOptions: MessageTypeDefinition
      MethodDescriptorProto: MessageTypeDefinition
      MethodOptions: MessageTypeDefinition
      OneofDescriptorProto: MessageTypeDefinition
      OneofOptions: MessageTypeDefinition
      ServiceDescriptorProto: MessageTypeDefinition
      ServiceOptions: MessageTypeDefinition
      SourceCodeInfo: MessageTypeDefinition
      Timestamp: MessageTypeDefinition
      UninterpretedOption: MessageTypeDefinition
    }
  }
  pando: {
    api: {
      Chunk: MessageTypeDefinition
      ClaimMetadata: MessageTypeDefinition
      ContactInfo: MessageTypeDefinition
      ContactType: EnumTypeDefinition
      DataRecord: MessageTypeDefinition
      DecimalValue: MessageTypeDefinition
      Empty: MessageTypeDefinition
      FileChunk: MessageTypeDefinition
      FileMetadata: MessageTypeDefinition
      FileType: EnumTypeDefinition
      MaskType: EnumTypeDefinition
      MessageMetadata: MessageTypeDefinition
      PaginationResult: MessageTypeDefinition
      ScopeMetadata: MessageTypeDefinition
      ServiceMetadata: MessageTypeDefinition
      UserContactInfo: MessageTypeDefinition
      UserMetadata: MessageTypeDefinition
      VariableType: EnumTypeDefinition
      survey: {
        v1: {
          Action: EnumTypeDefinition
          Answer: MessageTypeDefinition
          AnswerAction: EnumTypeDefinition
          AnswerMediaMetadata: MessageTypeDefinition
          AnswerMediaUploadRequest: MessageTypeDefinition
          AnswerOption: MessageTypeDefinition
          AnswerOptionList: MessageTypeDefinition
          AnswerTimestamp: MessageTypeDefinition
          AnswerType: EnumTypeDefinition
          Attempt: MessageTypeDefinition
          AttemptList: MessageTypeDefinition
          CompletionType: EnumTypeDefinition
          CreateInstanceRequest: MessageTypeDefinition
          CreateSurveyVersionRequest: MessageTypeDefinition
          CreateVariablesRequest: MessageTypeDefinition
          DeleteAnswerOptionRequest: MessageTypeDefinition
          DeleteInstanceRequest: MessageTypeDefinition
          DeleteMediaRequest: MessageTypeDefinition
          DeletePlaybackDataRequest: MessageTypeDefinition
          DeleteQuestionRequest: MessageTypeDefinition
          DeleteSurveyMediaRequest: MessageTypeDefinition
          DeleteSurveyQuestionDestinationRequest: MessageTypeDefinition
          DeleteSurveyRequest: MessageTypeDefinition
          DeleteSurveyVariableRequest: MessageTypeDefinition
          DeleteSurveyVersionRequest: MessageTypeDefinition
          DeleteVariableMediaRequest: MessageTypeDefinition
          DeleteVariableRequest: MessageTypeDefinition
          GetAnswerOptionRequest: MessageTypeDefinition
          GetAnswerOptionsRequest: MessageTypeDefinition
          GetPlaybackDataListRequest: MessageTypeDefinition
          GetQuestionRequest: MessageTypeDefinition
          GetQuestionsRequest: MessageTypeDefinition
          GetSurveyDocumentsRequest: MessageTypeDefinition
          GetSurveyMediaListRequest: MessageTypeDefinition
          GetSurveyQuestionDestinationRequest: MessageTypeDefinition
          GetSurveyQuestionDestinationsRequest: MessageTypeDefinition
          GetSurveyRequest: MessageTypeDefinition
          GetSurveyVariablesRequest: MessageTypeDefinition
          GetSurveyVersionsRequest: MessageTypeDefinition
          GetSurveysRequest: MessageTypeDefinition
          GetVariableMediaListRequest: MessageTypeDefinition
          GetVariableRequest: MessageTypeDefinition
          GetVariableTemplateRequest: MessageTypeDefinition
          GetVariableTemplateResponse: MessageTypeDefinition
          GetVariablesRequest: MessageTypeDefinition
          Instance: MessageTypeDefinition
          InstanceList: MessageTypeDefinition
          Media: MessageTypeDefinition
          MediaInfo: MessageTypeDefinition
          MediaList: MessageTypeDefinition
          MediaLocation: EnumTypeDefinition
          PlaybackData: MessageTypeDefinition
          PlaybackDataList: MessageTypeDefinition
          Question: MessageTypeDefinition
          QuestionWithPlaybackData: MessageTypeDefinition
          SelectionType: EnumTypeDefinition
          SendInstanceLink: MessageTypeDefinition
          Status: EnumTypeDefinition
          Survey: MessageTypeDefinition
          SurveyAdminService: SubtypeConstructor<typeof grpc.Client, _pando_api_survey_v1_SurveyAdminServiceClient> & { service: _pando_api_survey_v1_SurveyAdminServiceDefinition }
          SurveyDetail: MessageTypeDefinition
          SurveyDetailList: MessageTypeDefinition
          SurveyDocumentList: MessageTypeDefinition
          SurveyESignTemplate: MessageTypeDefinition
          SurveyInstanceESignSignatory: MessageTypeDefinition
          SurveyList: MessageTypeDefinition
          SurveyMediaMetadata: MessageTypeDefinition
          SurveyQuestionDestination: MessageTypeDefinition
          SurveyQuestionDestinationList: MessageTypeDefinition
          SurveyQuestionList: MessageTypeDefinition
          SurveyVariable: MessageTypeDefinition
          UpdateAnswerOptionRequest: MessageTypeDefinition
          UpdatePlaybackDataRequest: MessageTypeDefinition
          UpdateQuestionRequest: MessageTypeDefinition
          UpdateSurveyQuestionDestinationRequest: MessageTypeDefinition
          UpdateSurveyRequest: MessageTypeDefinition
          UpdateVariableRequest: MessageTypeDefinition
          UploadSurveyMediaResponse: MessageTypeDefinition
          UploadVariableMediaResponse: MessageTypeDefinition
          Variable: MessageTypeDefinition
          VariableList: MessageTypeDefinition
          Version: MessageTypeDefinition
        }
      }
    }
  }
}

