import type { MessageTypeDefinition } from '@grpc/proto-loader';
export interface ProtoGrpcType {
    google: {
        protobuf: {
            DescriptorProto: MessageTypeDefinition;
            EnumDescriptorProto: MessageTypeDefinition;
            EnumOptions: MessageTypeDefinition;
            EnumValueDescriptorProto: MessageTypeDefinition;
            EnumValueOptions: MessageTypeDefinition;
            FieldDescriptorProto: MessageTypeDefinition;
            FieldOptions: MessageTypeDefinition;
            FileDescriptorProto: MessageTypeDefinition;
            FileDescriptorSet: MessageTypeDefinition;
            FileOptions: MessageTypeDefinition;
            GeneratedCodeInfo: MessageTypeDefinition;
            MessageOptions: MessageTypeDefinition;
            MethodDescriptorProto: MessageTypeDefinition;
            MethodOptions: MessageTypeDefinition;
            OneofDescriptorProto: MessageTypeDefinition;
            OneofOptions: MessageTypeDefinition;
            ServiceDescriptorProto: MessageTypeDefinition;
            ServiceOptions: MessageTypeDefinition;
            SourceCodeInfo: MessageTypeDefinition;
            UninterpretedOption: MessageTypeDefinition;
        };
    };
    pando: {
        api: {
            ClaimMetadata: MessageTypeDefinition;
            MessageMetadata: MessageTypeDefinition;
            ScopeMetadata: MessageTypeDefinition;
            ServiceMetadata: MessageTypeDefinition;
        };
    };
}
