// Original file: proto/google/api/http.proto


export interface CustomHttpPattern {
  'kind'?: (string);
  'path'?: (string);
}

export interface CustomHttpPattern__Output {
  'kind'?: (string);
  'path'?: (string);
}
