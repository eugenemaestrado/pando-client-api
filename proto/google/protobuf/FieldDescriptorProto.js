"use strict";
// Original file: null
Object.defineProperty(exports, "__esModule", { value: true });
exports._google_protobuf_FieldDescriptorProto_Type = exports._google_protobuf_FieldDescriptorProto_Label = void 0;
// Original file: null
var _google_protobuf_FieldDescriptorProto_Label;
(function (_google_protobuf_FieldDescriptorProto_Label) {
    _google_protobuf_FieldDescriptorProto_Label[_google_protobuf_FieldDescriptorProto_Label["LABEL_OPTIONAL"] = 1] = "LABEL_OPTIONAL";
    _google_protobuf_FieldDescriptorProto_Label[_google_protobuf_FieldDescriptorProto_Label["LABEL_REQUIRED"] = 2] = "LABEL_REQUIRED";
    _google_protobuf_FieldDescriptorProto_Label[_google_protobuf_FieldDescriptorProto_Label["LABEL_REPEATED"] = 3] = "LABEL_REPEATED";
})(_google_protobuf_FieldDescriptorProto_Label = exports._google_protobuf_FieldDescriptorProto_Label || (exports._google_protobuf_FieldDescriptorProto_Label = {}));
// Original file: null
var _google_protobuf_FieldDescriptorProto_Type;
(function (_google_protobuf_FieldDescriptorProto_Type) {
    _google_protobuf_FieldDescriptorProto_Type[_google_protobuf_FieldDescriptorProto_Type["TYPE_DOUBLE"] = 1] = "TYPE_DOUBLE";
    _google_protobuf_FieldDescriptorProto_Type[_google_protobuf_FieldDescriptorProto_Type["TYPE_FLOAT"] = 2] = "TYPE_FLOAT";
    _google_protobuf_FieldDescriptorProto_Type[_google_protobuf_FieldDescriptorProto_Type["TYPE_INT64"] = 3] = "TYPE_INT64";
    _google_protobuf_FieldDescriptorProto_Type[_google_protobuf_FieldDescriptorProto_Type["TYPE_UINT64"] = 4] = "TYPE_UINT64";
    _google_protobuf_FieldDescriptorProto_Type[_google_protobuf_FieldDescriptorProto_Type["TYPE_INT32"] = 5] = "TYPE_INT32";
    _google_protobuf_FieldDescriptorProto_Type[_google_protobuf_FieldDescriptorProto_Type["TYPE_FIXED64"] = 6] = "TYPE_FIXED64";
    _google_protobuf_FieldDescriptorProto_Type[_google_protobuf_FieldDescriptorProto_Type["TYPE_FIXED32"] = 7] = "TYPE_FIXED32";
    _google_protobuf_FieldDescriptorProto_Type[_google_protobuf_FieldDescriptorProto_Type["TYPE_BOOL"] = 8] = "TYPE_BOOL";
    _google_protobuf_FieldDescriptorProto_Type[_google_protobuf_FieldDescriptorProto_Type["TYPE_STRING"] = 9] = "TYPE_STRING";
    _google_protobuf_FieldDescriptorProto_Type[_google_protobuf_FieldDescriptorProto_Type["TYPE_GROUP"] = 10] = "TYPE_GROUP";
    _google_protobuf_FieldDescriptorProto_Type[_google_protobuf_FieldDescriptorProto_Type["TYPE_MESSAGE"] = 11] = "TYPE_MESSAGE";
    _google_protobuf_FieldDescriptorProto_Type[_google_protobuf_FieldDescriptorProto_Type["TYPE_BYTES"] = 12] = "TYPE_BYTES";
    _google_protobuf_FieldDescriptorProto_Type[_google_protobuf_FieldDescriptorProto_Type["TYPE_UINT32"] = 13] = "TYPE_UINT32";
    _google_protobuf_FieldDescriptorProto_Type[_google_protobuf_FieldDescriptorProto_Type["TYPE_ENUM"] = 14] = "TYPE_ENUM";
    _google_protobuf_FieldDescriptorProto_Type[_google_protobuf_FieldDescriptorProto_Type["TYPE_SFIXED32"] = 15] = "TYPE_SFIXED32";
    _google_protobuf_FieldDescriptorProto_Type[_google_protobuf_FieldDescriptorProto_Type["TYPE_SFIXED64"] = 16] = "TYPE_SFIXED64";
    _google_protobuf_FieldDescriptorProto_Type[_google_protobuf_FieldDescriptorProto_Type["TYPE_SINT32"] = 17] = "TYPE_SINT32";
    _google_protobuf_FieldDescriptorProto_Type[_google_protobuf_FieldDescriptorProto_Type["TYPE_SINT64"] = 18] = "TYPE_SINT64";
})(_google_protobuf_FieldDescriptorProto_Type = exports._google_protobuf_FieldDescriptorProto_Type || (exports._google_protobuf_FieldDescriptorProto_Type = {}));
