"use strict";
// Original file: null
Object.defineProperty(exports, "__esModule", { value: true });
exports._google_protobuf_FieldOptions_JSType = exports._google_protobuf_FieldOptions_CType = void 0;
// Original file: null
var _google_protobuf_FieldOptions_CType;
(function (_google_protobuf_FieldOptions_CType) {
    _google_protobuf_FieldOptions_CType[_google_protobuf_FieldOptions_CType["STRING"] = 0] = "STRING";
    _google_protobuf_FieldOptions_CType[_google_protobuf_FieldOptions_CType["CORD"] = 1] = "CORD";
    _google_protobuf_FieldOptions_CType[_google_protobuf_FieldOptions_CType["STRING_PIECE"] = 2] = "STRING_PIECE";
})(_google_protobuf_FieldOptions_CType = exports._google_protobuf_FieldOptions_CType || (exports._google_protobuf_FieldOptions_CType = {}));
// Original file: null
var _google_protobuf_FieldOptions_JSType;
(function (_google_protobuf_FieldOptions_JSType) {
    _google_protobuf_FieldOptions_JSType[_google_protobuf_FieldOptions_JSType["JS_NORMAL"] = 0] = "JS_NORMAL";
    _google_protobuf_FieldOptions_JSType[_google_protobuf_FieldOptions_JSType["JS_STRING"] = 1] = "JS_STRING";
    _google_protobuf_FieldOptions_JSType[_google_protobuf_FieldOptions_JSType["JS_NUMBER"] = 2] = "JS_NUMBER";
})(_google_protobuf_FieldOptions_JSType = exports._google_protobuf_FieldOptions_JSType || (exports._google_protobuf_FieldOptions_JSType = {}));
