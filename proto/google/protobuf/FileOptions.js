"use strict";
// Original file: null
Object.defineProperty(exports, "__esModule", { value: true });
exports._google_protobuf_FileOptions_OptimizeMode = void 0;
// Original file: null
var _google_protobuf_FileOptions_OptimizeMode;
(function (_google_protobuf_FileOptions_OptimizeMode) {
    _google_protobuf_FileOptions_OptimizeMode[_google_protobuf_FileOptions_OptimizeMode["SPEED"] = 1] = "SPEED";
    _google_protobuf_FileOptions_OptimizeMode[_google_protobuf_FileOptions_OptimizeMode["CODE_SIZE"] = 2] = "CODE_SIZE";
    _google_protobuf_FileOptions_OptimizeMode[_google_protobuf_FileOptions_OptimizeMode["LITE_RUNTIME"] = 3] = "LITE_RUNTIME";
})(_google_protobuf_FileOptions_OptimizeMode = exports._google_protobuf_FileOptions_OptimizeMode || (exports._google_protobuf_FileOptions_OptimizeMode = {}));
