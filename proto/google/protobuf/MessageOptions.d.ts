import type { UninterpretedOption as _google_protobuf_UninterpretedOption, UninterpretedOption__Output as _google_protobuf_UninterpretedOption__Output } from '../../google/protobuf/UninterpretedOption';
import type { MessageMetadata as _pando_api_MessageMetadata, MessageMetadata__Output as _pando_api_MessageMetadata__Output } from '../../pando/api/MessageMetadata';
export interface MessageOptions {
    'messageSetWireFormat'?: (boolean);
    'noStandardDescriptorAccessor'?: (boolean);
    'deprecated'?: (boolean);
    'mapEntry'?: (boolean);
    'uninterpretedOption'?: (_google_protobuf_UninterpretedOption)[];
    '.pando.api.message'?: (_pando_api_MessageMetadata | null);
}
export interface MessageOptions__Output {
    'messageSetWireFormat'?: (boolean);
    'noStandardDescriptorAccessor'?: (boolean);
    'deprecated'?: (boolean);
    'mapEntry'?: (boolean);
    'uninterpretedOption'?: (_google_protobuf_UninterpretedOption__Output)[];
    '.pando.api.message'?: (_pando_api_MessageMetadata__Output);
}
