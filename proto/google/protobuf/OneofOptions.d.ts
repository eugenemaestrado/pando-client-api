import type { UninterpretedOption as _google_protobuf_UninterpretedOption, UninterpretedOption__Output as _google_protobuf_UninterpretedOption__Output } from '../../google/protobuf/UninterpretedOption';
import type { MessageMetadata as _pando_api_MessageMetadata, MessageMetadata__Output as _pando_api_MessageMetadata__Output } from '../../pando/api/MessageMetadata';
export interface OneofOptions {
    'uninterpretedOption'?: (_google_protobuf_UninterpretedOption)[];
    '.pando.api.oneof'?: (_pando_api_MessageMetadata | null);
}
export interface OneofOptions__Output {
    'uninterpretedOption'?: (_google_protobuf_UninterpretedOption__Output)[];
    '.pando.api.oneof'?: (_pando_api_MessageMetadata__Output);
}
