import type { UninterpretedOption as _google_protobuf_UninterpretedOption, UninterpretedOption__Output as _google_protobuf_UninterpretedOption__Output } from '../../google/protobuf/UninterpretedOption';
import type { ServiceMetadata as _pando_api_ServiceMetadata, ServiceMetadata__Output as _pando_api_ServiceMetadata__Output } from '../../pando/api/ServiceMetadata';
export interface ServiceOptions {
    'deprecated'?: (boolean);
    'uninterpretedOption'?: (_google_protobuf_UninterpretedOption)[];
    '.pando.api.service'?: (_pando_api_ServiceMetadata | null);
}
export interface ServiceOptions__Output {
    'deprecated'?: (boolean);
    'uninterpretedOption'?: (_google_protobuf_UninterpretedOption__Output)[];
    '.pando.api.service'?: (_pando_api_ServiceMetadata__Output);
}
