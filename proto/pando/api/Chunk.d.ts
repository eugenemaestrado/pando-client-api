/// <reference types="node" />
export interface Chunk {
    'bytes'?: (Buffer | Uint8Array | string);
}
export interface Chunk__Output {
    'bytes'?: (Buffer);
}
