// Original file: proto/pando/api/shared.proto


export interface Chunk {
  'bytes'?: (Buffer | Uint8Array | string);
}

export interface Chunk__Output {
  'bytes'?: (Buffer);
}
