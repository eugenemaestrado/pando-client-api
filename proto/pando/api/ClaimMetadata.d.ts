export interface ClaimMetadata {
    'claim'?: (string);
    'note'?: (string);
}
export interface ClaimMetadata__Output {
    'claim'?: (string);
    'note'?: (string);
}
