import type { ContactType as _pando_api_ContactType } from '../../pando/api/ContactType';
export interface ContactInfo {
    'contactType'?: (_pando_api_ContactType | keyof typeof _pando_api_ContactType);
    'contactInfo'?: (string);
}
export interface ContactInfo__Output {
    'contactType'?: (_pando_api_ContactType);
    'contactInfo'?: (string);
}
