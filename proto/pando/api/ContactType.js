"use strict";
// Original file: proto/pando/api/shared.proto
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContactType = void 0;
var ContactType;
(function (ContactType) {
    ContactType[ContactType["PHONE"] = 0] = "PHONE";
    ContactType[ContactType["EMAIL"] = 1] = "EMAIL";
})(ContactType = exports.ContactType || (exports.ContactType = {}));
