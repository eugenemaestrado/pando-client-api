// Original file: proto/pando/api/shared.proto

export enum ContactType {
  PHONE = 0,
  EMAIL = 1,
}
