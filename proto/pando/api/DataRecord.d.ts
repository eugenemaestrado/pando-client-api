import type { FileType as _pando_api_FileType } from '../../pando/api/FileType';
export interface DataRecord {
    'format'?: (_pando_api_FileType | keyof typeof _pando_api_FileType);
    'text'?: (string);
}
export interface DataRecord__Output {
    'format'?: (_pando_api_FileType);
    'text'?: (string);
}
