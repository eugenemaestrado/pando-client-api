/// <reference types="long" />
import type { Long } from '@grpc/proto-loader';
export interface DecimalValue {
    'units'?: (number | string | Long);
    'nanos'?: (number);
}
export interface DecimalValue__Output {
    'units'?: (Long);
    'nanos'?: (number);
}
