import type { FileMetadata as _pando_api_FileMetadata, FileMetadata__Output as _pando_api_FileMetadata__Output } from '../../pando/api/FileMetadata';
import type { Chunk as _pando_api_Chunk, Chunk__Output as _pando_api_Chunk__Output } from '../../pando/api/Chunk';
export interface FileChunk {
    'metadata'?: (_pando_api_FileMetadata | null);
    'chunk'?: (_pando_api_Chunk | null);
    'response'?: "metadata" | "chunk";
}
export interface FileChunk__Output {
    'metadata'?: (_pando_api_FileMetadata__Output);
    'chunk'?: (_pando_api_Chunk__Output);
}
