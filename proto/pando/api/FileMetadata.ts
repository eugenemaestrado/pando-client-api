// Original file: proto/pando/api/shared.proto

import type { FileType as _pando_api_FileType } from '../../pando/api/FileType';
import type { Long } from '@grpc/proto-loader';

export interface FileMetadata {
  'name'?: (string);
  'size'?: (number | string | Long);
  'type'?: (_pando_api_FileType | keyof typeof _pando_api_FileType);
  'guid'?: (string);
}

export interface FileMetadata__Output {
  'name'?: (string);
  'size'?: (Long);
  'type'?: (_pando_api_FileType);
  'guid'?: (string);
}
