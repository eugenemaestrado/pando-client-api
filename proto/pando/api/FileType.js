"use strict";
// Original file: proto/pando/api/shared.proto
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileType = void 0;
var FileType;
(function (FileType) {
    FileType[FileType["MP4"] = 0] = "MP4";
    FileType[FileType["MOV"] = 1] = "MOV";
    FileType[FileType["M4A"] = 100] = "M4A";
    FileType[FileType["WAV"] = 101] = "WAV";
    FileType[FileType["JS"] = 102] = "JS";
    FileType[FileType["JSON"] = 103] = "JSON";
    FileType[FileType["YAML"] = 104] = "YAML";
    FileType[FileType["XML"] = 105] = "XML";
    FileType[FileType["CSV"] = 106] = "CSV";
})(FileType = exports.FileType || (exports.FileType = {}));
