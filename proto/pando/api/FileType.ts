// Original file: proto/pando/api/shared.proto

export enum FileType {
  MP4 = 0,
  MOV = 1,
  M4A = 100,
  WAV = 101,
  JS = 102,
  JSON = 103,
  YAML = 104,
  XML = 105,
  CSV = 106,
}
