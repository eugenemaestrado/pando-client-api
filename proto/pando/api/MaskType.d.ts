export declare enum MaskType {
    NONE = 0,
    CREDIT_CARD_NUMBER = 1,
    BANK_ACCOUNT_NUMBER = 2,
    SOCIAL_SECURITY_NUMBER = 3
}
