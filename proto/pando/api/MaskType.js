"use strict";
// Original file: proto/pando/api/shared.proto
Object.defineProperty(exports, "__esModule", { value: true });
exports.MaskType = void 0;
var MaskType;
(function (MaskType) {
    MaskType[MaskType["NONE"] = 0] = "NONE";
    MaskType[MaskType["CREDIT_CARD_NUMBER"] = 1] = "CREDIT_CARD_NUMBER";
    MaskType[MaskType["BANK_ACCOUNT_NUMBER"] = 2] = "BANK_ACCOUNT_NUMBER";
    MaskType[MaskType["SOCIAL_SECURITY_NUMBER"] = 3] = "SOCIAL_SECURITY_NUMBER";
})(MaskType = exports.MaskType || (exports.MaskType = {}));
