// Original file: proto/pando/api/extensions.proto


export interface MessageMetadata {
  'personalInfo'?: (boolean);
  'sensitiveInfo'?: (boolean);
  'responseOnly'?: (boolean);
  'requestOnly'?: (boolean);
  'requestRequired'?: (boolean);
}

export interface MessageMetadata__Output {
  'personalInfo'?: (boolean);
  'sensitiveInfo'?: (boolean);
  'responseOnly'?: (boolean);
  'requestOnly'?: (boolean);
  'requestRequired'?: (boolean);
}
