// Original file: proto/pando/api/shared.proto


export interface PaginationResult {
  'currentPage'?: (number);
  'currentResultCount'?: (number);
  'totalPageCount'?: (number);
  'totalResultCount'?: (number);
}

export interface PaginationResult__Output {
  'currentPage'?: (number);
  'currentResultCount'?: (number);
  'totalPageCount'?: (number);
  'totalResultCount'?: (number);
}
