import type { ScopeMetadata as _pando_api_ScopeMetadata, ScopeMetadata__Output as _pando_api_ScopeMetadata__Output } from '../../pando/api/ScopeMetadata';
import type { ClaimMetadata as _pando_api_ClaimMetadata, ClaimMetadata__Output as _pando_api_ClaimMetadata__Output } from '../../pando/api/ClaimMetadata';
export interface ServiceMetadata {
    'requiredScope'?: (string)[];
    'requiredClaim'?: (string)[];
    'requireUserAuth'?: (boolean);
    'requireClientAuth'?: (boolean);
    'optionalScope'?: (_pando_api_ScopeMetadata)[];
    'optionalClaim'?: (_pando_api_ClaimMetadata)[];
}
export interface ServiceMetadata__Output {
    'requiredScope'?: (string)[];
    'requiredClaim'?: (string)[];
    'requireUserAuth'?: (boolean);
    'requireClientAuth'?: (boolean);
    'optionalScope'?: (_pando_api_ScopeMetadata__Output)[];
    'optionalClaim'?: (_pando_api_ClaimMetadata__Output)[];
}
