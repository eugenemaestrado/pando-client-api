import type { ContactInfo as _pando_api_ContactInfo, ContactInfo__Output as _pando_api_ContactInfo__Output } from '../../pando/api/ContactInfo';
export interface UserContactInfo {
    'userId'?: (string);
    'contactInfo'?: (_pando_api_ContactInfo | null);
}
export interface UserContactInfo__Output {
    'userId'?: (string);
    'contactInfo'?: (_pando_api_ContactInfo__Output);
}
