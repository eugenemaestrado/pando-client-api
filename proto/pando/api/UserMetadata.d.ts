export interface UserMetadata {
    'id'?: (string);
    'clientId'?: (string);
    'deviceId'?: (string);
    'longitude'?: (string);
    'latitude'?: (string);
}
export interface UserMetadata__Output {
    'id'?: (string);
    'clientId'?: (string);
    'deviceId'?: (string);
    'longitude'?: (string);
    'latitude'?: (string);
}
