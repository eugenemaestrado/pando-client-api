"use strict";
// Original file: proto/pando/api/shared.proto
Object.defineProperty(exports, "__esModule", { value: true });
exports.VariableType = void 0;
var VariableType;
(function (VariableType) {
    VariableType[VariableType["STRING"] = 0] = "STRING";
    VariableType[VariableType["INT"] = 1] = "INT";
    VariableType[VariableType["FLOAT"] = 2] = "FLOAT";
    VariableType[VariableType["BOOL"] = 3] = "BOOL";
    VariableType[VariableType["DATETIME"] = 4] = "DATETIME";
})(VariableType = exports.VariableType || (exports.VariableType = {}));
