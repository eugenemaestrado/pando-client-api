// Original file: proto/pando/api/shared.proto

export enum VariableType {
  STRING = 0,
  INT = 1,
  FLOAT = 2,
  BOOL = 3,
  DATETIME = 4,
}
