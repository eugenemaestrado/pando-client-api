export declare enum Action {
    STOP = 0,
    START = 1,
    PAUSE = 2,
    RESUME = 3
}
