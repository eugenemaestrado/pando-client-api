"use strict";
// Original file: proto/pando/api/survey/v1/shared.proto
Object.defineProperty(exports, "__esModule", { value: true });
exports.Action = void 0;
var Action;
(function (Action) {
    Action[Action["STOP"] = 0] = "STOP";
    Action[Action["START"] = 1] = "START";
    Action[Action["PAUSE"] = 2] = "PAUSE";
    Action[Action["RESUME"] = 3] = "RESUME";
})(Action = exports.Action || (exports.Action = {}));
