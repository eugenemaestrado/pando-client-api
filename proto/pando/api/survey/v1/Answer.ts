// Original file: proto/pando/api/survey/v1/shared.proto

import type { Timestamp as _google_protobuf_Timestamp, Timestamp__Output as _google_protobuf_Timestamp__Output } from '../../../../google/protobuf/Timestamp';

export interface Answer {
  'guid'?: (string);
  'attemptGuid'?: (string);
  'deviceId'?: (string);
  'text'?: (string);
  'timestamp'?: (_google_protobuf_Timestamp | null);
  'mediaGuids'?: (string)[];
  'questionGuid'?: (string);
}

export interface Answer__Output {
  'guid'?: (string);
  'attemptGuid'?: (string);
  'deviceId'?: (string);
  'text'?: (string);
  'timestamp'?: (_google_protobuf_Timestamp__Output);
  'mediaGuids'?: (string)[];
  'questionGuid'?: (string);
}
