"use strict";
// Original file: proto/pando/api/survey/v1/shared.proto
Object.defineProperty(exports, "__esModule", { value: true });
exports.AnswerAction = void 0;
var AnswerAction;
(function (AnswerAction) {
    AnswerAction[AnswerAction["ADD"] = 0] = "ADD";
    AnswerAction[AnswerAction["DELETE"] = 1] = "DELETE";
})(AnswerAction = exports.AnswerAction || (exports.AnswerAction = {}));
