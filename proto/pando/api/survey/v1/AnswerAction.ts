// Original file: proto/pando/api/survey/v1/shared.proto

export enum AnswerAction {
  ADD = 0,
  DELETE = 1,
}
