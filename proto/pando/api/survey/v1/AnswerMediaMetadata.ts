// Original file: proto/pando/api/survey/v1/shared.proto

import type { MediaInfo as _pando_api_survey_v1_MediaInfo, MediaInfo__Output as _pando_api_survey_v1_MediaInfo__Output } from '../../../../pando/api/survey/v1/MediaInfo';

export interface AnswerMediaMetadata {
  'answerGuid'?: (string);
  'info'?: (_pando_api_survey_v1_MediaInfo | null);
}

export interface AnswerMediaMetadata__Output {
  'answerGuid'?: (string);
  'info'?: (_pando_api_survey_v1_MediaInfo__Output);
}
