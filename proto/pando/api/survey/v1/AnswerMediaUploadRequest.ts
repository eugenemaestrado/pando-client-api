// Original file: proto/pando/api/survey/v1/shared.proto

import type { AnswerMediaMetadata as _pando_api_survey_v1_AnswerMediaMetadata, AnswerMediaMetadata__Output as _pando_api_survey_v1_AnswerMediaMetadata__Output } from '../../../../pando/api/survey/v1/AnswerMediaMetadata';
import type { FileChunk as _pando_api_FileChunk, FileChunk__Output as _pando_api_FileChunk__Output } from '../../../../pando/api/FileChunk';

export interface AnswerMediaUploadRequest {
  'meta'?: (_pando_api_survey_v1_AnswerMediaMetadata | null);
  'data'?: (_pando_api_FileChunk | null);
  'request'?: "meta"|"data";
}

export interface AnswerMediaUploadRequest__Output {
  'meta'?: (_pando_api_survey_v1_AnswerMediaMetadata__Output);
  'data'?: (_pando_api_FileChunk__Output);
}
