export interface AnswerOption {
    'guid'?: (string);
    'name'?: (string);
    'description'?: (string);
    'organizationCode'?: (string);
    'isDeleted'?: (boolean);
    'isAcceptable'?: (boolean);
}
export interface AnswerOption__Output {
    'guid'?: (string);
    'name'?: (string);
    'description'?: (string);
    'organizationCode'?: (string);
    'isDeleted'?: (boolean);
    'isAcceptable'?: (boolean);
}
