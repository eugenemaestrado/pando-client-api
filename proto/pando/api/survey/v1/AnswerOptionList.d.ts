import type { AnswerOption as _pando_api_survey_v1_AnswerOption, AnswerOption__Output as _pando_api_survey_v1_AnswerOption__Output } from '../../../../pando/api/survey/v1/AnswerOption';
import type { PaginationResult as _pando_api_PaginationResult, PaginationResult__Output as _pando_api_PaginationResult__Output } from '../../../../pando/api/PaginationResult';
export interface AnswerOptionList {
    'answerOptions'?: (_pando_api_survey_v1_AnswerOption)[];
    'pagination'?: (_pando_api_PaginationResult | null);
}
export interface AnswerOptionList__Output {
    'answerOptions'?: (_pando_api_survey_v1_AnswerOption__Output)[];
    'pagination'?: (_pando_api_PaginationResult__Output);
}
