// Original file: proto/pando/api/survey/v1/shared.proto

import type { AnswerType as _pando_api_survey_v1_AnswerType } from '../../../../pando/api/survey/v1/AnswerType';
import type { AnswerAction as _pando_api_survey_v1_AnswerAction } from '../../../../pando/api/survey/v1/AnswerAction';

export interface AnswerTimestamp {
  'answerGuid'?: (string);
  'answerType'?: (_pando_api_survey_v1_AnswerType | keyof typeof _pando_api_survey_v1_AnswerType);
  'answerAction'?: (_pando_api_survey_v1_AnswerAction | keyof typeof _pando_api_survey_v1_AnswerAction);
  'answerOptionGuid'?: (string);
  'answerText'?: (string);
  'timestamp'?: (number);
}

export interface AnswerTimestamp__Output {
  'answerGuid'?: (string);
  'answerType'?: (_pando_api_survey_v1_AnswerType);
  'answerAction'?: (_pando_api_survey_v1_AnswerAction);
  'answerOptionGuid'?: (string);
  'answerText'?: (string);
  'timestamp'?: (number);
}
