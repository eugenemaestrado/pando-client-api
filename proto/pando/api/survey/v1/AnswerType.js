"use strict";
// Original file: proto/pando/api/survey/v1/shared.proto
Object.defineProperty(exports, "__esModule", { value: true });
exports.AnswerType = void 0;
var AnswerType;
(function (AnswerType) {
    AnswerType[AnswerType["VOCAL"] = 0] = "VOCAL";
    AnswerType[AnswerType["BUTTON"] = 1] = "BUTTON";
})(AnswerType = exports.AnswerType || (exports.AnswerType = {}));
