// Original file: proto/pando/api/survey/v1/shared.proto

export enum AnswerType {
  VOCAL = 0,
  BUTTON = 1,
}
