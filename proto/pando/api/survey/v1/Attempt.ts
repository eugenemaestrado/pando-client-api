// Original file: proto/pando/api/survey/v1/shared.proto

import type { CompletionType as _pando_api_survey_v1_CompletionType } from '../../../../pando/api/survey/v1/CompletionType';

export interface Attempt {
  'guid'?: (string);
  'surveyInstanceGuid'?: (string);
  'completionType'?: (_pando_api_survey_v1_CompletionType | keyof typeof _pando_api_survey_v1_CompletionType);
  'totalAnswered'?: (number);
  'totalQuestions'?: (number);
}

export interface Attempt__Output {
  'guid'?: (string);
  'surveyInstanceGuid'?: (string);
  'completionType'?: (_pando_api_survey_v1_CompletionType);
  'totalAnswered'?: (number);
  'totalQuestions'?: (number);
}
