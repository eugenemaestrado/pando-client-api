// Original file: proto/pando/api/survey/v1/shared.proto

import type { Attempt as _pando_api_survey_v1_Attempt, Attempt__Output as _pando_api_survey_v1_Attempt__Output } from '../../../../pando/api/survey/v1/Attempt';
import type { PaginationResult as _pando_api_PaginationResult, PaginationResult__Output as _pando_api_PaginationResult__Output } from '../../../../pando/api/PaginationResult';

export interface AttemptList {
  'attempts'?: (_pando_api_survey_v1_Attempt)[];
  'pagination'?: (_pando_api_PaginationResult | null);
}

export interface AttemptList__Output {
  'attempts'?: (_pando_api_survey_v1_Attempt__Output)[];
  'pagination'?: (_pando_api_PaginationResult__Output);
}
