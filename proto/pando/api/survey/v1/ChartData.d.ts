export interface ChartData {
    'surveyName'?: (string);
    'completeCount'?: (number);
    'incompleteCount'?: (number);
}
export interface ChartData__Output {
    'surveyName'?: (string);
    'completeCount'?: (number);
    'incompleteCount'?: (number);
}
