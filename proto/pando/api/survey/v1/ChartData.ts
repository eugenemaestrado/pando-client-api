// Original file: proto/pando/api/survey/v1/reports.proto


export interface ChartData {
  'surveyName'?: (string);
  'completeCount'?: (number);
  'incompleteCount'?: (number);
}

export interface ChartData__Output {
  'surveyName'?: (string);
  'completeCount'?: (number);
  'incompleteCount'?: (number);
}
