export interface CompleteSurveyRequest {
    'surveyAttemptGuid'?: (string);
}
export interface CompleteSurveyRequest__Output {
    'surveyAttemptGuid'?: (string);
}
