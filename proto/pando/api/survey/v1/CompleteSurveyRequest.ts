// Original file: proto/pando/api/survey/v1/survey.proto


export interface CompleteSurveyRequest {
  'surveyAttemptGuid'?: (string);
}

export interface CompleteSurveyRequest__Output {
  'surveyAttemptGuid'?: (string);
}
