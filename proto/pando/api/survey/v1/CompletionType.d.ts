export declare enum CompletionType {
    INCOMPLETE = 0,
    UNACCEPTABLE = 1,
    ACCEPTABLE = 2
}
