"use strict";
// Original file: proto/pando/api/survey/v1/shared.proto
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompletionType = void 0;
var CompletionType;
(function (CompletionType) {
    CompletionType[CompletionType["INCOMPLETE"] = 0] = "INCOMPLETE";
    CompletionType[CompletionType["UNACCEPTABLE"] = 1] = "UNACCEPTABLE";
    CompletionType[CompletionType["ACCEPTABLE"] = 2] = "ACCEPTABLE";
})(CompletionType = exports.CompletionType || (exports.CompletionType = {}));
