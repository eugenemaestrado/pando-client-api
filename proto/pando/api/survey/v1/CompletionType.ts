// Original file: proto/pando/api/survey/v1/shared.proto

export enum CompletionType {
  INCOMPLETE = 0,
  UNACCEPTABLE = 1,
  ACCEPTABLE = 2,
}
