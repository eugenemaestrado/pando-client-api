import type { DataRecord as _pando_api_DataRecord, DataRecord__Output as _pando_api_DataRecord__Output } from '../../../../pando/api/DataRecord';
import type { ContactInfo as _pando_api_ContactInfo, ContactInfo__Output as _pando_api_ContactInfo__Output } from '../../../../pando/api/ContactInfo';
export interface CreateInstanceRequest {
    'surveyGuid'?: (string);
    'data'?: (_pando_api_DataRecord | null);
    'recordGuid'?: (string);
    'contactInfo'?: (_pando_api_ContactInfo | null);
    'respondentName'?: (string);
    'sendLink'?: (boolean);
    'userId'?: (string);
    'dataSource'?: "data" | "recordGuid";
}
export interface CreateInstanceRequest__Output {
    'surveyGuid'?: (string);
    'data'?: (_pando_api_DataRecord__Output);
    'recordGuid'?: (string);
    'contactInfo'?: (_pando_api_ContactInfo__Output);
    'respondentName'?: (string);
    'sendLink'?: (boolean);
    'userId'?: (string);
}
