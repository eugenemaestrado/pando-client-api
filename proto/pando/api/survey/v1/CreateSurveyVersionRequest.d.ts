export interface CreateSurveyVersionRequest {
    'surveyGuid'?: (string);
    'name'?: (string);
    'description'?: (string);
    'preSurveyText'?: (string);
    'firstQuestionGuid'?: (string);
}
export interface CreateSurveyVersionRequest__Output {
    'surveyGuid'?: (string);
    'name'?: (string);
    'description'?: (string);
    'preSurveyText'?: (string);
    'firstQuestionGuid'?: (string);
}
