// Original file: proto/pando/api/survey/v1/admin.proto


export interface CreateSurveyVersionRequest {
  'surveyGuid'?: (string);
  'name'?: (string);
  'description'?: (string);
  'preSurveyText'?: (string);
  'firstQuestionGuid'?: (string);
}

export interface CreateSurveyVersionRequest__Output {
  'surveyGuid'?: (string);
  'name'?: (string);
  'description'?: (string);
  'preSurveyText'?: (string);
  'firstQuestionGuid'?: (string);
}
