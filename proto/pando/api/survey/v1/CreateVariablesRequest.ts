// Original file: proto/pando/api/survey/v1/admin.proto

import type { FileType as _pando_api_FileType } from '../../../../pando/api/FileType';

export interface CreateVariablesRequest {
  'data'?: (string);
  'dataFormat'?: (_pando_api_FileType | keyof typeof _pando_api_FileType);
  'organizationCode'?: (string);
}

export interface CreateVariablesRequest__Output {
  'data'?: (string);
  'dataFormat'?: (_pando_api_FileType);
  'organizationCode'?: (string);
}
