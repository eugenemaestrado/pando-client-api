export interface DeleteAnswerOptionRequest {
    'guid'?: (string);
}
export interface DeleteAnswerOptionRequest__Output {
    'guid'?: (string);
}
