// Original file: proto/pando/api/survey/v1/admin.proto


export interface DeleteAnswerOptionRequest {
  'guid'?: (string);
}

export interface DeleteAnswerOptionRequest__Output {
  'guid'?: (string);
}
