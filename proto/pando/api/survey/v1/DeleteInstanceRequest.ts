// Original file: proto/pando/api/survey/v1/admin.proto


export interface DeleteInstanceRequest {
  'guid'?: (string);
}

export interface DeleteInstanceRequest__Output {
  'guid'?: (string);
}
