// Original file: proto/pando/api/survey/v1/admin.proto


export interface DeleteMediaRequest {
  'guid'?: (string);
}

export interface DeleteMediaRequest__Output {
  'guid'?: (string);
}
