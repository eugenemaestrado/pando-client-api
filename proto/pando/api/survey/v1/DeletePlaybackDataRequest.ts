// Original file: proto/pando/api/survey/v1/admin.proto


export interface DeletePlaybackDataRequest {
  'guid'?: (string);
}

export interface DeletePlaybackDataRequest__Output {
  'guid'?: (string);
}
