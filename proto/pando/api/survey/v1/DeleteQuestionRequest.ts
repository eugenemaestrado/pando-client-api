// Original file: proto/pando/api/survey/v1/admin.proto


export interface DeleteQuestionRequest {
  'guid'?: (string);
}

export interface DeleteQuestionRequest__Output {
  'guid'?: (string);
}
