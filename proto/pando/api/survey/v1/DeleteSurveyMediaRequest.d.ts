export interface DeleteSurveyMediaRequest {
    'surveyGuid'?: (string);
    'mediaGuid'?: (string);
}
export interface DeleteSurveyMediaRequest__Output {
    'surveyGuid'?: (string);
    'mediaGuid'?: (string);
}
