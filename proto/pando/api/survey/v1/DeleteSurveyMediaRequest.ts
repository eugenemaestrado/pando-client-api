// Original file: proto/pando/api/survey/v1/admin.proto


export interface DeleteSurveyMediaRequest {
  'surveyGuid'?: (string);
  'mediaGuid'?: (string);
}

export interface DeleteSurveyMediaRequest__Output {
  'surveyGuid'?: (string);
  'mediaGuid'?: (string);
}
