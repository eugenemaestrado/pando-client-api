export interface DeleteSurveyQuestionDestinationRequest {
    'guid'?: (string);
}
export interface DeleteSurveyQuestionDestinationRequest__Output {
    'guid'?: (string);
}
