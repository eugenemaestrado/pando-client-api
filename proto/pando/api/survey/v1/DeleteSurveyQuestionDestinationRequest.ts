// Original file: proto/pando/api/survey/v1/admin.proto


export interface DeleteSurveyQuestionDestinationRequest {
  'guid'?: (string);
}

export interface DeleteSurveyQuestionDestinationRequest__Output {
  'guid'?: (string);
}
