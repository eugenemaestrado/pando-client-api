export interface DeleteSurveyRequest {
    'guid'?: (string);
}
export interface DeleteSurveyRequest__Output {
    'guid'?: (string);
}
