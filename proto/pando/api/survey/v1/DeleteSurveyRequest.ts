// Original file: proto/pando/api/survey/v1/admin.proto


export interface DeleteSurveyRequest {
  'guid'?: (string);
}

export interface DeleteSurveyRequest__Output {
  'guid'?: (string);
}
