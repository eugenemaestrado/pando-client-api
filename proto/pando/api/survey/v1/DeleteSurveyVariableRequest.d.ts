export interface DeleteSurveyVariableRequest {
    'surveyGuid'?: (string);
    'variableGuid'?: (string);
}
export interface DeleteSurveyVariableRequest__Output {
    'surveyGuid'?: (string);
    'variableGuid'?: (string);
}
