// Original file: proto/pando/api/survey/v1/admin.proto


export interface DeleteSurveyVariableRequest {
  'surveyGuid'?: (string);
  'variableGuid'?: (string);
}

export interface DeleteSurveyVariableRequest__Output {
  'surveyGuid'?: (string);
  'variableGuid'?: (string);
}
