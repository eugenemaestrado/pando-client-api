export interface DeleteSurveyVersionRequest {
    'versionGuid'?: (string);
}
export interface DeleteSurveyVersionRequest__Output {
    'versionGuid'?: (string);
}
