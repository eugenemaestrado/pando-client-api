// Original file: proto/pando/api/survey/v1/admin.proto


export interface DeleteSurveyVersionRequest {
  'versionGuid'?: (string);
}

export interface DeleteSurveyVersionRequest__Output {
  'versionGuid'?: (string);
}
