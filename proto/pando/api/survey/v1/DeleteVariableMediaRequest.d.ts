export interface DeleteVariableMediaRequest {
    'variableGuid'?: (string);
    'mediaGuid'?: (string);
    'value'?: (string);
}
export interface DeleteVariableMediaRequest__Output {
    'variableGuid'?: (string);
    'mediaGuid'?: (string);
    'value'?: (string);
}
