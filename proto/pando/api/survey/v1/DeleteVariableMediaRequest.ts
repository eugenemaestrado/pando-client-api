// Original file: proto/pando/api/survey/v1/admin.proto


export interface DeleteVariableMediaRequest {
  'variableGuid'?: (string);
  'mediaGuid'?: (string);
  'value'?: (string);
}

export interface DeleteVariableMediaRequest__Output {
  'variableGuid'?: (string);
  'mediaGuid'?: (string);
  'value'?: (string);
}
