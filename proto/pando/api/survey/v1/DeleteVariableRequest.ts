// Original file: proto/pando/api/survey/v1/admin.proto


export interface DeleteVariableRequest {
  'guid'?: (string);
}

export interface DeleteVariableRequest__Output {
  'guid'?: (string);
}
