export interface ExecAddInstanceOwnerRequest {
    'instanceGuid'?: (string);
    'userIds'?: (string)[];
}
export interface ExecAddInstanceOwnerRequest__Output {
    'instanceGuid'?: (string);
    'userIds'?: (string)[];
}
