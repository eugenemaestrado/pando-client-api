// Original file: proto/pando/api/survey/v1/survey.proto


export interface ExecAddInstanceOwnerRequest {
  'instanceGuid'?: (string);
  'userIds'?: (string)[];
}

export interface ExecAddInstanceOwnerRequest__Output {
  'instanceGuid'?: (string);
  'userIds'?: (string)[];
}
