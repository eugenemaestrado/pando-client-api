// Original file: proto/pando/api/survey/v1/survey.proto


export interface ExecCompleteQuestionRequest {
  'answerGuid'?: (string);
}

export interface ExecCompleteQuestionRequest__Output {
  'answerGuid'?: (string);
}
