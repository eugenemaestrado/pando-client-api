// Original file: proto/pando/api/survey/v1/survey.proto

import type { CompletionType as _pando_api_survey_v1_CompletionType } from '../../../../pando/api/survey/v1/CompletionType';
import type { Timestamp as _google_protobuf_Timestamp, Timestamp__Output as _google_protobuf_Timestamp__Output } from '../../../../google/protobuf/Timestamp';

export interface ExecCompleteQuestionResponse {
  'completionType'?: (_pando_api_survey_v1_CompletionType | keyof typeof _pando_api_survey_v1_CompletionType);
  'message'?: (string);
  'timestamp'?: (_google_protobuf_Timestamp | null);
  'relativeTimestamp'?: (number);
}

export interface ExecCompleteQuestionResponse__Output {
  'completionType'?: (_pando_api_survey_v1_CompletionType);
  'message'?: (string);
  'timestamp'?: (_google_protobuf_Timestamp__Output);
  'relativeTimestamp'?: (number);
}
