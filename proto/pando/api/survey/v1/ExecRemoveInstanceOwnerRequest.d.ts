export interface ExecRemoveInstanceOwnerRequest {
    'instanceGuid'?: (string);
}
export interface ExecRemoveInstanceOwnerRequest__Output {
    'instanceGuid'?: (string);
}
