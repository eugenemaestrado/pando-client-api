// Original file: proto/pando/api/survey/v1/survey.proto


export interface ExecRemoveInstanceOwnerRequest {
  'instanceGuid'?: (string);
}

export interface ExecRemoveInstanceOwnerRequest__Output {
  'instanceGuid'?: (string);
}
