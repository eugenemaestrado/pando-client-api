// Original file: proto/pando/api/survey/v1/survey.proto


export interface FacialRecognitionRequest {
  'base64Img'?: (string);
}

export interface FacialRecognitionRequest__Output {
  'base64Img'?: (string);
}
