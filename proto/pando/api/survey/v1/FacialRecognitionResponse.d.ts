export interface FacialRecognitionResponse {
    'faceInFrame'?: (boolean);
}
export interface FacialRecognitionResponse__Output {
    'faceInFrame'?: (boolean);
}
