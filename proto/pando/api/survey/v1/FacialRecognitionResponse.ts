// Original file: proto/pando/api/survey/v1/survey.proto


export interface FacialRecognitionResponse {
  'faceInFrame'?: (boolean);
}

export interface FacialRecognitionResponse__Output {
  'faceInFrame'?: (boolean);
}
