export interface GetAnswerOptionsRequest {
    'filter'?: (string);
    'includeDeleted'?: (boolean);
    'organizationCode'?: (string);
    'page'?: (number);
    'resultsPerPage'?: (number);
}
export interface GetAnswerOptionsRequest__Output {
    'filter'?: (string);
    'includeDeleted'?: (boolean);
    'organizationCode'?: (string);
    'page'?: (number);
    'resultsPerPage'?: (number);
}
