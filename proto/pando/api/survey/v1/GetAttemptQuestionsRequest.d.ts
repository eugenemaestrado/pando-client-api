export interface GetAttemptQuestionsRequest {
    'surveyAttemptGuid'?: (string);
}
export interface GetAttemptQuestionsRequest__Output {
    'surveyAttemptGuid'?: (string);
}
