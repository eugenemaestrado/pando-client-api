// Original file: proto/pando/api/survey/v1/survey.proto


export interface GetAttemptQuestionsRequest {
  'surveyAttemptGuid'?: (string);
}

export interface GetAttemptQuestionsRequest__Output {
  'surveyAttemptGuid'?: (string);
}
