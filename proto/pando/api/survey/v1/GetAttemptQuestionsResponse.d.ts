export interface GetAttemptQuestionsResponse {
    'questionGuids'?: (string)[];
}
export interface GetAttemptQuestionsResponse__Output {
    'questionGuids'?: (string)[];
}
