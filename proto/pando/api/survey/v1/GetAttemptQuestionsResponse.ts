// Original file: proto/pando/api/survey/v1/survey.proto


export interface GetAttemptQuestionsResponse {
  'questionGuids'?: (string)[];
}

export interface GetAttemptQuestionsResponse__Output {
  'questionGuids'?: (string)[];
}
