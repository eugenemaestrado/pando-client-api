// Original file: proto/pando/api/survey/v1/reports.proto

import type { Timestamp as _google_protobuf_Timestamp, Timestamp__Output as _google_protobuf_Timestamp__Output } from '../../../../google/protobuf/Timestamp';

export interface GetInstancesReportRequest {
  'userId'?: (string);
  'organizationCode'?: (string);
  'start'?: (_google_protobuf_Timestamp | null);
  'end'?: (_google_protobuf_Timestamp | null);
  'page'?: (number);
  'resultsPerPage'?: (number);
  'target'?: "userId"|"organizationCode";
}

export interface GetInstancesReportRequest__Output {
  'userId'?: (string);
  'organizationCode'?: (string);
  'start'?: (_google_protobuf_Timestamp__Output);
  'end'?: (_google_protobuf_Timestamp__Output);
  'page'?: (number);
  'resultsPerPage'?: (number);
}
