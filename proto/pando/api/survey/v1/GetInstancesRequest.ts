// Original file: proto/pando/api/survey/v1/survey.proto

import type { Timestamp as _google_protobuf_Timestamp, Timestamp__Output as _google_protobuf_Timestamp__Output } from '../../../../google/protobuf/Timestamp';

export interface GetInstancesRequest {
  'userId'?: (string);
  'includeCompleted'?: (boolean);
  'start'?: (_google_protobuf_Timestamp | null);
  'end'?: (_google_protobuf_Timestamp | null);
  'includeDeleted'?: (boolean);
  'page'?: (number);
  'resultsPerPage'?: (number);
}

export interface GetInstancesRequest__Output {
  'userId'?: (string);
  'includeCompleted'?: (boolean);
  'start'?: (_google_protobuf_Timestamp__Output);
  'end'?: (_google_protobuf_Timestamp__Output);
  'includeDeleted'?: (boolean);
  'page'?: (number);
  'resultsPerPage'?: (number);
}
