// Original file: proto/pando/api/survey/v1/survey.proto


export interface GetMediaRequest {
  'mediaGuid'?: (string);
}

export interface GetMediaRequest__Output {
  'mediaGuid'?: (string);
}
