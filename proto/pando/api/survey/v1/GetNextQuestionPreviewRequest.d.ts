import type { DataRecord as _pando_api_DataRecord, DataRecord__Output as _pando_api_DataRecord__Output } from '../../../../pando/api/DataRecord';
export interface GetNextQuestionPreviewRequest {
    'questionAnswers'?: ({
        [key: string]: string;
    });
    'questionGuid'?: (string);
    'surveyVersionGuid'?: (string);
    'dataRecord'?: (_pando_api_DataRecord | null);
}
export interface GetNextQuestionPreviewRequest__Output {
    'questionAnswers'?: ({
        [key: string]: string;
    });
    'questionGuid'?: (string);
    'surveyVersionGuid'?: (string);
    'dataRecord'?: (_pando_api_DataRecord__Output);
}
