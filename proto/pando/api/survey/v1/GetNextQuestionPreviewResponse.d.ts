export interface GetNextQuestionPreviewResponse {
    'questionGuid'?: (string);
}
export interface GetNextQuestionPreviewResponse__Output {
    'questionGuid'?: (string);
}
