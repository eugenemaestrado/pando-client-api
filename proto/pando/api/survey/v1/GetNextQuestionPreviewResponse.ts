// Original file: proto/pando/api/survey/v1/preview.proto


export interface GetNextQuestionPreviewResponse {
  'questionGuid'?: (string);
}

export interface GetNextQuestionPreviewResponse__Output {
  'questionGuid'?: (string);
}
