// Original file: proto/pando/api/survey/v1/survey.proto


export interface GetNextQuestionRequest {
  'attemptGuid'?: (string);
}

export interface GetNextQuestionRequest__Output {
  'attemptGuid'?: (string);
}
