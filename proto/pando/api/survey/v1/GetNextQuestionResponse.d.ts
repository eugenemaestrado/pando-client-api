import type { QuestionWithPlaybackData as _pando_api_survey_v1_QuestionWithPlaybackData, QuestionWithPlaybackData__Output as _pando_api_survey_v1_QuestionWithPlaybackData__Output } from '../../../../pando/api/survey/v1/QuestionWithPlaybackData';
import type { NoMoreQuestionsResponse as _pando_api_survey_v1_NoMoreQuestionsResponse, NoMoreQuestionsResponse__Output as _pando_api_survey_v1_NoMoreQuestionsResponse__Output } from '../../../../pando/api/survey/v1/NoMoreQuestionsResponse';
export interface GetNextQuestionResponse {
    'question'?: (_pando_api_survey_v1_QuestionWithPlaybackData | null);
    'message'?: (_pando_api_survey_v1_NoMoreQuestionsResponse | null);
    'response'?: "question" | "message";
}
export interface GetNextQuestionResponse__Output {
    'question'?: (_pando_api_survey_v1_QuestionWithPlaybackData__Output);
    'message'?: (_pando_api_survey_v1_NoMoreQuestionsResponse__Output);
}
