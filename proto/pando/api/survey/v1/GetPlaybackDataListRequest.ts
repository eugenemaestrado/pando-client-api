// Original file: proto/pando/api/survey/v1/admin.proto


export interface GetPlaybackDataListRequest {
  'questionGuid'?: (string);
}

export interface GetPlaybackDataListRequest__Output {
  'questionGuid'?: (string);
}
