// Original file: proto/pando/api/survey/v1/admin.proto


export interface GetQuestionRequest {
  'guid'?: (string);
}

export interface GetQuestionRequest__Output {
  'guid'?: (string);
}
