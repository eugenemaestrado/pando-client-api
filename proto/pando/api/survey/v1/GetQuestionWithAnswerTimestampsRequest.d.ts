export interface GetQuestionWithAnswerTimestampsRequest {
    'surveyAttemptGuid'?: (string);
    'questionGuid'?: (string);
}
export interface GetQuestionWithAnswerTimestampsRequest__Output {
    'surveyAttemptGuid'?: (string);
    'questionGuid'?: (string);
}
