import type { QuestionWithPlaybackData as _pando_api_survey_v1_QuestionWithPlaybackData, QuestionWithPlaybackData__Output as _pando_api_survey_v1_QuestionWithPlaybackData__Output } from '../../../../pando/api/survey/v1/QuestionWithPlaybackData';
import type { AnswerTimestamp as _pando_api_survey_v1_AnswerTimestamp, AnswerTimestamp__Output as _pando_api_survey_v1_AnswerTimestamp__Output } from '../../../../pando/api/survey/v1/AnswerTimestamp';
export interface GetQuestionWithAnswerTimestampsResponse {
    'question'?: (_pando_api_survey_v1_QuestionWithPlaybackData | null);
    'answerTimestamps'?: (_pando_api_survey_v1_AnswerTimestamp)[];
    'answerMediaGuid'?: (string);
}
export interface GetQuestionWithAnswerTimestampsResponse__Output {
    'question'?: (_pando_api_survey_v1_QuestionWithPlaybackData__Output);
    'answerTimestamps'?: (_pando_api_survey_v1_AnswerTimestamp__Output)[];
    'answerMediaGuid'?: (string);
}
