export interface GetQuestionsRequest {
    'surveyGuid'?: (string);
    'filter'?: (string);
    'versionGuid'?: (string);
    'includeDeleted'?: (boolean);
    'page'?: (number);
    'resultsPerPage'?: (number);
}
export interface GetQuestionsRequest__Output {
    'surveyGuid'?: (string);
    'filter'?: (string);
    'versionGuid'?: (string);
    'includeDeleted'?: (boolean);
    'page'?: (number);
    'resultsPerPage'?: (number);
}
