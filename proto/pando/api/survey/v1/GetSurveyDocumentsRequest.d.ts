export interface GetSurveyDocumentsRequest {
    'surveyGuid'?: (string);
}
export interface GetSurveyDocumentsRequest__Output {
    'surveyGuid'?: (string);
}
