// Original file: proto/pando/api/survey/v1/admin.proto


export interface GetSurveyDocumentsRequest {
  'surveyGuid'?: (string);
}

export interface GetSurveyDocumentsRequest__Output {
  'surveyGuid'?: (string);
}
