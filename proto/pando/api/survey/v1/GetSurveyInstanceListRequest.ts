// Original file: proto/pando/api/survey/v1/survey.proto

import type { Timestamp as _google_protobuf_Timestamp, Timestamp__Output as _google_protobuf_Timestamp__Output } from '../../../../google/protobuf/Timestamp';

export interface GetSurveyInstanceListRequest {
  'start'?: (_google_protobuf_Timestamp | null);
  'end'?: (_google_protobuf_Timestamp | null);
  'page'?: (number);
  'resultsPerPage'?: (number);
}

export interface GetSurveyInstanceListRequest__Output {
  'start'?: (_google_protobuf_Timestamp__Output);
  'end'?: (_google_protobuf_Timestamp__Output);
  'page'?: (number);
  'resultsPerPage'?: (number);
}
