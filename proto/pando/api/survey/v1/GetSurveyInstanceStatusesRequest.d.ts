export interface GetSurveyInstanceStatusesRequest {
    'organizationCode'?: (string);
    'isChartDataIncluded'?: (boolean);
    'isCompletedOnly'?: (boolean);
    'page'?: (number);
    'resultsPerPage'?: (number);
}
export interface GetSurveyInstanceStatusesRequest__Output {
    'organizationCode'?: (string);
    'isChartDataIncluded'?: (boolean);
    'isCompletedOnly'?: (boolean);
    'page'?: (number);
    'resultsPerPage'?: (number);
}
