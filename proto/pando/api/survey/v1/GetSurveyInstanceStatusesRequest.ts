// Original file: proto/pando/api/survey/v1/reports.proto


export interface GetSurveyInstanceStatusesRequest {
  'organizationCode'?: (string);
  'isChartDataIncluded'?: (boolean);
  'isCompletedOnly'?: (boolean);
  'page'?: (number);
  'resultsPerPage'?: (number);
}

export interface GetSurveyInstanceStatusesRequest__Output {
  'organizationCode'?: (string);
  'isChartDataIncluded'?: (boolean);
  'isCompletedOnly'?: (boolean);
  'page'?: (number);
  'resultsPerPage'?: (number);
}
