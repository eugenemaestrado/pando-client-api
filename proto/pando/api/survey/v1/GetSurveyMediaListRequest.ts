// Original file: proto/pando/api/survey/v1/admin.proto


export interface GetSurveyMediaListRequest {
  'surveyGuid'?: (string);
  'filter'?: (string);
  'versionGuid'?: (string);
  'includeDeleted'?: (boolean);
  'page'?: (number);
  'resultsPerPage'?: (number);
}

export interface GetSurveyMediaListRequest__Output {
  'surveyGuid'?: (string);
  'filter'?: (string);
  'versionGuid'?: (string);
  'includeDeleted'?: (boolean);
  'page'?: (number);
  'resultsPerPage'?: (number);
}
