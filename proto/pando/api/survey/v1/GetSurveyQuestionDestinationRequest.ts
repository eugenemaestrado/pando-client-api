// Original file: proto/pando/api/survey/v1/admin.proto


export interface GetSurveyQuestionDestinationRequest {
  'guid'?: (string);
}

export interface GetSurveyQuestionDestinationRequest__Output {
  'guid'?: (string);
}
