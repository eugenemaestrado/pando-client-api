export interface GetSurveyQuestionDestinationsRequest {
    'questionGuid'?: (string);
}
export interface GetSurveyQuestionDestinationsRequest__Output {
    'questionGuid'?: (string);
}
