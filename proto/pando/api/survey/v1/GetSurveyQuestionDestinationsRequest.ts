// Original file: proto/pando/api/survey/v1/admin.proto


export interface GetSurveyQuestionDestinationsRequest {
  'questionGuid'?: (string);
}

export interface GetSurveyQuestionDestinationsRequest__Output {
  'questionGuid'?: (string);
}
