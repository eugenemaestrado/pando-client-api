export interface GetSurveyRequest {
    'surveyGuid'?: (string);
    'versionGuid'?: (string);
}
export interface GetSurveyRequest__Output {
    'surveyGuid'?: (string);
    'versionGuid'?: (string);
}
