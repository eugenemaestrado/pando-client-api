// Original file: proto/pando/api/survey/v1/admin.proto


export interface GetSurveyRequest {
  'surveyGuid'?: (string);
  'versionGuid'?: (string);
}

export interface GetSurveyRequest__Output {
  'surveyGuid'?: (string);
  'versionGuid'?: (string);
}
