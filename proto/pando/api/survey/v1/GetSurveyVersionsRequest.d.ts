export interface GetSurveyVersionsRequest {
    'surveyGuid'?: (string);
    'filter'?: (string);
    'includeDeleted'?: (boolean);
    'page'?: (number);
    'resultsPerPage'?: (number);
}
export interface GetSurveyVersionsRequest__Output {
    'surveyGuid'?: (string);
    'filter'?: (string);
    'includeDeleted'?: (boolean);
    'page'?: (number);
    'resultsPerPage'?: (number);
}
