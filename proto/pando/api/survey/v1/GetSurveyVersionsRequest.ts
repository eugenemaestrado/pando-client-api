// Original file: proto/pando/api/survey/v1/admin.proto


export interface GetSurveyVersionsRequest {
  'surveyGuid'?: (string);
  'filter'?: (string);
  'includeDeleted'?: (boolean);
  'page'?: (number);
  'resultsPerPage'?: (number);
}

export interface GetSurveyVersionsRequest__Output {
  'surveyGuid'?: (string);
  'filter'?: (string);
  'includeDeleted'?: (boolean);
  'page'?: (number);
  'resultsPerPage'?: (number);
}
