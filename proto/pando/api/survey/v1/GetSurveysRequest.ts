// Original file: proto/pando/api/survey/v1/admin.proto


export interface GetSurveysRequest {
  'organizationCode'?: (string);
  'filter'?: (string);
  'includeDeleted'?: (boolean);
  'includeOldVersions'?: (boolean);
  'includeUnpublishedVersions'?: (boolean);
  'page'?: (number);
  'resultsPerPage'?: (number);
}

export interface GetSurveysRequest__Output {
  'organizationCode'?: (string);
  'filter'?: (string);
  'includeDeleted'?: (boolean);
  'includeOldVersions'?: (boolean);
  'includeUnpublishedVersions'?: (boolean);
  'page'?: (number);
  'resultsPerPage'?: (number);
}
