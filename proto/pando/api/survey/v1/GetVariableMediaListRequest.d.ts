export interface GetVariableMediaListRequest {
    'variableGuid'?: (string);
    'filter'?: (string);
    'includeDeleted'?: (boolean);
    'page'?: (number);
    'resultsPerPage'?: (number);
}
export interface GetVariableMediaListRequest__Output {
    'variableGuid'?: (string);
    'filter'?: (string);
    'includeDeleted'?: (boolean);
    'page'?: (number);
    'resultsPerPage'?: (number);
}
