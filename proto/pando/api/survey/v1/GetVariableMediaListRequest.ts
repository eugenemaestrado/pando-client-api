// Original file: proto/pando/api/survey/v1/admin.proto


export interface GetVariableMediaListRequest {
  'variableGuid'?: (string);
  'filter'?: (string);
  'includeDeleted'?: (boolean);
  'page'?: (number);
  'resultsPerPage'?: (number);
}

export interface GetVariableMediaListRequest__Output {
  'variableGuid'?: (string);
  'filter'?: (string);
  'includeDeleted'?: (boolean);
  'page'?: (number);
  'resultsPerPage'?: (number);
}
