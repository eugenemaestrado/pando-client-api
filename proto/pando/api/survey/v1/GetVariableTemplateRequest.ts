// Original file: proto/pando/api/survey/v1/admin.proto

import type { FileType as _pando_api_FileType } from '../../../../pando/api/FileType';

export interface GetVariableTemplateRequest {
  'surveyGuid'?: (string);
  'versionGuid'?: (string);
  'dataFormat'?: (_pando_api_FileType | keyof typeof _pando_api_FileType);
}

export interface GetVariableTemplateRequest__Output {
  'surveyGuid'?: (string);
  'versionGuid'?: (string);
  'dataFormat'?: (_pando_api_FileType);
}
