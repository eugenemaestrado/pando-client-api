export interface GetVariablesRequest {
    'organizationCode'?: (string);
    'filter'?: (string);
    'includeDeleted'?: (boolean);
    'page'?: (number);
    'resultsPerPage'?: (number);
}
export interface GetVariablesRequest__Output {
    'organizationCode'?: (string);
    'filter'?: (string);
    'includeDeleted'?: (boolean);
    'page'?: (number);
    'resultsPerPage'?: (number);
}
