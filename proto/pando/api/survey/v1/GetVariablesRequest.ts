// Original file: proto/pando/api/survey/v1/admin.proto


export interface GetVariablesRequest {
  'organizationCode'?: (string);
  'filter'?: (string);
  'includeDeleted'?: (boolean);
  'page'?: (number);
  'resultsPerPage'?: (number);
}

export interface GetVariablesRequest__Output {
  'organizationCode'?: (string);
  'filter'?: (string);
  'includeDeleted'?: (boolean);
  'page'?: (number);
  'resultsPerPage'?: (number);
}
