// Original file: proto/pando/api/survey/v1/shared.proto

import type { ContactType as _pando_api_ContactType } from '../../../../pando/api/ContactType';
import type { Attempt as _pando_api_survey_v1_Attempt, Attempt__Output as _pando_api_survey_v1_Attempt__Output } from '../../../../pando/api/survey/v1/Attempt';
import type { FileType as _pando_api_FileType } from '../../../../pando/api/FileType';
import type { CompletionType as _pando_api_survey_v1_CompletionType } from '../../../../pando/api/survey/v1/CompletionType';
import type { Timestamp as _google_protobuf_Timestamp, Timestamp__Output as _google_protobuf_Timestamp__Output } from '../../../../google/protobuf/Timestamp';

export interface Instance {
  'guid'?: (string);
  'surveyGuid'?: (string);
  'contactType'?: (_pando_api_ContactType | keyof typeof _pando_api_ContactType);
  'contactInfo'?: (string);
  'current'?: (_pando_api_survey_v1_Attempt | null);
  'attemptGuids'?: (string)[];
  'questionGuids'?: (string)[];
  'mediaGuids'?: (string)[];
  'isDeleted'?: (boolean);
  'data'?: (string);
  'dataFormat'?: (_pando_api_FileType | keyof typeof _pando_api_FileType);
  'respondentName'?: (string);
  'organizationName'?: (string);
  'completionType'?: (_pando_api_survey_v1_CompletionType | keyof typeof _pando_api_survey_v1_CompletionType);
  'dateCompleted'?: (_google_protobuf_Timestamp | null);
  'surveyName'?: (string);
  'preSurveyText'?: (string);
  'dateCreated'?: (_google_protobuf_Timestamp | null);
  'signatoryGuid'?: (string);
  'userIds'?: (string)[];
  'description'?: (string);
}

export interface Instance__Output {
  'guid'?: (string);
  'surveyGuid'?: (string);
  'contactType'?: (_pando_api_ContactType);
  'contactInfo'?: (string);
  'current'?: (_pando_api_survey_v1_Attempt__Output);
  'attemptGuids'?: (string)[];
  'questionGuids'?: (string)[];
  'mediaGuids'?: (string)[];
  'isDeleted'?: (boolean);
  'data'?: (string);
  'dataFormat'?: (_pando_api_FileType);
  'respondentName'?: (string);
  'organizationName'?: (string);
  'completionType'?: (_pando_api_survey_v1_CompletionType);
  'dateCompleted'?: (_google_protobuf_Timestamp__Output);
  'surveyName'?: (string);
  'preSurveyText'?: (string);
  'dateCreated'?: (_google_protobuf_Timestamp__Output);
  'signatoryGuid'?: (string);
  'userIds'?: (string)[];
  'description'?: (string);
}
