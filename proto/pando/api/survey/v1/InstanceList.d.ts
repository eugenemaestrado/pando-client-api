import type { Instance as _pando_api_survey_v1_Instance, Instance__Output as _pando_api_survey_v1_Instance__Output } from '../../../../pando/api/survey/v1/Instance';
import type { PaginationResult as _pando_api_PaginationResult, PaginationResult__Output as _pando_api_PaginationResult__Output } from '../../../../pando/api/PaginationResult';
export interface InstanceList {
    'instances'?: (_pando_api_survey_v1_Instance)[];
    'pagination'?: (_pando_api_PaginationResult | null);
}
export interface InstanceList__Output {
    'instances'?: (_pando_api_survey_v1_Instance__Output)[];
    'pagination'?: (_pando_api_PaginationResult__Output);
}
