import type { FileType as _pando_api_FileType } from '../../../../pando/api/FileType';
import type { MediaLocation as _pando_api_survey_v1_MediaLocation } from '../../../../pando/api/survey/v1/MediaLocation';
export interface Media {
    'guid'?: (string);
    'type'?: (_pando_api_FileType | keyof typeof _pando_api_FileType);
    'name'?: (string);
    'location'?: (_pando_api_survey_v1_MediaLocation | keyof typeof _pando_api_survey_v1_MediaLocation);
    'path'?: (string);
}
export interface Media__Output {
    'guid'?: (string);
    'type'?: (_pando_api_FileType);
    'name'?: (string);
    'location'?: (_pando_api_survey_v1_MediaLocation);
    'path'?: (string);
}
