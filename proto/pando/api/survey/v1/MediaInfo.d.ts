import type { FileType as _pando_api_FileType } from '../../../../pando/api/FileType';
export interface MediaInfo {
    'name'?: (string);
    'type'?: (_pando_api_FileType | keyof typeof _pando_api_FileType);
}
export interface MediaInfo__Output {
    'name'?: (string);
    'type'?: (_pando_api_FileType);
}
