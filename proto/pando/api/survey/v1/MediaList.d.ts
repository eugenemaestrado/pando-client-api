import type { Media as _pando_api_survey_v1_Media, Media__Output as _pando_api_survey_v1_Media__Output } from '../../../../pando/api/survey/v1/Media';
import type { PaginationResult as _pando_api_PaginationResult, PaginationResult__Output as _pando_api_PaginationResult__Output } from '../../../../pando/api/PaginationResult';
export interface MediaList {
    'mediaList'?: (_pando_api_survey_v1_Media)[];
    'pagination'?: (_pando_api_PaginationResult | null);
}
export interface MediaList__Output {
    'mediaList'?: (_pando_api_survey_v1_Media__Output)[];
    'pagination'?: (_pando_api_PaginationResult__Output);
}
