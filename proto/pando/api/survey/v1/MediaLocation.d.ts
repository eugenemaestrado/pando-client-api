export declare enum MediaLocation {
    LOCAL = 0,
    NETWORK = 1,
    URL = 2
}
