"use strict";
// Original file: proto/pando/api/survey/v1/shared.proto
Object.defineProperty(exports, "__esModule", { value: true });
exports.MediaLocation = void 0;
var MediaLocation;
(function (MediaLocation) {
    MediaLocation[MediaLocation["LOCAL"] = 0] = "LOCAL";
    MediaLocation[MediaLocation["NETWORK"] = 1] = "NETWORK";
    MediaLocation[MediaLocation["URL"] = 2] = "URL";
})(MediaLocation = exports.MediaLocation || (exports.MediaLocation = {}));
