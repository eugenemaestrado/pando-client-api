// Original file: proto/pando/api/survey/v1/shared.proto

export enum MediaLocation {
  LOCAL = 0,
  NETWORK = 1,
  URL = 2,
}
