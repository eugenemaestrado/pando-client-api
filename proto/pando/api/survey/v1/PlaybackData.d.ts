import type { Action as _pando_api_survey_v1_Action } from '../../../../pando/api/survey/v1/Action';
import type { FileType as _pando_api_FileType } from '../../../../pando/api/FileType';
export interface PlaybackData {
    'guid'?: (string);
    'questionGuid'?: (string);
    'type'?: (_pando_api_survey_v1_Action | keyof typeof _pando_api_survey_v1_Action);
    'mediaGuid'?: (string);
    'timestamp'?: (number);
    'isDeleted'?: (boolean);
    'fileType'?: (_pando_api_FileType | keyof typeof _pando_api_FileType);
    'variableGuid'?: (string);
    'mediaName'?: (string);
}
export interface PlaybackData__Output {
    'guid'?: (string);
    'questionGuid'?: (string);
    'type'?: (_pando_api_survey_v1_Action);
    'mediaGuid'?: (string);
    'timestamp'?: (number);
    'isDeleted'?: (boolean);
    'fileType'?: (_pando_api_FileType);
    'variableGuid'?: (string);
    'mediaName'?: (string);
}
