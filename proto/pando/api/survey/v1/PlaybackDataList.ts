// Original file: proto/pando/api/survey/v1/shared.proto

import type { PlaybackData as _pando_api_survey_v1_PlaybackData, PlaybackData__Output as _pando_api_survey_v1_PlaybackData__Output } from '../../../../pando/api/survey/v1/PlaybackData';
import type { PaginationResult as _pando_api_PaginationResult, PaginationResult__Output as _pando_api_PaginationResult__Output } from '../../../../pando/api/PaginationResult';

export interface PlaybackDataList {
  'playbackDataList'?: (_pando_api_survey_v1_PlaybackData)[];
  'pagination'?: (_pando_api_PaginationResult | null);
}

export interface PlaybackDataList__Output {
  'playbackDataList'?: (_pando_api_survey_v1_PlaybackData__Output)[];
  'pagination'?: (_pando_api_PaginationResult__Output);
}
