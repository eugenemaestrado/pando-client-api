import type * as grpc from '@grpc/grpc-js';
import type { MethodDefinition } from '@grpc/proto-loader';
import type { GetNextQuestionPreviewRequest as _pando_api_survey_v1_GetNextQuestionPreviewRequest, GetNextQuestionPreviewRequest__Output as _pando_api_survey_v1_GetNextQuestionPreviewRequest__Output } from '../../../../pando/api/survey/v1/GetNextQuestionPreviewRequest';
import type { GetNextQuestionPreviewResponse as _pando_api_survey_v1_GetNextQuestionPreviewResponse, GetNextQuestionPreviewResponse__Output as _pando_api_survey_v1_GetNextQuestionPreviewResponse__Output } from '../../../../pando/api/survey/v1/GetNextQuestionPreviewResponse';
export interface PreviewServiceClient extends grpc.Client {
    GetNextQuestionPreview(argument: _pando_api_survey_v1_GetNextQuestionPreviewRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_GetNextQuestionPreviewResponse__Output>): grpc.ClientUnaryCall;
    GetNextQuestionPreview(argument: _pando_api_survey_v1_GetNextQuestionPreviewRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_GetNextQuestionPreviewResponse__Output>): grpc.ClientUnaryCall;
    GetNextQuestionPreview(argument: _pando_api_survey_v1_GetNextQuestionPreviewRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_GetNextQuestionPreviewResponse__Output>): grpc.ClientUnaryCall;
    GetNextQuestionPreview(argument: _pando_api_survey_v1_GetNextQuestionPreviewRequest, callback: grpc.requestCallback<_pando_api_survey_v1_GetNextQuestionPreviewResponse__Output>): grpc.ClientUnaryCall;
    getNextQuestionPreview(argument: _pando_api_survey_v1_GetNextQuestionPreviewRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_GetNextQuestionPreviewResponse__Output>): grpc.ClientUnaryCall;
    getNextQuestionPreview(argument: _pando_api_survey_v1_GetNextQuestionPreviewRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_GetNextQuestionPreviewResponse__Output>): grpc.ClientUnaryCall;
    getNextQuestionPreview(argument: _pando_api_survey_v1_GetNextQuestionPreviewRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_GetNextQuestionPreviewResponse__Output>): grpc.ClientUnaryCall;
    getNextQuestionPreview(argument: _pando_api_survey_v1_GetNextQuestionPreviewRequest, callback: grpc.requestCallback<_pando_api_survey_v1_GetNextQuestionPreviewResponse__Output>): grpc.ClientUnaryCall;
}
export interface PreviewServiceHandlers extends grpc.UntypedServiceImplementation {
    GetNextQuestionPreview: grpc.handleUnaryCall<_pando_api_survey_v1_GetNextQuestionPreviewRequest__Output, _pando_api_survey_v1_GetNextQuestionPreviewResponse>;
}
export interface PreviewServiceDefinition extends grpc.ServiceDefinition {
    GetNextQuestionPreview: MethodDefinition<_pando_api_survey_v1_GetNextQuestionPreviewRequest, _pando_api_survey_v1_GetNextQuestionPreviewResponse, _pando_api_survey_v1_GetNextQuestionPreviewRequest__Output, _pando_api_survey_v1_GetNextQuestionPreviewResponse__Output>;
}
