import type { SelectionType as _pando_api_survey_v1_SelectionType } from '../../../../pando/api/survey/v1/SelectionType';
import type { AnswerOption as _pando_api_survey_v1_AnswerOption, AnswerOption__Output as _pando_api_survey_v1_AnswerOption__Output } from '../../../../pando/api/survey/v1/AnswerOption';
export interface Question {
    'guid'?: (string);
    'surveyGuid'?: (string);
    'name'?: (string);
    'description'?: (string);
    'text'?: (string);
    'selectionType'?: (_pando_api_survey_v1_SelectionType | keyof typeof _pando_api_survey_v1_SelectionType);
    'answerOptions'?: (_pando_api_survey_v1_AnswerOption)[];
    'isDeleted'?: (boolean);
    'isOptional'?: (boolean);
}
export interface Question__Output {
    'guid'?: (string);
    'surveyGuid'?: (string);
    'name'?: (string);
    'description'?: (string);
    'text'?: (string);
    'selectionType'?: (_pando_api_survey_v1_SelectionType);
    'answerOptions'?: (_pando_api_survey_v1_AnswerOption__Output)[];
    'isDeleted'?: (boolean);
    'isOptional'?: (boolean);
}
