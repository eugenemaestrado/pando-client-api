/// <reference types="long" />
import type { PlaybackData as _pando_api_survey_v1_PlaybackData, PlaybackData__Output as _pando_api_survey_v1_PlaybackData__Output } from '../../../../pando/api/survey/v1/PlaybackData';
import type { AnswerOption as _pando_api_survey_v1_AnswerOption, AnswerOption__Output as _pando_api_survey_v1_AnswerOption__Output } from '../../../../pando/api/survey/v1/AnswerOption';
import type { SelectionType as _pando_api_survey_v1_SelectionType } from '../../../../pando/api/survey/v1/SelectionType';
import type { Long } from '@grpc/proto-loader';
export interface QuestionWithPlaybackData {
    'guid'?: (string);
    'surveyGuid'?: (string);
    'name'?: (string);
    'description'?: (string);
    'text'?: (string);
    'playbackData'?: (_pando_api_survey_v1_PlaybackData)[];
    'answerOptions'?: (_pando_api_survey_v1_AnswerOption)[];
    'selectionType'?: (_pando_api_survey_v1_SelectionType | keyof typeof _pando_api_survey_v1_SelectionType);
    'facialRecognitionInterval'?: (number | string | Long);
    'noAnswerWaitTimeout'?: (number | string | Long);
    'totalQuestions'?: (number | string | Long);
    'currentQuestion'?: (number | string | Long);
}
export interface QuestionWithPlaybackData__Output {
    'guid'?: (string);
    'surveyGuid'?: (string);
    'name'?: (string);
    'description'?: (string);
    'text'?: (string);
    'playbackData'?: (_pando_api_survey_v1_PlaybackData__Output)[];
    'answerOptions'?: (_pando_api_survey_v1_AnswerOption__Output)[];
    'selectionType'?: (_pando_api_survey_v1_SelectionType);
    'facialRecognitionInterval'?: (Long);
    'noAnswerWaitTimeout'?: (Long);
    'totalQuestions'?: (Long);
    'currentQuestion'?: (Long);
}
