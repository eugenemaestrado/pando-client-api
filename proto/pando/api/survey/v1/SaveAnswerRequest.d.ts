import type { AnswerType as _pando_api_survey_v1_AnswerType } from '../../../../pando/api/survey/v1/AnswerType';
import type { AnswerAction as _pando_api_survey_v1_AnswerAction } from '../../../../pando/api/survey/v1/AnswerAction';
export interface SaveAnswerRequest {
    'answerGuid'?: (string);
    'text'?: (string);
    'answerType'?: (_pando_api_survey_v1_AnswerType | keyof typeof _pando_api_survey_v1_AnswerType);
    'answerAction'?: (_pando_api_survey_v1_AnswerAction | keyof typeof _pando_api_survey_v1_AnswerAction);
    'timestamp'?: (number);
}
export interface SaveAnswerRequest__Output {
    'answerGuid'?: (string);
    'text'?: (string);
    'answerType'?: (_pando_api_survey_v1_AnswerType);
    'answerAction'?: (_pando_api_survey_v1_AnswerAction);
    'timestamp'?: (number);
}
