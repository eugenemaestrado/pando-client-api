"use strict";
// Original file: proto/pando/api/survey/v1/shared.proto
Object.defineProperty(exports, "__esModule", { value: true });
exports.SelectionType = void 0;
var SelectionType;
(function (SelectionType) {
    SelectionType[SelectionType["SINGLE_ANSWER"] = 0] = "SINGLE_ANSWER";
    SelectionType[SelectionType["MULTI_ANSWER"] = 1] = "MULTI_ANSWER";
})(SelectionType = exports.SelectionType || (exports.SelectionType = {}));
