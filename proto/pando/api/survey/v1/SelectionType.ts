// Original file: proto/pando/api/survey/v1/shared.proto

export enum SelectionType {
  SINGLE_ANSWER = 0,
  MULTI_ANSWER = 1,
}
