// Original file: proto/pando/api/survey/v1/admin.proto

import type { ContactType as _pando_api_ContactType } from '../../../../pando/api/ContactType';

export interface SendInstanceLink {
  'instanceGuid'?: (string);
  'contactType'?: (_pando_api_ContactType | keyof typeof _pando_api_ContactType);
  'contactInfo'?: (string);
}

export interface SendInstanceLink__Output {
  'instanceGuid'?: (string);
  'contactType'?: (_pando_api_ContactType);
  'contactInfo'?: (string);
}
