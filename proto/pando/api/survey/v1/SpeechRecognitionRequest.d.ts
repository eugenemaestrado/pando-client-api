/// <reference types="node" />
export interface SpeechRecognitionRequest {
    'bytes'?: (Buffer | Uint8Array | string);
}
export interface SpeechRecognitionRequest__Output {
    'bytes'?: (Buffer);
}
