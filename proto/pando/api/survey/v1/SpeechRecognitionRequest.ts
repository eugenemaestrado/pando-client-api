// Original file: proto/pando/api/survey/v1/survey.proto


export interface SpeechRecognitionRequest {
  'bytes'?: (Buffer | Uint8Array | string);
}

export interface SpeechRecognitionRequest__Output {
  'bytes'?: (Buffer);
}
