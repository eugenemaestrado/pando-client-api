// Original file: proto/pando/api/survey/v1/survey.proto


export interface SpeechRecognitionResponse {
  'recognized'?: (boolean);
  'recognizedText'?: (string);
  'isYes'?: (boolean);
  'isNo'?: (boolean);
}

export interface SpeechRecognitionResponse__Output {
  'recognized'?: (boolean);
  'recognizedText'?: (string);
  'isYes'?: (boolean);
  'isNo'?: (boolean);
}
