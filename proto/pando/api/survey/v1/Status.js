"use strict";
// Original file: proto/pando/api/survey/v1/shared.proto
Object.defineProperty(exports, "__esModule", { value: true });
exports.Status = void 0;
var Status;
(function (Status) {
    Status[Status["PENDING"] = 0] = "PENDING";
    Status[Status["IN_PROGRESS"] = 1] = "IN_PROGRESS";
    Status[Status["SUCCESS"] = 2] = "SUCCESS";
    Status[Status["FAILED"] = 3] = "FAILED";
})(Status = exports.Status || (exports.Status = {}));
