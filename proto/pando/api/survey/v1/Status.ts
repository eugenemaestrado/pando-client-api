// Original file: proto/pando/api/survey/v1/shared.proto

export enum Status {
  PENDING = 0,
  IN_PROGRESS = 1,
  SUCCESS = 2,
  FAILED = 3,
}
