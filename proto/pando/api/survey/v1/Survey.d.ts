import type { Timestamp as _google_protobuf_Timestamp, Timestamp__Output as _google_protobuf_Timestamp__Output } from '../../../../google/protobuf/Timestamp';
export interface Survey {
    'guid'?: (string);
    'organizationCode'?: (string);
    'name'?: (string);
    'description'?: (string);
    'preSurveyText'?: (string);
    'firstQuestionGuid'?: (string);
    'isDeleted'?: (boolean);
    'organizationName'?: (string);
    'versionGuid'?: (string);
    'numberOfInstances'?: (number);
    'datePublished'?: (_google_protobuf_Timestamp | null);
}
export interface Survey__Output {
    'guid'?: (string);
    'organizationCode'?: (string);
    'name'?: (string);
    'description'?: (string);
    'preSurveyText'?: (string);
    'firstQuestionGuid'?: (string);
    'isDeleted'?: (boolean);
    'organizationName'?: (string);
    'versionGuid'?: (string);
    'numberOfInstances'?: (number);
    'datePublished'?: (_google_protobuf_Timestamp__Output);
}
