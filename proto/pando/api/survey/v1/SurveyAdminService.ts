// Original file: proto/pando/api/survey/v1/admin.proto

import type * as grpc from '@grpc/grpc-js'
import type { MethodDefinition } from '@grpc/proto-loader'
import type { AnswerOption as _pando_api_survey_v1_AnswerOption, AnswerOption__Output as _pando_api_survey_v1_AnswerOption__Output } from '../../../../pando/api/survey/v1/AnswerOption';
import type { AnswerOptionList as _pando_api_survey_v1_AnswerOptionList, AnswerOptionList__Output as _pando_api_survey_v1_AnswerOptionList__Output } from '../../../../pando/api/survey/v1/AnswerOptionList';
import type { CreateInstanceRequest as _pando_api_survey_v1_CreateInstanceRequest, CreateInstanceRequest__Output as _pando_api_survey_v1_CreateInstanceRequest__Output } from '../../../../pando/api/survey/v1/CreateInstanceRequest';
import type { CreateSurveyVersionRequest as _pando_api_survey_v1_CreateSurveyVersionRequest, CreateSurveyVersionRequest__Output as _pando_api_survey_v1_CreateSurveyVersionRequest__Output } from '../../../../pando/api/survey/v1/CreateSurveyVersionRequest';
import type { CreateVariablesRequest as _pando_api_survey_v1_CreateVariablesRequest, CreateVariablesRequest__Output as _pando_api_survey_v1_CreateVariablesRequest__Output } from '../../../../pando/api/survey/v1/CreateVariablesRequest';
import type { DeleteAnswerOptionRequest as _pando_api_survey_v1_DeleteAnswerOptionRequest, DeleteAnswerOptionRequest__Output as _pando_api_survey_v1_DeleteAnswerOptionRequest__Output } from '../../../../pando/api/survey/v1/DeleteAnswerOptionRequest';
import type { DeleteInstanceRequest as _pando_api_survey_v1_DeleteInstanceRequest, DeleteInstanceRequest__Output as _pando_api_survey_v1_DeleteInstanceRequest__Output } from '../../../../pando/api/survey/v1/DeleteInstanceRequest';
import type { DeleteMediaRequest as _pando_api_survey_v1_DeleteMediaRequest, DeleteMediaRequest__Output as _pando_api_survey_v1_DeleteMediaRequest__Output } from '../../../../pando/api/survey/v1/DeleteMediaRequest';
import type { DeletePlaybackDataRequest as _pando_api_survey_v1_DeletePlaybackDataRequest, DeletePlaybackDataRequest__Output as _pando_api_survey_v1_DeletePlaybackDataRequest__Output } from '../../../../pando/api/survey/v1/DeletePlaybackDataRequest';
import type { DeleteQuestionRequest as _pando_api_survey_v1_DeleteQuestionRequest, DeleteQuestionRequest__Output as _pando_api_survey_v1_DeleteQuestionRequest__Output } from '../../../../pando/api/survey/v1/DeleteQuestionRequest';
import type { DeleteSurveyMediaRequest as _pando_api_survey_v1_DeleteSurveyMediaRequest, DeleteSurveyMediaRequest__Output as _pando_api_survey_v1_DeleteSurveyMediaRequest__Output } from '../../../../pando/api/survey/v1/DeleteSurveyMediaRequest';
import type { DeleteSurveyQuestionDestinationRequest as _pando_api_survey_v1_DeleteSurveyQuestionDestinationRequest, DeleteSurveyQuestionDestinationRequest__Output as _pando_api_survey_v1_DeleteSurveyQuestionDestinationRequest__Output } from '../../../../pando/api/survey/v1/DeleteSurveyQuestionDestinationRequest';
import type { DeleteSurveyRequest as _pando_api_survey_v1_DeleteSurveyRequest, DeleteSurveyRequest__Output as _pando_api_survey_v1_DeleteSurveyRequest__Output } from '../../../../pando/api/survey/v1/DeleteSurveyRequest';
import type { DeleteSurveyVariableRequest as _pando_api_survey_v1_DeleteSurveyVariableRequest, DeleteSurveyVariableRequest__Output as _pando_api_survey_v1_DeleteSurveyVariableRequest__Output } from '../../../../pando/api/survey/v1/DeleteSurveyVariableRequest';
import type { DeleteSurveyVersionRequest as _pando_api_survey_v1_DeleteSurveyVersionRequest, DeleteSurveyVersionRequest__Output as _pando_api_survey_v1_DeleteSurveyVersionRequest__Output } from '../../../../pando/api/survey/v1/DeleteSurveyVersionRequest';
import type { DeleteVariableMediaRequest as _pando_api_survey_v1_DeleteVariableMediaRequest, DeleteVariableMediaRequest__Output as _pando_api_survey_v1_DeleteVariableMediaRequest__Output } from '../../../../pando/api/survey/v1/DeleteVariableMediaRequest';
import type { DeleteVariableRequest as _pando_api_survey_v1_DeleteVariableRequest, DeleteVariableRequest__Output as _pando_api_survey_v1_DeleteVariableRequest__Output } from '../../../../pando/api/survey/v1/DeleteVariableRequest';
import type { Empty as _pando_api_Empty, Empty__Output as _pando_api_Empty__Output } from '../../../../pando/api/Empty';
import type { FileChunk as _pando_api_FileChunk, FileChunk__Output as _pando_api_FileChunk__Output } from '../../../../pando/api/FileChunk';
import type { GetAnswerOptionRequest as _pando_api_survey_v1_GetAnswerOptionRequest, GetAnswerOptionRequest__Output as _pando_api_survey_v1_GetAnswerOptionRequest__Output } from '../../../../pando/api/survey/v1/GetAnswerOptionRequest';
import type { GetAnswerOptionsRequest as _pando_api_survey_v1_GetAnswerOptionsRequest, GetAnswerOptionsRequest__Output as _pando_api_survey_v1_GetAnswerOptionsRequest__Output } from '../../../../pando/api/survey/v1/GetAnswerOptionsRequest';
import type { GetPlaybackDataListRequest as _pando_api_survey_v1_GetPlaybackDataListRequest, GetPlaybackDataListRequest__Output as _pando_api_survey_v1_GetPlaybackDataListRequest__Output } from '../../../../pando/api/survey/v1/GetPlaybackDataListRequest';
import type { GetQuestionRequest as _pando_api_survey_v1_GetQuestionRequest, GetQuestionRequest__Output as _pando_api_survey_v1_GetQuestionRequest__Output } from '../../../../pando/api/survey/v1/GetQuestionRequest';
import type { GetQuestionsRequest as _pando_api_survey_v1_GetQuestionsRequest, GetQuestionsRequest__Output as _pando_api_survey_v1_GetQuestionsRequest__Output } from '../../../../pando/api/survey/v1/GetQuestionsRequest';
import type { GetSurveyDocumentsRequest as _pando_api_survey_v1_GetSurveyDocumentsRequest, GetSurveyDocumentsRequest__Output as _pando_api_survey_v1_GetSurveyDocumentsRequest__Output } from '../../../../pando/api/survey/v1/GetSurveyDocumentsRequest';
import type { GetSurveyMediaListRequest as _pando_api_survey_v1_GetSurveyMediaListRequest, GetSurveyMediaListRequest__Output as _pando_api_survey_v1_GetSurveyMediaListRequest__Output } from '../../../../pando/api/survey/v1/GetSurveyMediaListRequest';
import type { GetSurveyQuestionDestinationRequest as _pando_api_survey_v1_GetSurveyQuestionDestinationRequest, GetSurveyQuestionDestinationRequest__Output as _pando_api_survey_v1_GetSurveyQuestionDestinationRequest__Output } from '../../../../pando/api/survey/v1/GetSurveyQuestionDestinationRequest';
import type { GetSurveyQuestionDestinationsRequest as _pando_api_survey_v1_GetSurveyQuestionDestinationsRequest, GetSurveyQuestionDestinationsRequest__Output as _pando_api_survey_v1_GetSurveyQuestionDestinationsRequest__Output } from '../../../../pando/api/survey/v1/GetSurveyQuestionDestinationsRequest';
import type { GetSurveyRequest as _pando_api_survey_v1_GetSurveyRequest, GetSurveyRequest__Output as _pando_api_survey_v1_GetSurveyRequest__Output } from '../../../../pando/api/survey/v1/GetSurveyRequest';
import type { GetSurveyVariablesRequest as _pando_api_survey_v1_GetSurveyVariablesRequest, GetSurveyVariablesRequest__Output as _pando_api_survey_v1_GetSurveyVariablesRequest__Output } from '../../../../pando/api/survey/v1/GetSurveyVariablesRequest';
import type { GetSurveyVersionsRequest as _pando_api_survey_v1_GetSurveyVersionsRequest, GetSurveyVersionsRequest__Output as _pando_api_survey_v1_GetSurveyVersionsRequest__Output } from '../../../../pando/api/survey/v1/GetSurveyVersionsRequest';
import type { GetSurveysRequest as _pando_api_survey_v1_GetSurveysRequest, GetSurveysRequest__Output as _pando_api_survey_v1_GetSurveysRequest__Output } from '../../../../pando/api/survey/v1/GetSurveysRequest';
import type { GetVariableMediaListRequest as _pando_api_survey_v1_GetVariableMediaListRequest, GetVariableMediaListRequest__Output as _pando_api_survey_v1_GetVariableMediaListRequest__Output } from '../../../../pando/api/survey/v1/GetVariableMediaListRequest';
import type { GetVariableRequest as _pando_api_survey_v1_GetVariableRequest, GetVariableRequest__Output as _pando_api_survey_v1_GetVariableRequest__Output } from '../../../../pando/api/survey/v1/GetVariableRequest';
import type { GetVariableTemplateRequest as _pando_api_survey_v1_GetVariableTemplateRequest, GetVariableTemplateRequest__Output as _pando_api_survey_v1_GetVariableTemplateRequest__Output } from '../../../../pando/api/survey/v1/GetVariableTemplateRequest';
import type { GetVariableTemplateResponse as _pando_api_survey_v1_GetVariableTemplateResponse, GetVariableTemplateResponse__Output as _pando_api_survey_v1_GetVariableTemplateResponse__Output } from '../../../../pando/api/survey/v1/GetVariableTemplateResponse';
import type { GetVariablesRequest as _pando_api_survey_v1_GetVariablesRequest, GetVariablesRequest__Output as _pando_api_survey_v1_GetVariablesRequest__Output } from '../../../../pando/api/survey/v1/GetVariablesRequest';
import type { Instance as _pando_api_survey_v1_Instance, Instance__Output as _pando_api_survey_v1_Instance__Output } from '../../../../pando/api/survey/v1/Instance';
import type { MediaList as _pando_api_survey_v1_MediaList, MediaList__Output as _pando_api_survey_v1_MediaList__Output } from '../../../../pando/api/survey/v1/MediaList';
import type { PlaybackData as _pando_api_survey_v1_PlaybackData, PlaybackData__Output as _pando_api_survey_v1_PlaybackData__Output } from '../../../../pando/api/survey/v1/PlaybackData';
import type { PlaybackDataList as _pando_api_survey_v1_PlaybackDataList, PlaybackDataList__Output as _pando_api_survey_v1_PlaybackDataList__Output } from '../../../../pando/api/survey/v1/PlaybackDataList';
import type { Question as _pando_api_survey_v1_Question, Question__Output as _pando_api_survey_v1_Question__Output } from '../../../../pando/api/survey/v1/Question';
import type { SendInstanceLink as _pando_api_survey_v1_SendInstanceLink, SendInstanceLink__Output as _pando_api_survey_v1_SendInstanceLink__Output } from '../../../../pando/api/survey/v1/SendInstanceLink';
import type { Survey as _pando_api_survey_v1_Survey, Survey__Output as _pando_api_survey_v1_Survey__Output } from '../../../../pando/api/survey/v1/Survey';
import type { SurveyDetailList as _pando_api_survey_v1_SurveyDetailList, SurveyDetailList__Output as _pando_api_survey_v1_SurveyDetailList__Output } from '../../../../pando/api/survey/v1/SurveyDetailList';
import type { SurveyDocumentList as _pando_api_survey_v1_SurveyDocumentList, SurveyDocumentList__Output as _pando_api_survey_v1_SurveyDocumentList__Output } from '../../../../pando/api/survey/v1/SurveyDocumentList';
import type { SurveyESignTemplate as _pando_api_survey_v1_SurveyESignTemplate, SurveyESignTemplate__Output as _pando_api_survey_v1_SurveyESignTemplate__Output } from '../../../../pando/api/survey/v1/SurveyESignTemplate';
import type { SurveyInstanceESignSignatory as _pando_api_survey_v1_SurveyInstanceESignSignatory, SurveyInstanceESignSignatory__Output as _pando_api_survey_v1_SurveyInstanceESignSignatory__Output } from '../../../../pando/api/survey/v1/SurveyInstanceESignSignatory';
import type { SurveyList as _pando_api_survey_v1_SurveyList, SurveyList__Output as _pando_api_survey_v1_SurveyList__Output } from '../../../../pando/api/survey/v1/SurveyList';
import type { SurveyQuestionDestination as _pando_api_survey_v1_SurveyQuestionDestination, SurveyQuestionDestination__Output as _pando_api_survey_v1_SurveyQuestionDestination__Output } from '../../../../pando/api/survey/v1/SurveyQuestionDestination';
import type { SurveyQuestionDestinationList as _pando_api_survey_v1_SurveyQuestionDestinationList, SurveyQuestionDestinationList__Output as _pando_api_survey_v1_SurveyQuestionDestinationList__Output } from '../../../../pando/api/survey/v1/SurveyQuestionDestinationList';
import type { SurveyQuestionList as _pando_api_survey_v1_SurveyQuestionList, SurveyQuestionList__Output as _pando_api_survey_v1_SurveyQuestionList__Output } from '../../../../pando/api/survey/v1/SurveyQuestionList';
import type { SurveyVariable as _pando_api_survey_v1_SurveyVariable, SurveyVariable__Output as _pando_api_survey_v1_SurveyVariable__Output } from '../../../../pando/api/survey/v1/SurveyVariable';
import type { UpdateAnswerOptionRequest as _pando_api_survey_v1_UpdateAnswerOptionRequest, UpdateAnswerOptionRequest__Output as _pando_api_survey_v1_UpdateAnswerOptionRequest__Output } from '../../../../pando/api/survey/v1/UpdateAnswerOptionRequest';
import type { UpdatePlaybackDataRequest as _pando_api_survey_v1_UpdatePlaybackDataRequest, UpdatePlaybackDataRequest__Output as _pando_api_survey_v1_UpdatePlaybackDataRequest__Output } from '../../../../pando/api/survey/v1/UpdatePlaybackDataRequest';
import type { UpdateQuestionRequest as _pando_api_survey_v1_UpdateQuestionRequest, UpdateQuestionRequest__Output as _pando_api_survey_v1_UpdateQuestionRequest__Output } from '../../../../pando/api/survey/v1/UpdateQuestionRequest';
import type { UpdateSurveyQuestionDestinationRequest as _pando_api_survey_v1_UpdateSurveyQuestionDestinationRequest, UpdateSurveyQuestionDestinationRequest__Output as _pando_api_survey_v1_UpdateSurveyQuestionDestinationRequest__Output } from '../../../../pando/api/survey/v1/UpdateSurveyQuestionDestinationRequest';
import type { UpdateSurveyRequest as _pando_api_survey_v1_UpdateSurveyRequest, UpdateSurveyRequest__Output as _pando_api_survey_v1_UpdateSurveyRequest__Output } from '../../../../pando/api/survey/v1/UpdateSurveyRequest';
import type { UpdateVariableRequest as _pando_api_survey_v1_UpdateVariableRequest, UpdateVariableRequest__Output as _pando_api_survey_v1_UpdateVariableRequest__Output } from '../../../../pando/api/survey/v1/UpdateVariableRequest';
import type { UploadSurveyMediaResponse as _pando_api_survey_v1_UploadSurveyMediaResponse, UploadSurveyMediaResponse__Output as _pando_api_survey_v1_UploadSurveyMediaResponse__Output } from '../../../../pando/api/survey/v1/UploadSurveyMediaResponse';
import type { UploadVariableMediaResponse as _pando_api_survey_v1_UploadVariableMediaResponse, UploadVariableMediaResponse__Output as _pando_api_survey_v1_UploadVariableMediaResponse__Output } from '../../../../pando/api/survey/v1/UploadVariableMediaResponse';
import type { Variable as _pando_api_survey_v1_Variable, Variable__Output as _pando_api_survey_v1_Variable__Output } from '../../../../pando/api/survey/v1/Variable';
import type { VariableList as _pando_api_survey_v1_VariableList, VariableList__Output as _pando_api_survey_v1_VariableList__Output } from '../../../../pando/api/survey/v1/VariableList';

export interface SurveyAdminServiceClient extends grpc.Client {
  CreateAnswerOption(argument: _pando_api_survey_v1_AnswerOption, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOption__Output>): grpc.ClientUnaryCall;
  CreateAnswerOption(argument: _pando_api_survey_v1_AnswerOption, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOption__Output>): grpc.ClientUnaryCall;
  CreateAnswerOption(argument: _pando_api_survey_v1_AnswerOption, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOption__Output>): grpc.ClientUnaryCall;
  CreateAnswerOption(argument: _pando_api_survey_v1_AnswerOption, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOption__Output>): grpc.ClientUnaryCall;
  createAnswerOption(argument: _pando_api_survey_v1_AnswerOption, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOption__Output>): grpc.ClientUnaryCall;
  createAnswerOption(argument: _pando_api_survey_v1_AnswerOption, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOption__Output>): grpc.ClientUnaryCall;
  createAnswerOption(argument: _pando_api_survey_v1_AnswerOption, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOption__Output>): grpc.ClientUnaryCall;
  createAnswerOption(argument: _pando_api_survey_v1_AnswerOption, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOption__Output>): grpc.ClientUnaryCall;
  
  CreateInstance(argument: _pando_api_survey_v1_CreateInstanceRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Instance__Output>): grpc.ClientUnaryCall;
  CreateInstance(argument: _pando_api_survey_v1_CreateInstanceRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_Instance__Output>): grpc.ClientUnaryCall;
  CreateInstance(argument: _pando_api_survey_v1_CreateInstanceRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Instance__Output>): grpc.ClientUnaryCall;
  CreateInstance(argument: _pando_api_survey_v1_CreateInstanceRequest, callback: grpc.requestCallback<_pando_api_survey_v1_Instance__Output>): grpc.ClientUnaryCall;
  createInstance(argument: _pando_api_survey_v1_CreateInstanceRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Instance__Output>): grpc.ClientUnaryCall;
  createInstance(argument: _pando_api_survey_v1_CreateInstanceRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_Instance__Output>): grpc.ClientUnaryCall;
  createInstance(argument: _pando_api_survey_v1_CreateInstanceRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Instance__Output>): grpc.ClientUnaryCall;
  createInstance(argument: _pando_api_survey_v1_CreateInstanceRequest, callback: grpc.requestCallback<_pando_api_survey_v1_Instance__Output>): grpc.ClientUnaryCall;
  
  CreatePlaybackData(argument: _pando_api_survey_v1_PlaybackData, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_PlaybackData__Output>): grpc.ClientUnaryCall;
  CreatePlaybackData(argument: _pando_api_survey_v1_PlaybackData, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_PlaybackData__Output>): grpc.ClientUnaryCall;
  CreatePlaybackData(argument: _pando_api_survey_v1_PlaybackData, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_PlaybackData__Output>): grpc.ClientUnaryCall;
  CreatePlaybackData(argument: _pando_api_survey_v1_PlaybackData, callback: grpc.requestCallback<_pando_api_survey_v1_PlaybackData__Output>): grpc.ClientUnaryCall;
  createPlaybackData(argument: _pando_api_survey_v1_PlaybackData, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_PlaybackData__Output>): grpc.ClientUnaryCall;
  createPlaybackData(argument: _pando_api_survey_v1_PlaybackData, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_PlaybackData__Output>): grpc.ClientUnaryCall;
  createPlaybackData(argument: _pando_api_survey_v1_PlaybackData, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_PlaybackData__Output>): grpc.ClientUnaryCall;
  createPlaybackData(argument: _pando_api_survey_v1_PlaybackData, callback: grpc.requestCallback<_pando_api_survey_v1_PlaybackData__Output>): grpc.ClientUnaryCall;
  
  CreateQuestion(argument: _pando_api_survey_v1_Question, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Question__Output>): grpc.ClientUnaryCall;
  CreateQuestion(argument: _pando_api_survey_v1_Question, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_Question__Output>): grpc.ClientUnaryCall;
  CreateQuestion(argument: _pando_api_survey_v1_Question, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Question__Output>): grpc.ClientUnaryCall;
  CreateQuestion(argument: _pando_api_survey_v1_Question, callback: grpc.requestCallback<_pando_api_survey_v1_Question__Output>): grpc.ClientUnaryCall;
  createQuestion(argument: _pando_api_survey_v1_Question, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Question__Output>): grpc.ClientUnaryCall;
  createQuestion(argument: _pando_api_survey_v1_Question, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_Question__Output>): grpc.ClientUnaryCall;
  createQuestion(argument: _pando_api_survey_v1_Question, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Question__Output>): grpc.ClientUnaryCall;
  createQuestion(argument: _pando_api_survey_v1_Question, callback: grpc.requestCallback<_pando_api_survey_v1_Question__Output>): grpc.ClientUnaryCall;
  
  CreateSurvey(argument: _pando_api_survey_v1_Survey, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  CreateSurvey(argument: _pando_api_survey_v1_Survey, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  CreateSurvey(argument: _pando_api_survey_v1_Survey, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  CreateSurvey(argument: _pando_api_survey_v1_Survey, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  createSurvey(argument: _pando_api_survey_v1_Survey, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  createSurvey(argument: _pando_api_survey_v1_Survey, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  createSurvey(argument: _pando_api_survey_v1_Survey, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  createSurvey(argument: _pando_api_survey_v1_Survey, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  
  CreateSurveyQuestionDestination(argument: _pando_api_survey_v1_SurveyQuestionDestination, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestination__Output>): grpc.ClientUnaryCall;
  CreateSurveyQuestionDestination(argument: _pando_api_survey_v1_SurveyQuestionDestination, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestination__Output>): grpc.ClientUnaryCall;
  CreateSurveyQuestionDestination(argument: _pando_api_survey_v1_SurveyQuestionDestination, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestination__Output>): grpc.ClientUnaryCall;
  CreateSurveyQuestionDestination(argument: _pando_api_survey_v1_SurveyQuestionDestination, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestination__Output>): grpc.ClientUnaryCall;
  createSurveyQuestionDestination(argument: _pando_api_survey_v1_SurveyQuestionDestination, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestination__Output>): grpc.ClientUnaryCall;
  createSurveyQuestionDestination(argument: _pando_api_survey_v1_SurveyQuestionDestination, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestination__Output>): grpc.ClientUnaryCall;
  createSurveyQuestionDestination(argument: _pando_api_survey_v1_SurveyQuestionDestination, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestination__Output>): grpc.ClientUnaryCall;
  createSurveyQuestionDestination(argument: _pando_api_survey_v1_SurveyQuestionDestination, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestination__Output>): grpc.ClientUnaryCall;
  
  CreateSurveyVariable(argument: _pando_api_survey_v1_SurveyVariable, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyVariable__Output>): grpc.ClientUnaryCall;
  CreateSurveyVariable(argument: _pando_api_survey_v1_SurveyVariable, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyVariable__Output>): grpc.ClientUnaryCall;
  CreateSurveyVariable(argument: _pando_api_survey_v1_SurveyVariable, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyVariable__Output>): grpc.ClientUnaryCall;
  CreateSurveyVariable(argument: _pando_api_survey_v1_SurveyVariable, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyVariable__Output>): grpc.ClientUnaryCall;
  createSurveyVariable(argument: _pando_api_survey_v1_SurveyVariable, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyVariable__Output>): grpc.ClientUnaryCall;
  createSurveyVariable(argument: _pando_api_survey_v1_SurveyVariable, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyVariable__Output>): grpc.ClientUnaryCall;
  createSurveyVariable(argument: _pando_api_survey_v1_SurveyVariable, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyVariable__Output>): grpc.ClientUnaryCall;
  createSurveyVariable(argument: _pando_api_survey_v1_SurveyVariable, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyVariable__Output>): grpc.ClientUnaryCall;
  
  CreateSurveyVersion(argument: _pando_api_survey_v1_CreateSurveyVersionRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  CreateSurveyVersion(argument: _pando_api_survey_v1_CreateSurveyVersionRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  CreateSurveyVersion(argument: _pando_api_survey_v1_CreateSurveyVersionRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  CreateSurveyVersion(argument: _pando_api_survey_v1_CreateSurveyVersionRequest, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  createSurveyVersion(argument: _pando_api_survey_v1_CreateSurveyVersionRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  createSurveyVersion(argument: _pando_api_survey_v1_CreateSurveyVersionRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  createSurveyVersion(argument: _pando_api_survey_v1_CreateSurveyVersionRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  createSurveyVersion(argument: _pando_api_survey_v1_CreateSurveyVersionRequest, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  
  CreateVariable(argument: _pando_api_survey_v1_Variable, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Variable__Output>): grpc.ClientUnaryCall;
  CreateVariable(argument: _pando_api_survey_v1_Variable, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_Variable__Output>): grpc.ClientUnaryCall;
  CreateVariable(argument: _pando_api_survey_v1_Variable, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Variable__Output>): grpc.ClientUnaryCall;
  CreateVariable(argument: _pando_api_survey_v1_Variable, callback: grpc.requestCallback<_pando_api_survey_v1_Variable__Output>): grpc.ClientUnaryCall;
  createVariable(argument: _pando_api_survey_v1_Variable, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Variable__Output>): grpc.ClientUnaryCall;
  createVariable(argument: _pando_api_survey_v1_Variable, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_Variable__Output>): grpc.ClientUnaryCall;
  createVariable(argument: _pando_api_survey_v1_Variable, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Variable__Output>): grpc.ClientUnaryCall;
  createVariable(argument: _pando_api_survey_v1_Variable, callback: grpc.requestCallback<_pando_api_survey_v1_Variable__Output>): grpc.ClientUnaryCall;
  
  CreateVariables(argument: _pando_api_survey_v1_CreateVariablesRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_VariableList__Output>): grpc.ClientUnaryCall;
  CreateVariables(argument: _pando_api_survey_v1_CreateVariablesRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_VariableList__Output>): grpc.ClientUnaryCall;
  CreateVariables(argument: _pando_api_survey_v1_CreateVariablesRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_VariableList__Output>): grpc.ClientUnaryCall;
  CreateVariables(argument: _pando_api_survey_v1_CreateVariablesRequest, callback: grpc.requestCallback<_pando_api_survey_v1_VariableList__Output>): grpc.ClientUnaryCall;
  createVariables(argument: _pando_api_survey_v1_CreateVariablesRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_VariableList__Output>): grpc.ClientUnaryCall;
  createVariables(argument: _pando_api_survey_v1_CreateVariablesRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_VariableList__Output>): grpc.ClientUnaryCall;
  createVariables(argument: _pando_api_survey_v1_CreateVariablesRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_VariableList__Output>): grpc.ClientUnaryCall;
  createVariables(argument: _pando_api_survey_v1_CreateVariablesRequest, callback: grpc.requestCallback<_pando_api_survey_v1_VariableList__Output>): grpc.ClientUnaryCall;
  
  DeleteAnswerOption(argument: _pando_api_survey_v1_DeleteAnswerOptionRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteAnswerOption(argument: _pando_api_survey_v1_DeleteAnswerOptionRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteAnswerOption(argument: _pando_api_survey_v1_DeleteAnswerOptionRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteAnswerOption(argument: _pando_api_survey_v1_DeleteAnswerOptionRequest, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteAnswerOption(argument: _pando_api_survey_v1_DeleteAnswerOptionRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteAnswerOption(argument: _pando_api_survey_v1_DeleteAnswerOptionRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteAnswerOption(argument: _pando_api_survey_v1_DeleteAnswerOptionRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteAnswerOption(argument: _pando_api_survey_v1_DeleteAnswerOptionRequest, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  
  DeleteInstance(argument: _pando_api_survey_v1_DeleteInstanceRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteInstance(argument: _pando_api_survey_v1_DeleteInstanceRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteInstance(argument: _pando_api_survey_v1_DeleteInstanceRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteInstance(argument: _pando_api_survey_v1_DeleteInstanceRequest, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteInstance(argument: _pando_api_survey_v1_DeleteInstanceRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteInstance(argument: _pando_api_survey_v1_DeleteInstanceRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteInstance(argument: _pando_api_survey_v1_DeleteInstanceRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteInstance(argument: _pando_api_survey_v1_DeleteInstanceRequest, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  
  DeleteMedia(argument: _pando_api_survey_v1_DeleteMediaRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteMedia(argument: _pando_api_survey_v1_DeleteMediaRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteMedia(argument: _pando_api_survey_v1_DeleteMediaRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteMedia(argument: _pando_api_survey_v1_DeleteMediaRequest, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteMedia(argument: _pando_api_survey_v1_DeleteMediaRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteMedia(argument: _pando_api_survey_v1_DeleteMediaRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteMedia(argument: _pando_api_survey_v1_DeleteMediaRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteMedia(argument: _pando_api_survey_v1_DeleteMediaRequest, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  
  DeletePlaybackData(argument: _pando_api_survey_v1_DeletePlaybackDataRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeletePlaybackData(argument: _pando_api_survey_v1_DeletePlaybackDataRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeletePlaybackData(argument: _pando_api_survey_v1_DeletePlaybackDataRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeletePlaybackData(argument: _pando_api_survey_v1_DeletePlaybackDataRequest, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deletePlaybackData(argument: _pando_api_survey_v1_DeletePlaybackDataRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deletePlaybackData(argument: _pando_api_survey_v1_DeletePlaybackDataRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deletePlaybackData(argument: _pando_api_survey_v1_DeletePlaybackDataRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deletePlaybackData(argument: _pando_api_survey_v1_DeletePlaybackDataRequest, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  
  DeleteQuestion(argument: _pando_api_survey_v1_DeleteQuestionRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteQuestion(argument: _pando_api_survey_v1_DeleteQuestionRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteQuestion(argument: _pando_api_survey_v1_DeleteQuestionRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteQuestion(argument: _pando_api_survey_v1_DeleteQuestionRequest, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteQuestion(argument: _pando_api_survey_v1_DeleteQuestionRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteQuestion(argument: _pando_api_survey_v1_DeleteQuestionRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteQuestion(argument: _pando_api_survey_v1_DeleteQuestionRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteQuestion(argument: _pando_api_survey_v1_DeleteQuestionRequest, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  
  DeleteSurvey(argument: _pando_api_survey_v1_DeleteSurveyRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteSurvey(argument: _pando_api_survey_v1_DeleteSurveyRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteSurvey(argument: _pando_api_survey_v1_DeleteSurveyRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteSurvey(argument: _pando_api_survey_v1_DeleteSurveyRequest, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteSurvey(argument: _pando_api_survey_v1_DeleteSurveyRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteSurvey(argument: _pando_api_survey_v1_DeleteSurveyRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteSurvey(argument: _pando_api_survey_v1_DeleteSurveyRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteSurvey(argument: _pando_api_survey_v1_DeleteSurveyRequest, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  
  DeleteSurveyMedia(argument: _pando_api_survey_v1_DeleteSurveyMediaRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteSurveyMedia(argument: _pando_api_survey_v1_DeleteSurveyMediaRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteSurveyMedia(argument: _pando_api_survey_v1_DeleteSurveyMediaRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteSurveyMedia(argument: _pando_api_survey_v1_DeleteSurveyMediaRequest, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteSurveyMedia(argument: _pando_api_survey_v1_DeleteSurveyMediaRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteSurveyMedia(argument: _pando_api_survey_v1_DeleteSurveyMediaRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteSurveyMedia(argument: _pando_api_survey_v1_DeleteSurveyMediaRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteSurveyMedia(argument: _pando_api_survey_v1_DeleteSurveyMediaRequest, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  
  DeleteSurveyQuestionDestination(argument: _pando_api_survey_v1_DeleteSurveyQuestionDestinationRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteSurveyQuestionDestination(argument: _pando_api_survey_v1_DeleteSurveyQuestionDestinationRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteSurveyQuestionDestination(argument: _pando_api_survey_v1_DeleteSurveyQuestionDestinationRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteSurveyQuestionDestination(argument: _pando_api_survey_v1_DeleteSurveyQuestionDestinationRequest, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteSurveyQuestionDestination(argument: _pando_api_survey_v1_DeleteSurveyQuestionDestinationRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteSurveyQuestionDestination(argument: _pando_api_survey_v1_DeleteSurveyQuestionDestinationRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteSurveyQuestionDestination(argument: _pando_api_survey_v1_DeleteSurveyQuestionDestinationRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteSurveyQuestionDestination(argument: _pando_api_survey_v1_DeleteSurveyQuestionDestinationRequest, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  
  DeleteSurveyVariable(argument: _pando_api_survey_v1_DeleteSurveyVariableRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteSurveyVariable(argument: _pando_api_survey_v1_DeleteSurveyVariableRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteSurveyVariable(argument: _pando_api_survey_v1_DeleteSurveyVariableRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteSurveyVariable(argument: _pando_api_survey_v1_DeleteSurveyVariableRequest, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteSurveyVariable(argument: _pando_api_survey_v1_DeleteSurveyVariableRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteSurveyVariable(argument: _pando_api_survey_v1_DeleteSurveyVariableRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteSurveyVariable(argument: _pando_api_survey_v1_DeleteSurveyVariableRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteSurveyVariable(argument: _pando_api_survey_v1_DeleteSurveyVariableRequest, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  
  DeleteSurveyVersion(argument: _pando_api_survey_v1_DeleteSurveyVersionRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteSurveyVersion(argument: _pando_api_survey_v1_DeleteSurveyVersionRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteSurveyVersion(argument: _pando_api_survey_v1_DeleteSurveyVersionRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteSurveyVersion(argument: _pando_api_survey_v1_DeleteSurveyVersionRequest, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteSurveyVersion(argument: _pando_api_survey_v1_DeleteSurveyVersionRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteSurveyVersion(argument: _pando_api_survey_v1_DeleteSurveyVersionRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteSurveyVersion(argument: _pando_api_survey_v1_DeleteSurveyVersionRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteSurveyVersion(argument: _pando_api_survey_v1_DeleteSurveyVersionRequest, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  
  DeleteVariable(argument: _pando_api_survey_v1_DeleteVariableRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteVariable(argument: _pando_api_survey_v1_DeleteVariableRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteVariable(argument: _pando_api_survey_v1_DeleteVariableRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteVariable(argument: _pando_api_survey_v1_DeleteVariableRequest, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteVariable(argument: _pando_api_survey_v1_DeleteVariableRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteVariable(argument: _pando_api_survey_v1_DeleteVariableRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteVariable(argument: _pando_api_survey_v1_DeleteVariableRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteVariable(argument: _pando_api_survey_v1_DeleteVariableRequest, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  
  DeleteVariableMedia(argument: _pando_api_survey_v1_DeleteVariableMediaRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteVariableMedia(argument: _pando_api_survey_v1_DeleteVariableMediaRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteVariableMedia(argument: _pando_api_survey_v1_DeleteVariableMediaRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  DeleteVariableMedia(argument: _pando_api_survey_v1_DeleteVariableMediaRequest, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteVariableMedia(argument: _pando_api_survey_v1_DeleteVariableMediaRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteVariableMedia(argument: _pando_api_survey_v1_DeleteVariableMediaRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteVariableMedia(argument: _pando_api_survey_v1_DeleteVariableMediaRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  deleteVariableMedia(argument: _pando_api_survey_v1_DeleteVariableMediaRequest, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  
  ExecAddSurveyESignTemplate(argument: _pando_api_survey_v1_SurveyESignTemplate, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyESignTemplate__Output>): grpc.ClientUnaryCall;
  ExecAddSurveyESignTemplate(argument: _pando_api_survey_v1_SurveyESignTemplate, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyESignTemplate__Output>): grpc.ClientUnaryCall;
  ExecAddSurveyESignTemplate(argument: _pando_api_survey_v1_SurveyESignTemplate, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyESignTemplate__Output>): grpc.ClientUnaryCall;
  ExecAddSurveyESignTemplate(argument: _pando_api_survey_v1_SurveyESignTemplate, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyESignTemplate__Output>): grpc.ClientUnaryCall;
  execAddSurveyESignTemplate(argument: _pando_api_survey_v1_SurveyESignTemplate, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyESignTemplate__Output>): grpc.ClientUnaryCall;
  execAddSurveyESignTemplate(argument: _pando_api_survey_v1_SurveyESignTemplate, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyESignTemplate__Output>): grpc.ClientUnaryCall;
  execAddSurveyESignTemplate(argument: _pando_api_survey_v1_SurveyESignTemplate, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyESignTemplate__Output>): grpc.ClientUnaryCall;
  execAddSurveyESignTemplate(argument: _pando_api_survey_v1_SurveyESignTemplate, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyESignTemplate__Output>): grpc.ClientUnaryCall;
  
  ExecAddSurveyInstanceESignSignatory(argument: _pando_api_survey_v1_SurveyInstanceESignSignatory, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyInstanceESignSignatory__Output>): grpc.ClientUnaryCall;
  ExecAddSurveyInstanceESignSignatory(argument: _pando_api_survey_v1_SurveyInstanceESignSignatory, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyInstanceESignSignatory__Output>): grpc.ClientUnaryCall;
  ExecAddSurveyInstanceESignSignatory(argument: _pando_api_survey_v1_SurveyInstanceESignSignatory, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyInstanceESignSignatory__Output>): grpc.ClientUnaryCall;
  ExecAddSurveyInstanceESignSignatory(argument: _pando_api_survey_v1_SurveyInstanceESignSignatory, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyInstanceESignSignatory__Output>): grpc.ClientUnaryCall;
  execAddSurveyInstanceESignSignatory(argument: _pando_api_survey_v1_SurveyInstanceESignSignatory, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyInstanceESignSignatory__Output>): grpc.ClientUnaryCall;
  execAddSurveyInstanceESignSignatory(argument: _pando_api_survey_v1_SurveyInstanceESignSignatory, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyInstanceESignSignatory__Output>): grpc.ClientUnaryCall;
  execAddSurveyInstanceESignSignatory(argument: _pando_api_survey_v1_SurveyInstanceESignSignatory, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyInstanceESignSignatory__Output>): grpc.ClientUnaryCall;
  execAddSurveyInstanceESignSignatory(argument: _pando_api_survey_v1_SurveyInstanceESignSignatory, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyInstanceESignSignatory__Output>): grpc.ClientUnaryCall;
  
  ExecRemoveSurveyESignTemplate(argument: _pando_api_survey_v1_SurveyESignTemplate, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  ExecRemoveSurveyESignTemplate(argument: _pando_api_survey_v1_SurveyESignTemplate, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  ExecRemoveSurveyESignTemplate(argument: _pando_api_survey_v1_SurveyESignTemplate, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  ExecRemoveSurveyESignTemplate(argument: _pando_api_survey_v1_SurveyESignTemplate, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  execRemoveSurveyESignTemplate(argument: _pando_api_survey_v1_SurveyESignTemplate, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  execRemoveSurveyESignTemplate(argument: _pando_api_survey_v1_SurveyESignTemplate, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  execRemoveSurveyESignTemplate(argument: _pando_api_survey_v1_SurveyESignTemplate, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  execRemoveSurveyESignTemplate(argument: _pando_api_survey_v1_SurveyESignTemplate, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  
  ExecRemoveSurveyInstanceESignSignatory(argument: _pando_api_survey_v1_SurveyInstanceESignSignatory, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  ExecRemoveSurveyInstanceESignSignatory(argument: _pando_api_survey_v1_SurveyInstanceESignSignatory, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  ExecRemoveSurveyInstanceESignSignatory(argument: _pando_api_survey_v1_SurveyInstanceESignSignatory, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  ExecRemoveSurveyInstanceESignSignatory(argument: _pando_api_survey_v1_SurveyInstanceESignSignatory, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  execRemoveSurveyInstanceESignSignatory(argument: _pando_api_survey_v1_SurveyInstanceESignSignatory, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  execRemoveSurveyInstanceESignSignatory(argument: _pando_api_survey_v1_SurveyInstanceESignSignatory, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  execRemoveSurveyInstanceESignSignatory(argument: _pando_api_survey_v1_SurveyInstanceESignSignatory, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  execRemoveSurveyInstanceESignSignatory(argument: _pando_api_survey_v1_SurveyInstanceESignSignatory, callback: grpc.requestCallback<_pando_api_Empty__Output>): grpc.ClientUnaryCall;
  
  ExecSendInstanceLink(argument: _pando_api_survey_v1_SendInstanceLink, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SendInstanceLink__Output>): grpc.ClientUnaryCall;
  ExecSendInstanceLink(argument: _pando_api_survey_v1_SendInstanceLink, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SendInstanceLink__Output>): grpc.ClientUnaryCall;
  ExecSendInstanceLink(argument: _pando_api_survey_v1_SendInstanceLink, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SendInstanceLink__Output>): grpc.ClientUnaryCall;
  ExecSendInstanceLink(argument: _pando_api_survey_v1_SendInstanceLink, callback: grpc.requestCallback<_pando_api_survey_v1_SendInstanceLink__Output>): grpc.ClientUnaryCall;
  execSendInstanceLink(argument: _pando_api_survey_v1_SendInstanceLink, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SendInstanceLink__Output>): grpc.ClientUnaryCall;
  execSendInstanceLink(argument: _pando_api_survey_v1_SendInstanceLink, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SendInstanceLink__Output>): grpc.ClientUnaryCall;
  execSendInstanceLink(argument: _pando_api_survey_v1_SendInstanceLink, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SendInstanceLink__Output>): grpc.ClientUnaryCall;
  execSendInstanceLink(argument: _pando_api_survey_v1_SendInstanceLink, callback: grpc.requestCallback<_pando_api_survey_v1_SendInstanceLink__Output>): grpc.ClientUnaryCall;
  
  GetAnswerOption(argument: _pando_api_survey_v1_GetAnswerOptionRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOption__Output>): grpc.ClientUnaryCall;
  GetAnswerOption(argument: _pando_api_survey_v1_GetAnswerOptionRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOption__Output>): grpc.ClientUnaryCall;
  GetAnswerOption(argument: _pando_api_survey_v1_GetAnswerOptionRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOption__Output>): grpc.ClientUnaryCall;
  GetAnswerOption(argument: _pando_api_survey_v1_GetAnswerOptionRequest, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOption__Output>): grpc.ClientUnaryCall;
  getAnswerOption(argument: _pando_api_survey_v1_GetAnswerOptionRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOption__Output>): grpc.ClientUnaryCall;
  getAnswerOption(argument: _pando_api_survey_v1_GetAnswerOptionRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOption__Output>): grpc.ClientUnaryCall;
  getAnswerOption(argument: _pando_api_survey_v1_GetAnswerOptionRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOption__Output>): grpc.ClientUnaryCall;
  getAnswerOption(argument: _pando_api_survey_v1_GetAnswerOptionRequest, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOption__Output>): grpc.ClientUnaryCall;
  
  GetAnswerOptions(argument: _pando_api_survey_v1_GetAnswerOptionsRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOptionList__Output>): grpc.ClientUnaryCall;
  GetAnswerOptions(argument: _pando_api_survey_v1_GetAnswerOptionsRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOptionList__Output>): grpc.ClientUnaryCall;
  GetAnswerOptions(argument: _pando_api_survey_v1_GetAnswerOptionsRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOptionList__Output>): grpc.ClientUnaryCall;
  GetAnswerOptions(argument: _pando_api_survey_v1_GetAnswerOptionsRequest, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOptionList__Output>): grpc.ClientUnaryCall;
  getAnswerOptions(argument: _pando_api_survey_v1_GetAnswerOptionsRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOptionList__Output>): grpc.ClientUnaryCall;
  getAnswerOptions(argument: _pando_api_survey_v1_GetAnswerOptionsRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOptionList__Output>): grpc.ClientUnaryCall;
  getAnswerOptions(argument: _pando_api_survey_v1_GetAnswerOptionsRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOptionList__Output>): grpc.ClientUnaryCall;
  getAnswerOptions(argument: _pando_api_survey_v1_GetAnswerOptionsRequest, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOptionList__Output>): grpc.ClientUnaryCall;
  
  GetPlaybackDataList(argument: _pando_api_survey_v1_GetPlaybackDataListRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_PlaybackDataList__Output>): grpc.ClientUnaryCall;
  GetPlaybackDataList(argument: _pando_api_survey_v1_GetPlaybackDataListRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_PlaybackDataList__Output>): grpc.ClientUnaryCall;
  GetPlaybackDataList(argument: _pando_api_survey_v1_GetPlaybackDataListRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_PlaybackDataList__Output>): grpc.ClientUnaryCall;
  GetPlaybackDataList(argument: _pando_api_survey_v1_GetPlaybackDataListRequest, callback: grpc.requestCallback<_pando_api_survey_v1_PlaybackDataList__Output>): grpc.ClientUnaryCall;
  getPlaybackDataList(argument: _pando_api_survey_v1_GetPlaybackDataListRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_PlaybackDataList__Output>): grpc.ClientUnaryCall;
  getPlaybackDataList(argument: _pando_api_survey_v1_GetPlaybackDataListRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_PlaybackDataList__Output>): grpc.ClientUnaryCall;
  getPlaybackDataList(argument: _pando_api_survey_v1_GetPlaybackDataListRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_PlaybackDataList__Output>): grpc.ClientUnaryCall;
  getPlaybackDataList(argument: _pando_api_survey_v1_GetPlaybackDataListRequest, callback: grpc.requestCallback<_pando_api_survey_v1_PlaybackDataList__Output>): grpc.ClientUnaryCall;
  
  GetQuestion(argument: _pando_api_survey_v1_GetQuestionRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Question__Output>): grpc.ClientUnaryCall;
  GetQuestion(argument: _pando_api_survey_v1_GetQuestionRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_Question__Output>): grpc.ClientUnaryCall;
  GetQuestion(argument: _pando_api_survey_v1_GetQuestionRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Question__Output>): grpc.ClientUnaryCall;
  GetQuestion(argument: _pando_api_survey_v1_GetQuestionRequest, callback: grpc.requestCallback<_pando_api_survey_v1_Question__Output>): grpc.ClientUnaryCall;
  getQuestion(argument: _pando_api_survey_v1_GetQuestionRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Question__Output>): grpc.ClientUnaryCall;
  getQuestion(argument: _pando_api_survey_v1_GetQuestionRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_Question__Output>): grpc.ClientUnaryCall;
  getQuestion(argument: _pando_api_survey_v1_GetQuestionRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Question__Output>): grpc.ClientUnaryCall;
  getQuestion(argument: _pando_api_survey_v1_GetQuestionRequest, callback: grpc.requestCallback<_pando_api_survey_v1_Question__Output>): grpc.ClientUnaryCall;
  
  GetQuestions(argument: _pando_api_survey_v1_GetQuestionsRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionList__Output>): grpc.ClientUnaryCall;
  GetQuestions(argument: _pando_api_survey_v1_GetQuestionsRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionList__Output>): grpc.ClientUnaryCall;
  GetQuestions(argument: _pando_api_survey_v1_GetQuestionsRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionList__Output>): grpc.ClientUnaryCall;
  GetQuestions(argument: _pando_api_survey_v1_GetQuestionsRequest, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionList__Output>): grpc.ClientUnaryCall;
  getQuestions(argument: _pando_api_survey_v1_GetQuestionsRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionList__Output>): grpc.ClientUnaryCall;
  getQuestions(argument: _pando_api_survey_v1_GetQuestionsRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionList__Output>): grpc.ClientUnaryCall;
  getQuestions(argument: _pando_api_survey_v1_GetQuestionsRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionList__Output>): grpc.ClientUnaryCall;
  getQuestions(argument: _pando_api_survey_v1_GetQuestionsRequest, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionList__Output>): grpc.ClientUnaryCall;
  
  GetSurvey(argument: _pando_api_survey_v1_GetSurveyRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  GetSurvey(argument: _pando_api_survey_v1_GetSurveyRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  GetSurvey(argument: _pando_api_survey_v1_GetSurveyRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  GetSurvey(argument: _pando_api_survey_v1_GetSurveyRequest, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  getSurvey(argument: _pando_api_survey_v1_GetSurveyRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  getSurvey(argument: _pando_api_survey_v1_GetSurveyRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  getSurvey(argument: _pando_api_survey_v1_GetSurveyRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  getSurvey(argument: _pando_api_survey_v1_GetSurveyRequest, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  
  GetSurveyDocuments(argument: _pando_api_survey_v1_GetSurveyDocumentsRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyDocumentList__Output>): grpc.ClientUnaryCall;
  GetSurveyDocuments(argument: _pando_api_survey_v1_GetSurveyDocumentsRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyDocumentList__Output>): grpc.ClientUnaryCall;
  GetSurveyDocuments(argument: _pando_api_survey_v1_GetSurveyDocumentsRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyDocumentList__Output>): grpc.ClientUnaryCall;
  GetSurveyDocuments(argument: _pando_api_survey_v1_GetSurveyDocumentsRequest, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyDocumentList__Output>): grpc.ClientUnaryCall;
  getSurveyDocuments(argument: _pando_api_survey_v1_GetSurveyDocumentsRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyDocumentList__Output>): grpc.ClientUnaryCall;
  getSurveyDocuments(argument: _pando_api_survey_v1_GetSurveyDocumentsRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyDocumentList__Output>): grpc.ClientUnaryCall;
  getSurveyDocuments(argument: _pando_api_survey_v1_GetSurveyDocumentsRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyDocumentList__Output>): grpc.ClientUnaryCall;
  getSurveyDocuments(argument: _pando_api_survey_v1_GetSurveyDocumentsRequest, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyDocumentList__Output>): grpc.ClientUnaryCall;
  
  GetSurveyMediaList(argument: _pando_api_survey_v1_GetSurveyMediaListRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_MediaList__Output>): grpc.ClientUnaryCall;
  GetSurveyMediaList(argument: _pando_api_survey_v1_GetSurveyMediaListRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_MediaList__Output>): grpc.ClientUnaryCall;
  GetSurveyMediaList(argument: _pando_api_survey_v1_GetSurveyMediaListRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_MediaList__Output>): grpc.ClientUnaryCall;
  GetSurveyMediaList(argument: _pando_api_survey_v1_GetSurveyMediaListRequest, callback: grpc.requestCallback<_pando_api_survey_v1_MediaList__Output>): grpc.ClientUnaryCall;
  getSurveyMediaList(argument: _pando_api_survey_v1_GetSurveyMediaListRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_MediaList__Output>): grpc.ClientUnaryCall;
  getSurveyMediaList(argument: _pando_api_survey_v1_GetSurveyMediaListRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_MediaList__Output>): grpc.ClientUnaryCall;
  getSurveyMediaList(argument: _pando_api_survey_v1_GetSurveyMediaListRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_MediaList__Output>): grpc.ClientUnaryCall;
  getSurveyMediaList(argument: _pando_api_survey_v1_GetSurveyMediaListRequest, callback: grpc.requestCallback<_pando_api_survey_v1_MediaList__Output>): grpc.ClientUnaryCall;
  
  GetSurveyQuestionDestination(argument: _pando_api_survey_v1_GetSurveyQuestionDestinationRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestination__Output>): grpc.ClientUnaryCall;
  GetSurveyQuestionDestination(argument: _pando_api_survey_v1_GetSurveyQuestionDestinationRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestination__Output>): grpc.ClientUnaryCall;
  GetSurveyQuestionDestination(argument: _pando_api_survey_v1_GetSurveyQuestionDestinationRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestination__Output>): grpc.ClientUnaryCall;
  GetSurveyQuestionDestination(argument: _pando_api_survey_v1_GetSurveyQuestionDestinationRequest, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestination__Output>): grpc.ClientUnaryCall;
  getSurveyQuestionDestination(argument: _pando_api_survey_v1_GetSurveyQuestionDestinationRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestination__Output>): grpc.ClientUnaryCall;
  getSurveyQuestionDestination(argument: _pando_api_survey_v1_GetSurveyQuestionDestinationRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestination__Output>): grpc.ClientUnaryCall;
  getSurveyQuestionDestination(argument: _pando_api_survey_v1_GetSurveyQuestionDestinationRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestination__Output>): grpc.ClientUnaryCall;
  getSurveyQuestionDestination(argument: _pando_api_survey_v1_GetSurveyQuestionDestinationRequest, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestination__Output>): grpc.ClientUnaryCall;
  
  GetSurveyQuestionDestinations(argument: _pando_api_survey_v1_GetSurveyQuestionDestinationsRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestinationList__Output>): grpc.ClientUnaryCall;
  GetSurveyQuestionDestinations(argument: _pando_api_survey_v1_GetSurveyQuestionDestinationsRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestinationList__Output>): grpc.ClientUnaryCall;
  GetSurveyQuestionDestinations(argument: _pando_api_survey_v1_GetSurveyQuestionDestinationsRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestinationList__Output>): grpc.ClientUnaryCall;
  GetSurveyQuestionDestinations(argument: _pando_api_survey_v1_GetSurveyQuestionDestinationsRequest, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestinationList__Output>): grpc.ClientUnaryCall;
  getSurveyQuestionDestinations(argument: _pando_api_survey_v1_GetSurveyQuestionDestinationsRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestinationList__Output>): grpc.ClientUnaryCall;
  getSurveyQuestionDestinations(argument: _pando_api_survey_v1_GetSurveyQuestionDestinationsRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestinationList__Output>): grpc.ClientUnaryCall;
  getSurveyQuestionDestinations(argument: _pando_api_survey_v1_GetSurveyQuestionDestinationsRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestinationList__Output>): grpc.ClientUnaryCall;
  getSurveyQuestionDestinations(argument: _pando_api_survey_v1_GetSurveyQuestionDestinationsRequest, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestinationList__Output>): grpc.ClientUnaryCall;
  
  GetSurveyVariables(argument: _pando_api_survey_v1_GetSurveyVariablesRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_VariableList__Output>): grpc.ClientUnaryCall;
  GetSurveyVariables(argument: _pando_api_survey_v1_GetSurveyVariablesRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_VariableList__Output>): grpc.ClientUnaryCall;
  GetSurveyVariables(argument: _pando_api_survey_v1_GetSurveyVariablesRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_VariableList__Output>): grpc.ClientUnaryCall;
  GetSurveyVariables(argument: _pando_api_survey_v1_GetSurveyVariablesRequest, callback: grpc.requestCallback<_pando_api_survey_v1_VariableList__Output>): grpc.ClientUnaryCall;
  getSurveyVariables(argument: _pando_api_survey_v1_GetSurveyVariablesRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_VariableList__Output>): grpc.ClientUnaryCall;
  getSurveyVariables(argument: _pando_api_survey_v1_GetSurveyVariablesRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_VariableList__Output>): grpc.ClientUnaryCall;
  getSurveyVariables(argument: _pando_api_survey_v1_GetSurveyVariablesRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_VariableList__Output>): grpc.ClientUnaryCall;
  getSurveyVariables(argument: _pando_api_survey_v1_GetSurveyVariablesRequest, callback: grpc.requestCallback<_pando_api_survey_v1_VariableList__Output>): grpc.ClientUnaryCall;
  
  GetSurveyVersions(argument: _pando_api_survey_v1_GetSurveyVersionsRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyList__Output>): grpc.ClientUnaryCall;
  GetSurveyVersions(argument: _pando_api_survey_v1_GetSurveyVersionsRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyList__Output>): grpc.ClientUnaryCall;
  GetSurveyVersions(argument: _pando_api_survey_v1_GetSurveyVersionsRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyList__Output>): grpc.ClientUnaryCall;
  GetSurveyVersions(argument: _pando_api_survey_v1_GetSurveyVersionsRequest, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyList__Output>): grpc.ClientUnaryCall;
  getSurveyVersions(argument: _pando_api_survey_v1_GetSurveyVersionsRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyList__Output>): grpc.ClientUnaryCall;
  getSurveyVersions(argument: _pando_api_survey_v1_GetSurveyVersionsRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyList__Output>): grpc.ClientUnaryCall;
  getSurveyVersions(argument: _pando_api_survey_v1_GetSurveyVersionsRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyList__Output>): grpc.ClientUnaryCall;
  getSurveyVersions(argument: _pando_api_survey_v1_GetSurveyVersionsRequest, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyList__Output>): grpc.ClientUnaryCall;
  
  GetSurveys(argument: _pando_api_survey_v1_GetSurveysRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyDetailList__Output>): grpc.ClientUnaryCall;
  GetSurveys(argument: _pando_api_survey_v1_GetSurveysRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyDetailList__Output>): grpc.ClientUnaryCall;
  GetSurveys(argument: _pando_api_survey_v1_GetSurveysRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyDetailList__Output>): grpc.ClientUnaryCall;
  GetSurveys(argument: _pando_api_survey_v1_GetSurveysRequest, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyDetailList__Output>): grpc.ClientUnaryCall;
  getSurveys(argument: _pando_api_survey_v1_GetSurveysRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyDetailList__Output>): grpc.ClientUnaryCall;
  getSurveys(argument: _pando_api_survey_v1_GetSurveysRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyDetailList__Output>): grpc.ClientUnaryCall;
  getSurveys(argument: _pando_api_survey_v1_GetSurveysRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyDetailList__Output>): grpc.ClientUnaryCall;
  getSurveys(argument: _pando_api_survey_v1_GetSurveysRequest, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyDetailList__Output>): grpc.ClientUnaryCall;
  
  GetVariable(argument: _pando_api_survey_v1_GetVariableRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Variable__Output>): grpc.ClientUnaryCall;
  GetVariable(argument: _pando_api_survey_v1_GetVariableRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_Variable__Output>): grpc.ClientUnaryCall;
  GetVariable(argument: _pando_api_survey_v1_GetVariableRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Variable__Output>): grpc.ClientUnaryCall;
  GetVariable(argument: _pando_api_survey_v1_GetVariableRequest, callback: grpc.requestCallback<_pando_api_survey_v1_Variable__Output>): grpc.ClientUnaryCall;
  getVariable(argument: _pando_api_survey_v1_GetVariableRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Variable__Output>): grpc.ClientUnaryCall;
  getVariable(argument: _pando_api_survey_v1_GetVariableRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_Variable__Output>): grpc.ClientUnaryCall;
  getVariable(argument: _pando_api_survey_v1_GetVariableRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Variable__Output>): grpc.ClientUnaryCall;
  getVariable(argument: _pando_api_survey_v1_GetVariableRequest, callback: grpc.requestCallback<_pando_api_survey_v1_Variable__Output>): grpc.ClientUnaryCall;
  
  GetVariableMediaList(argument: _pando_api_survey_v1_GetVariableMediaListRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_MediaList__Output>): grpc.ClientUnaryCall;
  GetVariableMediaList(argument: _pando_api_survey_v1_GetVariableMediaListRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_MediaList__Output>): grpc.ClientUnaryCall;
  GetVariableMediaList(argument: _pando_api_survey_v1_GetVariableMediaListRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_MediaList__Output>): grpc.ClientUnaryCall;
  GetVariableMediaList(argument: _pando_api_survey_v1_GetVariableMediaListRequest, callback: grpc.requestCallback<_pando_api_survey_v1_MediaList__Output>): grpc.ClientUnaryCall;
  getVariableMediaList(argument: _pando_api_survey_v1_GetVariableMediaListRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_MediaList__Output>): grpc.ClientUnaryCall;
  getVariableMediaList(argument: _pando_api_survey_v1_GetVariableMediaListRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_MediaList__Output>): grpc.ClientUnaryCall;
  getVariableMediaList(argument: _pando_api_survey_v1_GetVariableMediaListRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_MediaList__Output>): grpc.ClientUnaryCall;
  getVariableMediaList(argument: _pando_api_survey_v1_GetVariableMediaListRequest, callback: grpc.requestCallback<_pando_api_survey_v1_MediaList__Output>): grpc.ClientUnaryCall;
  
  GetVariableTemplate(argument: _pando_api_survey_v1_GetVariableTemplateRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_GetVariableTemplateResponse__Output>): grpc.ClientUnaryCall;
  GetVariableTemplate(argument: _pando_api_survey_v1_GetVariableTemplateRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_GetVariableTemplateResponse__Output>): grpc.ClientUnaryCall;
  GetVariableTemplate(argument: _pando_api_survey_v1_GetVariableTemplateRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_GetVariableTemplateResponse__Output>): grpc.ClientUnaryCall;
  GetVariableTemplate(argument: _pando_api_survey_v1_GetVariableTemplateRequest, callback: grpc.requestCallback<_pando_api_survey_v1_GetVariableTemplateResponse__Output>): grpc.ClientUnaryCall;
  getVariableTemplate(argument: _pando_api_survey_v1_GetVariableTemplateRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_GetVariableTemplateResponse__Output>): grpc.ClientUnaryCall;
  getVariableTemplate(argument: _pando_api_survey_v1_GetVariableTemplateRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_GetVariableTemplateResponse__Output>): grpc.ClientUnaryCall;
  getVariableTemplate(argument: _pando_api_survey_v1_GetVariableTemplateRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_GetVariableTemplateResponse__Output>): grpc.ClientUnaryCall;
  getVariableTemplate(argument: _pando_api_survey_v1_GetVariableTemplateRequest, callback: grpc.requestCallback<_pando_api_survey_v1_GetVariableTemplateResponse__Output>): grpc.ClientUnaryCall;
  
  GetVariables(argument: _pando_api_survey_v1_GetVariablesRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_VariableList__Output>): grpc.ClientUnaryCall;
  GetVariables(argument: _pando_api_survey_v1_GetVariablesRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_VariableList__Output>): grpc.ClientUnaryCall;
  GetVariables(argument: _pando_api_survey_v1_GetVariablesRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_VariableList__Output>): grpc.ClientUnaryCall;
  GetVariables(argument: _pando_api_survey_v1_GetVariablesRequest, callback: grpc.requestCallback<_pando_api_survey_v1_VariableList__Output>): grpc.ClientUnaryCall;
  getVariables(argument: _pando_api_survey_v1_GetVariablesRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_VariableList__Output>): grpc.ClientUnaryCall;
  getVariables(argument: _pando_api_survey_v1_GetVariablesRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_VariableList__Output>): grpc.ClientUnaryCall;
  getVariables(argument: _pando_api_survey_v1_GetVariablesRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_VariableList__Output>): grpc.ClientUnaryCall;
  getVariables(argument: _pando_api_survey_v1_GetVariablesRequest, callback: grpc.requestCallback<_pando_api_survey_v1_VariableList__Output>): grpc.ClientUnaryCall;
  
  UpdateAnswerOption(argument: _pando_api_survey_v1_UpdateAnswerOptionRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOption__Output>): grpc.ClientUnaryCall;
  UpdateAnswerOption(argument: _pando_api_survey_v1_UpdateAnswerOptionRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOption__Output>): grpc.ClientUnaryCall;
  UpdateAnswerOption(argument: _pando_api_survey_v1_UpdateAnswerOptionRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOption__Output>): grpc.ClientUnaryCall;
  UpdateAnswerOption(argument: _pando_api_survey_v1_UpdateAnswerOptionRequest, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOption__Output>): grpc.ClientUnaryCall;
  updateAnswerOption(argument: _pando_api_survey_v1_UpdateAnswerOptionRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOption__Output>): grpc.ClientUnaryCall;
  updateAnswerOption(argument: _pando_api_survey_v1_UpdateAnswerOptionRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOption__Output>): grpc.ClientUnaryCall;
  updateAnswerOption(argument: _pando_api_survey_v1_UpdateAnswerOptionRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOption__Output>): grpc.ClientUnaryCall;
  updateAnswerOption(argument: _pando_api_survey_v1_UpdateAnswerOptionRequest, callback: grpc.requestCallback<_pando_api_survey_v1_AnswerOption__Output>): grpc.ClientUnaryCall;
  
  UpdatePlaybackData(argument: _pando_api_survey_v1_UpdatePlaybackDataRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_PlaybackData__Output>): grpc.ClientUnaryCall;
  UpdatePlaybackData(argument: _pando_api_survey_v1_UpdatePlaybackDataRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_PlaybackData__Output>): grpc.ClientUnaryCall;
  UpdatePlaybackData(argument: _pando_api_survey_v1_UpdatePlaybackDataRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_PlaybackData__Output>): grpc.ClientUnaryCall;
  UpdatePlaybackData(argument: _pando_api_survey_v1_UpdatePlaybackDataRequest, callback: grpc.requestCallback<_pando_api_survey_v1_PlaybackData__Output>): grpc.ClientUnaryCall;
  updatePlaybackData(argument: _pando_api_survey_v1_UpdatePlaybackDataRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_PlaybackData__Output>): grpc.ClientUnaryCall;
  updatePlaybackData(argument: _pando_api_survey_v1_UpdatePlaybackDataRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_PlaybackData__Output>): grpc.ClientUnaryCall;
  updatePlaybackData(argument: _pando_api_survey_v1_UpdatePlaybackDataRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_PlaybackData__Output>): grpc.ClientUnaryCall;
  updatePlaybackData(argument: _pando_api_survey_v1_UpdatePlaybackDataRequest, callback: grpc.requestCallback<_pando_api_survey_v1_PlaybackData__Output>): grpc.ClientUnaryCall;
  
  UpdateQuestion(argument: _pando_api_survey_v1_UpdateQuestionRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Question__Output>): grpc.ClientUnaryCall;
  UpdateQuestion(argument: _pando_api_survey_v1_UpdateQuestionRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_Question__Output>): grpc.ClientUnaryCall;
  UpdateQuestion(argument: _pando_api_survey_v1_UpdateQuestionRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Question__Output>): grpc.ClientUnaryCall;
  UpdateQuestion(argument: _pando_api_survey_v1_UpdateQuestionRequest, callback: grpc.requestCallback<_pando_api_survey_v1_Question__Output>): grpc.ClientUnaryCall;
  updateQuestion(argument: _pando_api_survey_v1_UpdateQuestionRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Question__Output>): grpc.ClientUnaryCall;
  updateQuestion(argument: _pando_api_survey_v1_UpdateQuestionRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_Question__Output>): grpc.ClientUnaryCall;
  updateQuestion(argument: _pando_api_survey_v1_UpdateQuestionRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Question__Output>): grpc.ClientUnaryCall;
  updateQuestion(argument: _pando_api_survey_v1_UpdateQuestionRequest, callback: grpc.requestCallback<_pando_api_survey_v1_Question__Output>): grpc.ClientUnaryCall;
  
  UpdateSurvey(argument: _pando_api_survey_v1_UpdateSurveyRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  UpdateSurvey(argument: _pando_api_survey_v1_UpdateSurveyRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  UpdateSurvey(argument: _pando_api_survey_v1_UpdateSurveyRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  UpdateSurvey(argument: _pando_api_survey_v1_UpdateSurveyRequest, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  updateSurvey(argument: _pando_api_survey_v1_UpdateSurveyRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  updateSurvey(argument: _pando_api_survey_v1_UpdateSurveyRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  updateSurvey(argument: _pando_api_survey_v1_UpdateSurveyRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  updateSurvey(argument: _pando_api_survey_v1_UpdateSurveyRequest, callback: grpc.requestCallback<_pando_api_survey_v1_Survey__Output>): grpc.ClientUnaryCall;
  
  UpdateSurveyQuestionDestination(argument: _pando_api_survey_v1_UpdateSurveyQuestionDestinationRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestination__Output>): grpc.ClientUnaryCall;
  UpdateSurveyQuestionDestination(argument: _pando_api_survey_v1_UpdateSurveyQuestionDestinationRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestination__Output>): grpc.ClientUnaryCall;
  UpdateSurveyQuestionDestination(argument: _pando_api_survey_v1_UpdateSurveyQuestionDestinationRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestination__Output>): grpc.ClientUnaryCall;
  UpdateSurveyQuestionDestination(argument: _pando_api_survey_v1_UpdateSurveyQuestionDestinationRequest, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestination__Output>): grpc.ClientUnaryCall;
  updateSurveyQuestionDestination(argument: _pando_api_survey_v1_UpdateSurveyQuestionDestinationRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestination__Output>): grpc.ClientUnaryCall;
  updateSurveyQuestionDestination(argument: _pando_api_survey_v1_UpdateSurveyQuestionDestinationRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestination__Output>): grpc.ClientUnaryCall;
  updateSurveyQuestionDestination(argument: _pando_api_survey_v1_UpdateSurveyQuestionDestinationRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestination__Output>): grpc.ClientUnaryCall;
  updateSurveyQuestionDestination(argument: _pando_api_survey_v1_UpdateSurveyQuestionDestinationRequest, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyQuestionDestination__Output>): grpc.ClientUnaryCall;
  
  UpdateVariable(argument: _pando_api_survey_v1_UpdateVariableRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Variable__Output>): grpc.ClientUnaryCall;
  UpdateVariable(argument: _pando_api_survey_v1_UpdateVariableRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_Variable__Output>): grpc.ClientUnaryCall;
  UpdateVariable(argument: _pando_api_survey_v1_UpdateVariableRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Variable__Output>): grpc.ClientUnaryCall;
  UpdateVariable(argument: _pando_api_survey_v1_UpdateVariableRequest, callback: grpc.requestCallback<_pando_api_survey_v1_Variable__Output>): grpc.ClientUnaryCall;
  updateVariable(argument: _pando_api_survey_v1_UpdateVariableRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Variable__Output>): grpc.ClientUnaryCall;
  updateVariable(argument: _pando_api_survey_v1_UpdateVariableRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_Variable__Output>): grpc.ClientUnaryCall;
  updateVariable(argument: _pando_api_survey_v1_UpdateVariableRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_Variable__Output>): grpc.ClientUnaryCall;
  updateVariable(argument: _pando_api_survey_v1_UpdateVariableRequest, callback: grpc.requestCallback<_pando_api_survey_v1_Variable__Output>): grpc.ClientUnaryCall;
  
  UploadSurveyMedia(metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_UploadSurveyMediaResponse__Output>): grpc.ClientWritableStream<_pando_api_FileChunk>;
  UploadSurveyMedia(metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_UploadSurveyMediaResponse__Output>): grpc.ClientWritableStream<_pando_api_FileChunk>;
  UploadSurveyMedia(options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_UploadSurveyMediaResponse__Output>): grpc.ClientWritableStream<_pando_api_FileChunk>;
  UploadSurveyMedia(callback: grpc.requestCallback<_pando_api_survey_v1_UploadSurveyMediaResponse__Output>): grpc.ClientWritableStream<_pando_api_FileChunk>;
  uploadSurveyMedia(metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_UploadSurveyMediaResponse__Output>): grpc.ClientWritableStream<_pando_api_FileChunk>;
  uploadSurveyMedia(metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_UploadSurveyMediaResponse__Output>): grpc.ClientWritableStream<_pando_api_FileChunk>;
  uploadSurveyMedia(options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_UploadSurveyMediaResponse__Output>): grpc.ClientWritableStream<_pando_api_FileChunk>;
  uploadSurveyMedia(callback: grpc.requestCallback<_pando_api_survey_v1_UploadSurveyMediaResponse__Output>): grpc.ClientWritableStream<_pando_api_FileChunk>;
  
  UploadVariableMedia(metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_UploadVariableMediaResponse__Output>): grpc.ClientWritableStream<_pando_api_FileChunk>;
  UploadVariableMedia(metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_UploadVariableMediaResponse__Output>): grpc.ClientWritableStream<_pando_api_FileChunk>;
  UploadVariableMedia(options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_UploadVariableMediaResponse__Output>): grpc.ClientWritableStream<_pando_api_FileChunk>;
  UploadVariableMedia(callback: grpc.requestCallback<_pando_api_survey_v1_UploadVariableMediaResponse__Output>): grpc.ClientWritableStream<_pando_api_FileChunk>;
  uploadVariableMedia(metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_UploadVariableMediaResponse__Output>): grpc.ClientWritableStream<_pando_api_FileChunk>;
  uploadVariableMedia(metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_UploadVariableMediaResponse__Output>): grpc.ClientWritableStream<_pando_api_FileChunk>;
  uploadVariableMedia(options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_UploadVariableMediaResponse__Output>): grpc.ClientWritableStream<_pando_api_FileChunk>;
  uploadVariableMedia(callback: grpc.requestCallback<_pando_api_survey_v1_UploadVariableMediaResponse__Output>): grpc.ClientWritableStream<_pando_api_FileChunk>;
  
}

export interface SurveyAdminServiceHandlers extends grpc.UntypedServiceImplementation {
  CreateAnswerOption: grpc.handleUnaryCall<_pando_api_survey_v1_AnswerOption__Output, _pando_api_survey_v1_AnswerOption>;
  
  CreateInstance: grpc.handleUnaryCall<_pando_api_survey_v1_CreateInstanceRequest__Output, _pando_api_survey_v1_Instance>;
  
  CreatePlaybackData: grpc.handleUnaryCall<_pando_api_survey_v1_PlaybackData__Output, _pando_api_survey_v1_PlaybackData>;
  
  CreateQuestion: grpc.handleUnaryCall<_pando_api_survey_v1_Question__Output, _pando_api_survey_v1_Question>;
  
  CreateSurvey: grpc.handleUnaryCall<_pando_api_survey_v1_Survey__Output, _pando_api_survey_v1_Survey>;
  
  CreateSurveyQuestionDestination: grpc.handleUnaryCall<_pando_api_survey_v1_SurveyQuestionDestination__Output, _pando_api_survey_v1_SurveyQuestionDestination>;
  
  CreateSurveyVariable: grpc.handleUnaryCall<_pando_api_survey_v1_SurveyVariable__Output, _pando_api_survey_v1_SurveyVariable>;
  
  CreateSurveyVersion: grpc.handleUnaryCall<_pando_api_survey_v1_CreateSurveyVersionRequest__Output, _pando_api_survey_v1_Survey>;
  
  CreateVariable: grpc.handleUnaryCall<_pando_api_survey_v1_Variable__Output, _pando_api_survey_v1_Variable>;
  
  CreateVariables: grpc.handleUnaryCall<_pando_api_survey_v1_CreateVariablesRequest__Output, _pando_api_survey_v1_VariableList>;
  
  DeleteAnswerOption: grpc.handleUnaryCall<_pando_api_survey_v1_DeleteAnswerOptionRequest__Output, _pando_api_Empty>;
  
  DeleteInstance: grpc.handleUnaryCall<_pando_api_survey_v1_DeleteInstanceRequest__Output, _pando_api_Empty>;
  
  DeleteMedia: grpc.handleUnaryCall<_pando_api_survey_v1_DeleteMediaRequest__Output, _pando_api_Empty>;
  
  DeletePlaybackData: grpc.handleUnaryCall<_pando_api_survey_v1_DeletePlaybackDataRequest__Output, _pando_api_Empty>;
  
  DeleteQuestion: grpc.handleUnaryCall<_pando_api_survey_v1_DeleteQuestionRequest__Output, _pando_api_Empty>;
  
  DeleteSurvey: grpc.handleUnaryCall<_pando_api_survey_v1_DeleteSurveyRequest__Output, _pando_api_Empty>;
  
  DeleteSurveyMedia: grpc.handleUnaryCall<_pando_api_survey_v1_DeleteSurveyMediaRequest__Output, _pando_api_Empty>;
  
  DeleteSurveyQuestionDestination: grpc.handleUnaryCall<_pando_api_survey_v1_DeleteSurveyQuestionDestinationRequest__Output, _pando_api_Empty>;
  
  DeleteSurveyVariable: grpc.handleUnaryCall<_pando_api_survey_v1_DeleteSurveyVariableRequest__Output, _pando_api_Empty>;
  
  DeleteSurveyVersion: grpc.handleUnaryCall<_pando_api_survey_v1_DeleteSurveyVersionRequest__Output, _pando_api_Empty>;
  
  DeleteVariable: grpc.handleUnaryCall<_pando_api_survey_v1_DeleteVariableRequest__Output, _pando_api_Empty>;
  
  DeleteVariableMedia: grpc.handleUnaryCall<_pando_api_survey_v1_DeleteVariableMediaRequest__Output, _pando_api_Empty>;
  
  ExecAddSurveyESignTemplate: grpc.handleUnaryCall<_pando_api_survey_v1_SurveyESignTemplate__Output, _pando_api_survey_v1_SurveyESignTemplate>;
  
  ExecAddSurveyInstanceESignSignatory: grpc.handleUnaryCall<_pando_api_survey_v1_SurveyInstanceESignSignatory__Output, _pando_api_survey_v1_SurveyInstanceESignSignatory>;
  
  ExecRemoveSurveyESignTemplate: grpc.handleUnaryCall<_pando_api_survey_v1_SurveyESignTemplate__Output, _pando_api_Empty>;
  
  ExecRemoveSurveyInstanceESignSignatory: grpc.handleUnaryCall<_pando_api_survey_v1_SurveyInstanceESignSignatory__Output, _pando_api_Empty>;
  
  ExecSendInstanceLink: grpc.handleUnaryCall<_pando_api_survey_v1_SendInstanceLink__Output, _pando_api_survey_v1_SendInstanceLink>;
  
  GetAnswerOption: grpc.handleUnaryCall<_pando_api_survey_v1_GetAnswerOptionRequest__Output, _pando_api_survey_v1_AnswerOption>;
  
  GetAnswerOptions: grpc.handleUnaryCall<_pando_api_survey_v1_GetAnswerOptionsRequest__Output, _pando_api_survey_v1_AnswerOptionList>;
  
  GetPlaybackDataList: grpc.handleUnaryCall<_pando_api_survey_v1_GetPlaybackDataListRequest__Output, _pando_api_survey_v1_PlaybackDataList>;
  
  GetQuestion: grpc.handleUnaryCall<_pando_api_survey_v1_GetQuestionRequest__Output, _pando_api_survey_v1_Question>;
  
  GetQuestions: grpc.handleUnaryCall<_pando_api_survey_v1_GetQuestionsRequest__Output, _pando_api_survey_v1_SurveyQuestionList>;
  
  GetSurvey: grpc.handleUnaryCall<_pando_api_survey_v1_GetSurveyRequest__Output, _pando_api_survey_v1_Survey>;
  
  GetSurveyDocuments: grpc.handleUnaryCall<_pando_api_survey_v1_GetSurveyDocumentsRequest__Output, _pando_api_survey_v1_SurveyDocumentList>;
  
  GetSurveyMediaList: grpc.handleUnaryCall<_pando_api_survey_v1_GetSurveyMediaListRequest__Output, _pando_api_survey_v1_MediaList>;
  
  GetSurveyQuestionDestination: grpc.handleUnaryCall<_pando_api_survey_v1_GetSurveyQuestionDestinationRequest__Output, _pando_api_survey_v1_SurveyQuestionDestination>;
  
  GetSurveyQuestionDestinations: grpc.handleUnaryCall<_pando_api_survey_v1_GetSurveyQuestionDestinationsRequest__Output, _pando_api_survey_v1_SurveyQuestionDestinationList>;
  
  GetSurveyVariables: grpc.handleUnaryCall<_pando_api_survey_v1_GetSurveyVariablesRequest__Output, _pando_api_survey_v1_VariableList>;
  
  GetSurveyVersions: grpc.handleUnaryCall<_pando_api_survey_v1_GetSurveyVersionsRequest__Output, _pando_api_survey_v1_SurveyList>;
  
  GetSurveys: grpc.handleUnaryCall<_pando_api_survey_v1_GetSurveysRequest__Output, _pando_api_survey_v1_SurveyDetailList>;
  
  GetVariable: grpc.handleUnaryCall<_pando_api_survey_v1_GetVariableRequest__Output, _pando_api_survey_v1_Variable>;
  
  GetVariableMediaList: grpc.handleUnaryCall<_pando_api_survey_v1_GetVariableMediaListRequest__Output, _pando_api_survey_v1_MediaList>;
  
  GetVariableTemplate: grpc.handleUnaryCall<_pando_api_survey_v1_GetVariableTemplateRequest__Output, _pando_api_survey_v1_GetVariableTemplateResponse>;
  
  GetVariables: grpc.handleUnaryCall<_pando_api_survey_v1_GetVariablesRequest__Output, _pando_api_survey_v1_VariableList>;
  
  UpdateAnswerOption: grpc.handleUnaryCall<_pando_api_survey_v1_UpdateAnswerOptionRequest__Output, _pando_api_survey_v1_AnswerOption>;
  
  UpdatePlaybackData: grpc.handleUnaryCall<_pando_api_survey_v1_UpdatePlaybackDataRequest__Output, _pando_api_survey_v1_PlaybackData>;
  
  UpdateQuestion: grpc.handleUnaryCall<_pando_api_survey_v1_UpdateQuestionRequest__Output, _pando_api_survey_v1_Question>;
  
  UpdateSurvey: grpc.handleUnaryCall<_pando_api_survey_v1_UpdateSurveyRequest__Output, _pando_api_survey_v1_Survey>;
  
  UpdateSurveyQuestionDestination: grpc.handleUnaryCall<_pando_api_survey_v1_UpdateSurveyQuestionDestinationRequest__Output, _pando_api_survey_v1_SurveyQuestionDestination>;
  
  UpdateVariable: grpc.handleUnaryCall<_pando_api_survey_v1_UpdateVariableRequest__Output, _pando_api_survey_v1_Variable>;
  
  UploadSurveyMedia: grpc.handleClientStreamingCall<_pando_api_FileChunk__Output, _pando_api_survey_v1_UploadSurveyMediaResponse>;
  
  UploadVariableMedia: grpc.handleClientStreamingCall<_pando_api_FileChunk__Output, _pando_api_survey_v1_UploadVariableMediaResponse>;
  
}

export interface SurveyAdminServiceDefinition extends grpc.ServiceDefinition {
  CreateAnswerOption: MethodDefinition<_pando_api_survey_v1_AnswerOption, _pando_api_survey_v1_AnswerOption, _pando_api_survey_v1_AnswerOption__Output, _pando_api_survey_v1_AnswerOption__Output>
  CreateInstance: MethodDefinition<_pando_api_survey_v1_CreateInstanceRequest, _pando_api_survey_v1_Instance, _pando_api_survey_v1_CreateInstanceRequest__Output, _pando_api_survey_v1_Instance__Output>
  CreatePlaybackData: MethodDefinition<_pando_api_survey_v1_PlaybackData, _pando_api_survey_v1_PlaybackData, _pando_api_survey_v1_PlaybackData__Output, _pando_api_survey_v1_PlaybackData__Output>
  CreateQuestion: MethodDefinition<_pando_api_survey_v1_Question, _pando_api_survey_v1_Question, _pando_api_survey_v1_Question__Output, _pando_api_survey_v1_Question__Output>
  CreateSurvey: MethodDefinition<_pando_api_survey_v1_Survey, _pando_api_survey_v1_Survey, _pando_api_survey_v1_Survey__Output, _pando_api_survey_v1_Survey__Output>
  CreateSurveyQuestionDestination: MethodDefinition<_pando_api_survey_v1_SurveyQuestionDestination, _pando_api_survey_v1_SurveyQuestionDestination, _pando_api_survey_v1_SurveyQuestionDestination__Output, _pando_api_survey_v1_SurveyQuestionDestination__Output>
  CreateSurveyVariable: MethodDefinition<_pando_api_survey_v1_SurveyVariable, _pando_api_survey_v1_SurveyVariable, _pando_api_survey_v1_SurveyVariable__Output, _pando_api_survey_v1_SurveyVariable__Output>
  CreateSurveyVersion: MethodDefinition<_pando_api_survey_v1_CreateSurveyVersionRequest, _pando_api_survey_v1_Survey, _pando_api_survey_v1_CreateSurveyVersionRequest__Output, _pando_api_survey_v1_Survey__Output>
  CreateVariable: MethodDefinition<_pando_api_survey_v1_Variable, _pando_api_survey_v1_Variable, _pando_api_survey_v1_Variable__Output, _pando_api_survey_v1_Variable__Output>
  CreateVariables: MethodDefinition<_pando_api_survey_v1_CreateVariablesRequest, _pando_api_survey_v1_VariableList, _pando_api_survey_v1_CreateVariablesRequest__Output, _pando_api_survey_v1_VariableList__Output>
  DeleteAnswerOption: MethodDefinition<_pando_api_survey_v1_DeleteAnswerOptionRequest, _pando_api_Empty, _pando_api_survey_v1_DeleteAnswerOptionRequest__Output, _pando_api_Empty__Output>
  DeleteInstance: MethodDefinition<_pando_api_survey_v1_DeleteInstanceRequest, _pando_api_Empty, _pando_api_survey_v1_DeleteInstanceRequest__Output, _pando_api_Empty__Output>
  DeleteMedia: MethodDefinition<_pando_api_survey_v1_DeleteMediaRequest, _pando_api_Empty, _pando_api_survey_v1_DeleteMediaRequest__Output, _pando_api_Empty__Output>
  DeletePlaybackData: MethodDefinition<_pando_api_survey_v1_DeletePlaybackDataRequest, _pando_api_Empty, _pando_api_survey_v1_DeletePlaybackDataRequest__Output, _pando_api_Empty__Output>
  DeleteQuestion: MethodDefinition<_pando_api_survey_v1_DeleteQuestionRequest, _pando_api_Empty, _pando_api_survey_v1_DeleteQuestionRequest__Output, _pando_api_Empty__Output>
  DeleteSurvey: MethodDefinition<_pando_api_survey_v1_DeleteSurveyRequest, _pando_api_Empty, _pando_api_survey_v1_DeleteSurveyRequest__Output, _pando_api_Empty__Output>
  DeleteSurveyMedia: MethodDefinition<_pando_api_survey_v1_DeleteSurveyMediaRequest, _pando_api_Empty, _pando_api_survey_v1_DeleteSurveyMediaRequest__Output, _pando_api_Empty__Output>
  DeleteSurveyQuestionDestination: MethodDefinition<_pando_api_survey_v1_DeleteSurveyQuestionDestinationRequest, _pando_api_Empty, _pando_api_survey_v1_DeleteSurveyQuestionDestinationRequest__Output, _pando_api_Empty__Output>
  DeleteSurveyVariable: MethodDefinition<_pando_api_survey_v1_DeleteSurveyVariableRequest, _pando_api_Empty, _pando_api_survey_v1_DeleteSurveyVariableRequest__Output, _pando_api_Empty__Output>
  DeleteSurveyVersion: MethodDefinition<_pando_api_survey_v1_DeleteSurveyVersionRequest, _pando_api_Empty, _pando_api_survey_v1_DeleteSurveyVersionRequest__Output, _pando_api_Empty__Output>
  DeleteVariable: MethodDefinition<_pando_api_survey_v1_DeleteVariableRequest, _pando_api_Empty, _pando_api_survey_v1_DeleteVariableRequest__Output, _pando_api_Empty__Output>
  DeleteVariableMedia: MethodDefinition<_pando_api_survey_v1_DeleteVariableMediaRequest, _pando_api_Empty, _pando_api_survey_v1_DeleteVariableMediaRequest__Output, _pando_api_Empty__Output>
  ExecAddSurveyESignTemplate: MethodDefinition<_pando_api_survey_v1_SurveyESignTemplate, _pando_api_survey_v1_SurveyESignTemplate, _pando_api_survey_v1_SurveyESignTemplate__Output, _pando_api_survey_v1_SurveyESignTemplate__Output>
  ExecAddSurveyInstanceESignSignatory: MethodDefinition<_pando_api_survey_v1_SurveyInstanceESignSignatory, _pando_api_survey_v1_SurveyInstanceESignSignatory, _pando_api_survey_v1_SurveyInstanceESignSignatory__Output, _pando_api_survey_v1_SurveyInstanceESignSignatory__Output>
  ExecRemoveSurveyESignTemplate: MethodDefinition<_pando_api_survey_v1_SurveyESignTemplate, _pando_api_Empty, _pando_api_survey_v1_SurveyESignTemplate__Output, _pando_api_Empty__Output>
  ExecRemoveSurveyInstanceESignSignatory: MethodDefinition<_pando_api_survey_v1_SurveyInstanceESignSignatory, _pando_api_Empty, _pando_api_survey_v1_SurveyInstanceESignSignatory__Output, _pando_api_Empty__Output>
  ExecSendInstanceLink: MethodDefinition<_pando_api_survey_v1_SendInstanceLink, _pando_api_survey_v1_SendInstanceLink, _pando_api_survey_v1_SendInstanceLink__Output, _pando_api_survey_v1_SendInstanceLink__Output>
  GetAnswerOption: MethodDefinition<_pando_api_survey_v1_GetAnswerOptionRequest, _pando_api_survey_v1_AnswerOption, _pando_api_survey_v1_GetAnswerOptionRequest__Output, _pando_api_survey_v1_AnswerOption__Output>
  GetAnswerOptions: MethodDefinition<_pando_api_survey_v1_GetAnswerOptionsRequest, _pando_api_survey_v1_AnswerOptionList, _pando_api_survey_v1_GetAnswerOptionsRequest__Output, _pando_api_survey_v1_AnswerOptionList__Output>
  GetPlaybackDataList: MethodDefinition<_pando_api_survey_v1_GetPlaybackDataListRequest, _pando_api_survey_v1_PlaybackDataList, _pando_api_survey_v1_GetPlaybackDataListRequest__Output, _pando_api_survey_v1_PlaybackDataList__Output>
  GetQuestion: MethodDefinition<_pando_api_survey_v1_GetQuestionRequest, _pando_api_survey_v1_Question, _pando_api_survey_v1_GetQuestionRequest__Output, _pando_api_survey_v1_Question__Output>
  GetQuestions: MethodDefinition<_pando_api_survey_v1_GetQuestionsRequest, _pando_api_survey_v1_SurveyQuestionList, _pando_api_survey_v1_GetQuestionsRequest__Output, _pando_api_survey_v1_SurveyQuestionList__Output>
  GetSurvey: MethodDefinition<_pando_api_survey_v1_GetSurveyRequest, _pando_api_survey_v1_Survey, _pando_api_survey_v1_GetSurveyRequest__Output, _pando_api_survey_v1_Survey__Output>
  GetSurveyDocuments: MethodDefinition<_pando_api_survey_v1_GetSurveyDocumentsRequest, _pando_api_survey_v1_SurveyDocumentList, _pando_api_survey_v1_GetSurveyDocumentsRequest__Output, _pando_api_survey_v1_SurveyDocumentList__Output>
  GetSurveyMediaList: MethodDefinition<_pando_api_survey_v1_GetSurveyMediaListRequest, _pando_api_survey_v1_MediaList, _pando_api_survey_v1_GetSurveyMediaListRequest__Output, _pando_api_survey_v1_MediaList__Output>
  GetSurveyQuestionDestination: MethodDefinition<_pando_api_survey_v1_GetSurveyQuestionDestinationRequest, _pando_api_survey_v1_SurveyQuestionDestination, _pando_api_survey_v1_GetSurveyQuestionDestinationRequest__Output, _pando_api_survey_v1_SurveyQuestionDestination__Output>
  GetSurveyQuestionDestinations: MethodDefinition<_pando_api_survey_v1_GetSurveyQuestionDestinationsRequest, _pando_api_survey_v1_SurveyQuestionDestinationList, _pando_api_survey_v1_GetSurveyQuestionDestinationsRequest__Output, _pando_api_survey_v1_SurveyQuestionDestinationList__Output>
  GetSurveyVariables: MethodDefinition<_pando_api_survey_v1_GetSurveyVariablesRequest, _pando_api_survey_v1_VariableList, _pando_api_survey_v1_GetSurveyVariablesRequest__Output, _pando_api_survey_v1_VariableList__Output>
  GetSurveyVersions: MethodDefinition<_pando_api_survey_v1_GetSurveyVersionsRequest, _pando_api_survey_v1_SurveyList, _pando_api_survey_v1_GetSurveyVersionsRequest__Output, _pando_api_survey_v1_SurveyList__Output>
  GetSurveys: MethodDefinition<_pando_api_survey_v1_GetSurveysRequest, _pando_api_survey_v1_SurveyDetailList, _pando_api_survey_v1_GetSurveysRequest__Output, _pando_api_survey_v1_SurveyDetailList__Output>
  GetVariable: MethodDefinition<_pando_api_survey_v1_GetVariableRequest, _pando_api_survey_v1_Variable, _pando_api_survey_v1_GetVariableRequest__Output, _pando_api_survey_v1_Variable__Output>
  GetVariableMediaList: MethodDefinition<_pando_api_survey_v1_GetVariableMediaListRequest, _pando_api_survey_v1_MediaList, _pando_api_survey_v1_GetVariableMediaListRequest__Output, _pando_api_survey_v1_MediaList__Output>
  GetVariableTemplate: MethodDefinition<_pando_api_survey_v1_GetVariableTemplateRequest, _pando_api_survey_v1_GetVariableTemplateResponse, _pando_api_survey_v1_GetVariableTemplateRequest__Output, _pando_api_survey_v1_GetVariableTemplateResponse__Output>
  GetVariables: MethodDefinition<_pando_api_survey_v1_GetVariablesRequest, _pando_api_survey_v1_VariableList, _pando_api_survey_v1_GetVariablesRequest__Output, _pando_api_survey_v1_VariableList__Output>
  UpdateAnswerOption: MethodDefinition<_pando_api_survey_v1_UpdateAnswerOptionRequest, _pando_api_survey_v1_AnswerOption, _pando_api_survey_v1_UpdateAnswerOptionRequest__Output, _pando_api_survey_v1_AnswerOption__Output>
  UpdatePlaybackData: MethodDefinition<_pando_api_survey_v1_UpdatePlaybackDataRequest, _pando_api_survey_v1_PlaybackData, _pando_api_survey_v1_UpdatePlaybackDataRequest__Output, _pando_api_survey_v1_PlaybackData__Output>
  UpdateQuestion: MethodDefinition<_pando_api_survey_v1_UpdateQuestionRequest, _pando_api_survey_v1_Question, _pando_api_survey_v1_UpdateQuestionRequest__Output, _pando_api_survey_v1_Question__Output>
  UpdateSurvey: MethodDefinition<_pando_api_survey_v1_UpdateSurveyRequest, _pando_api_survey_v1_Survey, _pando_api_survey_v1_UpdateSurveyRequest__Output, _pando_api_survey_v1_Survey__Output>
  UpdateSurveyQuestionDestination: MethodDefinition<_pando_api_survey_v1_UpdateSurveyQuestionDestinationRequest, _pando_api_survey_v1_SurveyQuestionDestination, _pando_api_survey_v1_UpdateSurveyQuestionDestinationRequest__Output, _pando_api_survey_v1_SurveyQuestionDestination__Output>
  UpdateVariable: MethodDefinition<_pando_api_survey_v1_UpdateVariableRequest, _pando_api_survey_v1_Variable, _pando_api_survey_v1_UpdateVariableRequest__Output, _pando_api_survey_v1_Variable__Output>
  UploadSurveyMedia: MethodDefinition<_pando_api_FileChunk, _pando_api_survey_v1_UploadSurveyMediaResponse, _pando_api_FileChunk__Output, _pando_api_survey_v1_UploadSurveyMediaResponse__Output>
  UploadVariableMedia: MethodDefinition<_pando_api_FileChunk, _pando_api_survey_v1_UploadVariableMediaResponse, _pando_api_FileChunk__Output, _pando_api_survey_v1_UploadVariableMediaResponse__Output>
}
