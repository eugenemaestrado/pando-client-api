import type { Timestamp as _google_protobuf_Timestamp, Timestamp__Output as _google_protobuf_Timestamp__Output } from '../../../../google/protobuf/Timestamp';
import type { Version as _pando_api_survey_v1_Version, Version__Output as _pando_api_survey_v1_Version__Output } from '../../../../pando/api/survey/v1/Version';
export interface SurveyDetail {
    'guid'?: (string);
    'organizationCode'?: (string);
    'name'?: (string);
    'description'?: (string);
    'preSurveyText'?: (string);
    'firstQuestionGuid'?: (string);
    'isDeleted'?: (boolean);
    'organizationName'?: (string);
    'versionGuid'?: (string);
    'numberOfInstances'?: (number);
    'datePublished'?: (_google_protobuf_Timestamp | null);
    'isCurrentSurveyVersion'?: (boolean);
    'versions'?: (_pando_api_survey_v1_Version)[];
}
export interface SurveyDetail__Output {
    'guid'?: (string);
    'organizationCode'?: (string);
    'name'?: (string);
    'description'?: (string);
    'preSurveyText'?: (string);
    'firstQuestionGuid'?: (string);
    'isDeleted'?: (boolean);
    'organizationName'?: (string);
    'versionGuid'?: (string);
    'numberOfInstances'?: (number);
    'datePublished'?: (_google_protobuf_Timestamp__Output);
    'isCurrentSurveyVersion'?: (boolean);
    'versions'?: (_pando_api_survey_v1_Version__Output)[];
}
