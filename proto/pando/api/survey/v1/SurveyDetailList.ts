// Original file: proto/pando/api/survey/v1/shared.proto

import type { SurveyDetail as _pando_api_survey_v1_SurveyDetail, SurveyDetail__Output as _pando_api_survey_v1_SurveyDetail__Output } from '../../../../pando/api/survey/v1/SurveyDetail';
import type { PaginationResult as _pando_api_PaginationResult, PaginationResult__Output as _pando_api_PaginationResult__Output } from '../../../../pando/api/PaginationResult';

export interface SurveyDetailList {
  'surveys'?: (_pando_api_survey_v1_SurveyDetail)[];
  'pagination'?: (_pando_api_PaginationResult | null);
}

export interface SurveyDetailList__Output {
  'surveys'?: (_pando_api_survey_v1_SurveyDetail__Output)[];
  'pagination'?: (_pando_api_PaginationResult__Output);
}
