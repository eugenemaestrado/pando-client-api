import type { PaginationResult as _pando_api_PaginationResult, PaginationResult__Output as _pando_api_PaginationResult__Output } from '../../../../pando/api/PaginationResult';
export interface SurveyDocumentList {
    'templateGuids'?: (string)[];
    'pagination'?: (_pando_api_PaginationResult | null);
}
export interface SurveyDocumentList__Output {
    'templateGuids'?: (string)[];
    'pagination'?: (_pando_api_PaginationResult__Output);
}
