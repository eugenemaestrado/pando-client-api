export interface SurveyESignTemplate {
    'surveyGuid'?: (string);
    'templateGuid'?: (string);
    'isRequired'?: (boolean);
}
export interface SurveyESignTemplate__Output {
    'surveyGuid'?: (string);
    'templateGuid'?: (string);
    'isRequired'?: (boolean);
}
