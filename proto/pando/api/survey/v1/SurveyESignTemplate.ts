// Original file: proto/pando/api/survey/v1/admin.proto


export interface SurveyESignTemplate {
  'surveyGuid'?: (string);
  'templateGuid'?: (string);
  'isRequired'?: (boolean);
}

export interface SurveyESignTemplate__Output {
  'surveyGuid'?: (string);
  'templateGuid'?: (string);
  'isRequired'?: (boolean);
}
