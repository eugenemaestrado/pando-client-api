// Original file: proto/pando/api/survey/v1/admin.proto


export interface SurveyInstanceESignSignatory {
  'instanceGuid'?: (string);
  'signatoryGuid'?: (string);
}

export interface SurveyInstanceESignSignatory__Output {
  'instanceGuid'?: (string);
  'signatoryGuid'?: (string);
}
