// Original file: proto/pando/api/survey/v1/reports.proto

import type { Timestamp as _google_protobuf_Timestamp, Timestamp__Output as _google_protobuf_Timestamp__Output } from '../../../../google/protobuf/Timestamp';

export interface SurveyInstanceStatus {
  'guid'?: (string);
  'organizationName'?: (string);
  'dateCreated'?: (_google_protobuf_Timestamp | null);
  'contactInfo'?: (string);
  'respondentName'?: (string);
  'dateComplete'?: (_google_protobuf_Timestamp | null);
  'name'?: (string);
}

export interface SurveyInstanceStatus__Output {
  'guid'?: (string);
  'organizationName'?: (string);
  'dateCreated'?: (_google_protobuf_Timestamp__Output);
  'contactInfo'?: (string);
  'respondentName'?: (string);
  'dateComplete'?: (_google_protobuf_Timestamp__Output);
  'name'?: (string);
}
