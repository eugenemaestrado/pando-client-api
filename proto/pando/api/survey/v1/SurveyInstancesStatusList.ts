// Original file: proto/pando/api/survey/v1/reports.proto

import type { SurveyInstanceStatus as _pando_api_survey_v1_SurveyInstanceStatus, SurveyInstanceStatus__Output as _pando_api_survey_v1_SurveyInstanceStatus__Output } from '../../../../pando/api/survey/v1/SurveyInstanceStatus';
import type { ChartData as _pando_api_survey_v1_ChartData, ChartData__Output as _pando_api_survey_v1_ChartData__Output } from '../../../../pando/api/survey/v1/ChartData';
import type { PaginationResult as _pando_api_PaginationResult, PaginationResult__Output as _pando_api_PaginationResult__Output } from '../../../../pando/api/PaginationResult';

export interface SurveyInstancesStatusList {
  'surveyInstanceStatus'?: (_pando_api_survey_v1_SurveyInstanceStatus)[];
  'chartData'?: (_pando_api_survey_v1_ChartData)[];
  'pagination'?: (_pando_api_PaginationResult | null);
}

export interface SurveyInstancesStatusList__Output {
  'surveyInstanceStatus'?: (_pando_api_survey_v1_SurveyInstanceStatus__Output)[];
  'chartData'?: (_pando_api_survey_v1_ChartData__Output)[];
  'pagination'?: (_pando_api_PaginationResult__Output);
}
