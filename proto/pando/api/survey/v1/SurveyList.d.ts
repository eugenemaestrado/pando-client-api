import type { Survey as _pando_api_survey_v1_Survey, Survey__Output as _pando_api_survey_v1_Survey__Output } from '../../../../pando/api/survey/v1/Survey';
import type { PaginationResult as _pando_api_PaginationResult, PaginationResult__Output as _pando_api_PaginationResult__Output } from '../../../../pando/api/PaginationResult';
export interface SurveyList {
    'surveys'?: (_pando_api_survey_v1_Survey)[];
    'pagination'?: (_pando_api_PaginationResult | null);
}
export interface SurveyList__Output {
    'surveys'?: (_pando_api_survey_v1_Survey__Output)[];
    'pagination'?: (_pando_api_PaginationResult__Output);
}
