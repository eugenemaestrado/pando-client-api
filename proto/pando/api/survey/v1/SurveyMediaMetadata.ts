// Original file: proto/pando/api/survey/v1/shared.proto

import type { Status as _pando_api_survey_v1_Status } from '../../../../pando/api/survey/v1/Status';
import type { MediaInfo as _pando_api_survey_v1_MediaInfo, MediaInfo__Output as _pando_api_survey_v1_MediaInfo__Output } from '../../../../pando/api/survey/v1/MediaInfo';

export interface SurveyMediaMetadata {
  'surveyMediaGuid'?: (string);
  'status'?: (_pando_api_survey_v1_Status | keyof typeof _pando_api_survey_v1_Status);
  'info'?: (_pando_api_survey_v1_MediaInfo | null);
}

export interface SurveyMediaMetadata__Output {
  'surveyMediaGuid'?: (string);
  'status'?: (_pando_api_survey_v1_Status);
  'info'?: (_pando_api_survey_v1_MediaInfo__Output);
}
