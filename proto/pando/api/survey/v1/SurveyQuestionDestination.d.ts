export interface SurveyQuestionDestination {
    'questionGuid'?: (string);
    'destinationQuestionGuid'?: (string);
    'condition'?: (string);
    'priority'?: (number);
    'guid'?: (string);
    'isDeleted'?: (boolean);
}
export interface SurveyQuestionDestination__Output {
    'questionGuid'?: (string);
    'destinationQuestionGuid'?: (string);
    'condition'?: (string);
    'priority'?: (number);
    'guid'?: (string);
    'isDeleted'?: (boolean);
}
