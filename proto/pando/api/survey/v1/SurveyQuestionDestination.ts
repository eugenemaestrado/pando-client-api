// Original file: proto/pando/api/survey/v1/shared.proto


export interface SurveyQuestionDestination {
  'questionGuid'?: (string);
  'destinationQuestionGuid'?: (string);
  'condition'?: (string);
  'priority'?: (number);
  'guid'?: (string);
  'isDeleted'?: (boolean);
}

export interface SurveyQuestionDestination__Output {
  'questionGuid'?: (string);
  'destinationQuestionGuid'?: (string);
  'condition'?: (string);
  'priority'?: (number);
  'guid'?: (string);
  'isDeleted'?: (boolean);
}
