import type { SurveyQuestionDestination as _pando_api_survey_v1_SurveyQuestionDestination, SurveyQuestionDestination__Output as _pando_api_survey_v1_SurveyQuestionDestination__Output } from '../../../../pando/api/survey/v1/SurveyQuestionDestination';
import type { PaginationResult as _pando_api_PaginationResult, PaginationResult__Output as _pando_api_PaginationResult__Output } from '../../../../pando/api/PaginationResult';
export interface SurveyQuestionDestinationList {
    'destinations'?: (_pando_api_survey_v1_SurveyQuestionDestination)[];
    'pagination'?: (_pando_api_PaginationResult | null);
}
export interface SurveyQuestionDestinationList__Output {
    'destinations'?: (_pando_api_survey_v1_SurveyQuestionDestination__Output)[];
    'pagination'?: (_pando_api_PaginationResult__Output);
}
