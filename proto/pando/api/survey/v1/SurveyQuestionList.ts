// Original file: proto/pando/api/survey/v1/shared.proto

import type { Question as _pando_api_survey_v1_Question, Question__Output as _pando_api_survey_v1_Question__Output } from '../../../../pando/api/survey/v1/Question';
import type { PaginationResult as _pando_api_PaginationResult, PaginationResult__Output as _pando_api_PaginationResult__Output } from '../../../../pando/api/PaginationResult';

export interface SurveyQuestionList {
  'surveyQuestions'?: (_pando_api_survey_v1_Question)[];
  'pagination'?: (_pando_api_PaginationResult | null);
}

export interface SurveyQuestionList__Output {
  'surveyQuestions'?: (_pando_api_survey_v1_Question__Output)[];
  'pagination'?: (_pando_api_PaginationResult__Output);
}
