import type * as grpc from '@grpc/grpc-js';
import type { MethodDefinition } from '@grpc/proto-loader';
import type { GetInstancesReportRequest as _pando_api_survey_v1_GetInstancesReportRequest, GetInstancesReportRequest__Output as _pando_api_survey_v1_GetInstancesReportRequest__Output } from '../../../../pando/api/survey/v1/GetInstancesReportRequest';
import type { GetSurveyInstanceStatusesRequest as _pando_api_survey_v1_GetSurveyInstanceStatusesRequest, GetSurveyInstanceStatusesRequest__Output as _pando_api_survey_v1_GetSurveyInstanceStatusesRequest__Output } from '../../../../pando/api/survey/v1/GetSurveyInstanceStatusesRequest';
import type { InstanceList as _pando_api_survey_v1_InstanceList, InstanceList__Output as _pando_api_survey_v1_InstanceList__Output } from '../../../../pando/api/survey/v1/InstanceList';
import type { SurveyInstancesStatusList as _pando_api_survey_v1_SurveyInstancesStatusList, SurveyInstancesStatusList__Output as _pando_api_survey_v1_SurveyInstancesStatusList__Output } from '../../../../pando/api/survey/v1/SurveyInstancesStatusList';
export interface SurveyReportServiceClient extends grpc.Client {
    GetCompletedSurveyInstances(argument: _pando_api_survey_v1_GetInstancesReportRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_InstanceList__Output>): grpc.ClientUnaryCall;
    GetCompletedSurveyInstances(argument: _pando_api_survey_v1_GetInstancesReportRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_InstanceList__Output>): grpc.ClientUnaryCall;
    GetCompletedSurveyInstances(argument: _pando_api_survey_v1_GetInstancesReportRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_InstanceList__Output>): grpc.ClientUnaryCall;
    GetCompletedSurveyInstances(argument: _pando_api_survey_v1_GetInstancesReportRequest, callback: grpc.requestCallback<_pando_api_survey_v1_InstanceList__Output>): grpc.ClientUnaryCall;
    getCompletedSurveyInstances(argument: _pando_api_survey_v1_GetInstancesReportRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_InstanceList__Output>): grpc.ClientUnaryCall;
    getCompletedSurveyInstances(argument: _pando_api_survey_v1_GetInstancesReportRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_InstanceList__Output>): grpc.ClientUnaryCall;
    getCompletedSurveyInstances(argument: _pando_api_survey_v1_GetInstancesReportRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_InstanceList__Output>): grpc.ClientUnaryCall;
    getCompletedSurveyInstances(argument: _pando_api_survey_v1_GetInstancesReportRequest, callback: grpc.requestCallback<_pando_api_survey_v1_InstanceList__Output>): grpc.ClientUnaryCall;
    GetIncompleteSurveyInstances(argument: _pando_api_survey_v1_GetInstancesReportRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_InstanceList__Output>): grpc.ClientUnaryCall;
    GetIncompleteSurveyInstances(argument: _pando_api_survey_v1_GetInstancesReportRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_InstanceList__Output>): grpc.ClientUnaryCall;
    GetIncompleteSurveyInstances(argument: _pando_api_survey_v1_GetInstancesReportRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_InstanceList__Output>): grpc.ClientUnaryCall;
    GetIncompleteSurveyInstances(argument: _pando_api_survey_v1_GetInstancesReportRequest, callback: grpc.requestCallback<_pando_api_survey_v1_InstanceList__Output>): grpc.ClientUnaryCall;
    getIncompleteSurveyInstances(argument: _pando_api_survey_v1_GetInstancesReportRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_InstanceList__Output>): grpc.ClientUnaryCall;
    getIncompleteSurveyInstances(argument: _pando_api_survey_v1_GetInstancesReportRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_InstanceList__Output>): grpc.ClientUnaryCall;
    getIncompleteSurveyInstances(argument: _pando_api_survey_v1_GetInstancesReportRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_InstanceList__Output>): grpc.ClientUnaryCall;
    getIncompleteSurveyInstances(argument: _pando_api_survey_v1_GetInstancesReportRequest, callback: grpc.requestCallback<_pando_api_survey_v1_InstanceList__Output>): grpc.ClientUnaryCall;
    GetRecentlyCompletedSurveyInstances(argument: _pando_api_survey_v1_GetInstancesReportRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_InstanceList__Output>): grpc.ClientUnaryCall;
    GetRecentlyCompletedSurveyInstances(argument: _pando_api_survey_v1_GetInstancesReportRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_InstanceList__Output>): grpc.ClientUnaryCall;
    GetRecentlyCompletedSurveyInstances(argument: _pando_api_survey_v1_GetInstancesReportRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_InstanceList__Output>): grpc.ClientUnaryCall;
    GetRecentlyCompletedSurveyInstances(argument: _pando_api_survey_v1_GetInstancesReportRequest, callback: grpc.requestCallback<_pando_api_survey_v1_InstanceList__Output>): grpc.ClientUnaryCall;
    getRecentlyCompletedSurveyInstances(argument: _pando_api_survey_v1_GetInstancesReportRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_InstanceList__Output>): grpc.ClientUnaryCall;
    getRecentlyCompletedSurveyInstances(argument: _pando_api_survey_v1_GetInstancesReportRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_InstanceList__Output>): grpc.ClientUnaryCall;
    getRecentlyCompletedSurveyInstances(argument: _pando_api_survey_v1_GetInstancesReportRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_InstanceList__Output>): grpc.ClientUnaryCall;
    getRecentlyCompletedSurveyInstances(argument: _pando_api_survey_v1_GetInstancesReportRequest, callback: grpc.requestCallback<_pando_api_survey_v1_InstanceList__Output>): grpc.ClientUnaryCall;
    GetSurveyInstanceStatuses(argument: _pando_api_survey_v1_GetSurveyInstanceStatusesRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyInstancesStatusList__Output>): grpc.ClientUnaryCall;
    GetSurveyInstanceStatuses(argument: _pando_api_survey_v1_GetSurveyInstanceStatusesRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyInstancesStatusList__Output>): grpc.ClientUnaryCall;
    GetSurveyInstanceStatuses(argument: _pando_api_survey_v1_GetSurveyInstanceStatusesRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyInstancesStatusList__Output>): grpc.ClientUnaryCall;
    GetSurveyInstanceStatuses(argument: _pando_api_survey_v1_GetSurveyInstanceStatusesRequest, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyInstancesStatusList__Output>): grpc.ClientUnaryCall;
    getSurveyInstanceStatuses(argument: _pando_api_survey_v1_GetSurveyInstanceStatusesRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyInstancesStatusList__Output>): grpc.ClientUnaryCall;
    getSurveyInstanceStatuses(argument: _pando_api_survey_v1_GetSurveyInstanceStatusesRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyInstancesStatusList__Output>): grpc.ClientUnaryCall;
    getSurveyInstanceStatuses(argument: _pando_api_survey_v1_GetSurveyInstanceStatusesRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyInstancesStatusList__Output>): grpc.ClientUnaryCall;
    getSurveyInstanceStatuses(argument: _pando_api_survey_v1_GetSurveyInstanceStatusesRequest, callback: grpc.requestCallback<_pando_api_survey_v1_SurveyInstancesStatusList__Output>): grpc.ClientUnaryCall;
}
export interface SurveyReportServiceHandlers extends grpc.UntypedServiceImplementation {
    GetCompletedSurveyInstances: grpc.handleUnaryCall<_pando_api_survey_v1_GetInstancesReportRequest__Output, _pando_api_survey_v1_InstanceList>;
    GetIncompleteSurveyInstances: grpc.handleUnaryCall<_pando_api_survey_v1_GetInstancesReportRequest__Output, _pando_api_survey_v1_InstanceList>;
    GetRecentlyCompletedSurveyInstances: grpc.handleUnaryCall<_pando_api_survey_v1_GetInstancesReportRequest__Output, _pando_api_survey_v1_InstanceList>;
    GetSurveyInstanceStatuses: grpc.handleUnaryCall<_pando_api_survey_v1_GetSurveyInstanceStatusesRequest__Output, _pando_api_survey_v1_SurveyInstancesStatusList>;
}
export interface SurveyReportServiceDefinition extends grpc.ServiceDefinition {
    GetCompletedSurveyInstances: MethodDefinition<_pando_api_survey_v1_GetInstancesReportRequest, _pando_api_survey_v1_InstanceList, _pando_api_survey_v1_GetInstancesReportRequest__Output, _pando_api_survey_v1_InstanceList__Output>;
    GetIncompleteSurveyInstances: MethodDefinition<_pando_api_survey_v1_GetInstancesReportRequest, _pando_api_survey_v1_InstanceList, _pando_api_survey_v1_GetInstancesReportRequest__Output, _pando_api_survey_v1_InstanceList__Output>;
    GetRecentlyCompletedSurveyInstances: MethodDefinition<_pando_api_survey_v1_GetInstancesReportRequest, _pando_api_survey_v1_InstanceList, _pando_api_survey_v1_GetInstancesReportRequest__Output, _pando_api_survey_v1_InstanceList__Output>;
    GetSurveyInstanceStatuses: MethodDefinition<_pando_api_survey_v1_GetSurveyInstanceStatusesRequest, _pando_api_survey_v1_SurveyInstancesStatusList, _pando_api_survey_v1_GetSurveyInstanceStatusesRequest__Output, _pando_api_survey_v1_SurveyInstancesStatusList__Output>;
}
