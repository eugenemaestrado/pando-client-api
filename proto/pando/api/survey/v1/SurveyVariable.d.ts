export interface SurveyVariable {
    'surveyGuid'?: (string);
    'variableGuid'?: (string);
    'required'?: (boolean);
}
export interface SurveyVariable__Output {
    'surveyGuid'?: (string);
    'variableGuid'?: (string);
    'required'?: (boolean);
}
