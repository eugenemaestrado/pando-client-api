import type { AnswerOption as _pando_api_survey_v1_AnswerOption, AnswerOption__Output as _pando_api_survey_v1_AnswerOption__Output } from '../../../../pando/api/survey/v1/AnswerOption';
import type { FieldMask as _google_protobuf_FieldMask, FieldMask__Output as _google_protobuf_FieldMask__Output } from '../../../../google/protobuf/FieldMask';
export interface UpdateAnswerOptionRequest {
    'answerOption'?: (_pando_api_survey_v1_AnswerOption | null);
    'updateMask'?: (_google_protobuf_FieldMask | null);
}
export interface UpdateAnswerOptionRequest__Output {
    'answerOption'?: (_pando_api_survey_v1_AnswerOption__Output);
    'updateMask'?: (_google_protobuf_FieldMask__Output);
}
