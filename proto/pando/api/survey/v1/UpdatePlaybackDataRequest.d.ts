import type { PlaybackData as _pando_api_survey_v1_PlaybackData, PlaybackData__Output as _pando_api_survey_v1_PlaybackData__Output } from '../../../../pando/api/survey/v1/PlaybackData';
import type { FieldMask as _google_protobuf_FieldMask, FieldMask__Output as _google_protobuf_FieldMask__Output } from '../../../../google/protobuf/FieldMask';
export interface UpdatePlaybackDataRequest {
    'playbackData'?: (_pando_api_survey_v1_PlaybackData | null);
    'updateMask'?: (_google_protobuf_FieldMask | null);
}
export interface UpdatePlaybackDataRequest__Output {
    'playbackData'?: (_pando_api_survey_v1_PlaybackData__Output);
    'updateMask'?: (_google_protobuf_FieldMask__Output);
}
