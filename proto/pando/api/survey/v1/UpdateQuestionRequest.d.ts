import type { Question as _pando_api_survey_v1_Question, Question__Output as _pando_api_survey_v1_Question__Output } from '../../../../pando/api/survey/v1/Question';
import type { FieldMask as _google_protobuf_FieldMask, FieldMask__Output as _google_protobuf_FieldMask__Output } from '../../../../google/protobuf/FieldMask';
export interface UpdateQuestionRequest {
    'question'?: (_pando_api_survey_v1_Question | null);
    'updateMask'?: (_google_protobuf_FieldMask | null);
}
export interface UpdateQuestionRequest__Output {
    'question'?: (_pando_api_survey_v1_Question__Output);
    'updateMask'?: (_google_protobuf_FieldMask__Output);
}
