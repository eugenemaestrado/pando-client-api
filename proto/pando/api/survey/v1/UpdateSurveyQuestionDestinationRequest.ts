// Original file: proto/pando/api/survey/v1/admin.proto

import type { SurveyQuestionDestination as _pando_api_survey_v1_SurveyQuestionDestination, SurveyQuestionDestination__Output as _pando_api_survey_v1_SurveyQuestionDestination__Output } from '../../../../pando/api/survey/v1/SurveyQuestionDestination';
import type { FieldMask as _google_protobuf_FieldMask, FieldMask__Output as _google_protobuf_FieldMask__Output } from '../../../../google/protobuf/FieldMask';

export interface UpdateSurveyQuestionDestinationRequest {
  'destination'?: (_pando_api_survey_v1_SurveyQuestionDestination | null);
  'updateMask'?: (_google_protobuf_FieldMask | null);
}

export interface UpdateSurveyQuestionDestinationRequest__Output {
  'destination'?: (_pando_api_survey_v1_SurveyQuestionDestination__Output);
  'updateMask'?: (_google_protobuf_FieldMask__Output);
}
