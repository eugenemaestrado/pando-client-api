// Original file: proto/pando/api/survey/v1/admin.proto

import type { Survey as _pando_api_survey_v1_Survey, Survey__Output as _pando_api_survey_v1_Survey__Output } from '../../../../pando/api/survey/v1/Survey';
import type { FieldMask as _google_protobuf_FieldMask, FieldMask__Output as _google_protobuf_FieldMask__Output } from '../../../../google/protobuf/FieldMask';

export interface UpdateSurveyRequest {
  'survey'?: (_pando_api_survey_v1_Survey | null);
  'updateMask'?: (_google_protobuf_FieldMask | null);
}

export interface UpdateSurveyRequest__Output {
  'survey'?: (_pando_api_survey_v1_Survey__Output);
  'updateMask'?: (_google_protobuf_FieldMask__Output);
}
