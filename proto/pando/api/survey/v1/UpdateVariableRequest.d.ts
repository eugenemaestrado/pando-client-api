import type { Variable as _pando_api_survey_v1_Variable, Variable__Output as _pando_api_survey_v1_Variable__Output } from '../../../../pando/api/survey/v1/Variable';
import type { FieldMask as _google_protobuf_FieldMask, FieldMask__Output as _google_protobuf_FieldMask__Output } from '../../../../google/protobuf/FieldMask';
export interface UpdateVariableRequest {
    'variable'?: (_pando_api_survey_v1_Variable | null);
    'updateMask'?: (_google_protobuf_FieldMask | null);
}
export interface UpdateVariableRequest__Output {
    'variable'?: (_pando_api_survey_v1_Variable__Output);
    'updateMask'?: (_google_protobuf_FieldMask__Output);
}
