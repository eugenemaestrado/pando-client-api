export interface UploadAnswerMediaResponse {
    'mediaAnswerGuid'?: (string);
}
export interface UploadAnswerMediaResponse__Output {
    'mediaAnswerGuid'?: (string);
}
