// Original file: proto/pando/api/survey/v1/survey.proto


export interface UploadAnswerMediaResponse {
  'mediaAnswerGuid'?: (string);
}

export interface UploadAnswerMediaResponse__Output {
  'mediaAnswerGuid'?: (string);
}
