export interface UploadSurveyMediaResponse {
    'surveyMediaGuid'?: (string);
}
export interface UploadSurveyMediaResponse__Output {
    'surveyMediaGuid'?: (string);
}
