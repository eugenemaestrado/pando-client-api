// Original file: proto/pando/api/survey/v1/admin.proto


export interface UploadSurveyMediaResponse {
  'surveyMediaGuid'?: (string);
}

export interface UploadSurveyMediaResponse__Output {
  'surveyMediaGuid'?: (string);
}
