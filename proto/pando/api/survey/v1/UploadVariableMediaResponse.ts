// Original file: proto/pando/api/survey/v1/admin.proto


export interface UploadVariableMediaResponse {
  'variableMediaGuid'?: (string);
}

export interface UploadVariableMediaResponse__Output {
  'variableMediaGuid'?: (string);
}
