import type { VariableType as _pando_api_VariableType } from '../../../../pando/api/VariableType';
import type { MaskType as _pando_api_MaskType } from '../../../../pando/api/MaskType';
export interface Variable {
    'variableType'?: (_pando_api_VariableType | keyof typeof _pando_api_VariableType);
    'maskType'?: (_pando_api_MaskType | keyof typeof _pando_api_MaskType);
    'organization_Code'?: (string);
    'name'?: (string);
    'guid'?: (string);
    'isDeleted'?: (boolean);
}
export interface Variable__Output {
    'variableType'?: (_pando_api_VariableType);
    'maskType'?: (_pando_api_MaskType);
    'organization_Code'?: (string);
    'name'?: (string);
    'guid'?: (string);
    'isDeleted'?: (boolean);
}
