import type { Variable as _pando_api_survey_v1_Variable, Variable__Output as _pando_api_survey_v1_Variable__Output } from '../../../../pando/api/survey/v1/Variable';
import type { PaginationResult as _pando_api_PaginationResult, PaginationResult__Output as _pando_api_PaginationResult__Output } from '../../../../pando/api/PaginationResult';
export interface VariableList {
    'variables'?: (_pando_api_survey_v1_Variable)[];
    'pagination'?: (_pando_api_PaginationResult | null);
}
export interface VariableList__Output {
    'variables'?: (_pando_api_survey_v1_Variable__Output)[];
    'pagination'?: (_pando_api_PaginationResult__Output);
}
