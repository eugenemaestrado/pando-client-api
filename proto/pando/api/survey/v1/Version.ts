// Original file: proto/pando/api/survey/v1/shared.proto

import type { Timestamp as _google_protobuf_Timestamp, Timestamp__Output as _google_protobuf_Timestamp__Output } from '../../../../google/protobuf/Timestamp';

export interface Version {
  'guid'?: (string);
  'datePublished'?: (_google_protobuf_Timestamp | null);
}

export interface Version__Output {
  'guid'?: (string);
  'datePublished'?: (_google_protobuf_Timestamp__Output);
}
