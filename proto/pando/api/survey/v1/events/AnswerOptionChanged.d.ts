import type { AnswerOption as _pando_api_survey_v1_AnswerOption, AnswerOption__Output as _pando_api_survey_v1_AnswerOption__Output } from '../../../../../pando/api/survey/v1/AnswerOption';
import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';
export interface AnswerOptionChanged {
    'answerOptionId'?: (string);
    'oldModel'?: (_pando_api_survey_v1_AnswerOption | null);
    'newModel'?: (_pando_api_survey_v1_AnswerOption | null);
    'userData'?: (_pando_api_UserMetadata | null);
}
export interface AnswerOptionChanged__Output {
    'answerOptionId'?: (string);
    'oldModel'?: (_pando_api_survey_v1_AnswerOption__Output);
    'newModel'?: (_pando_api_survey_v1_AnswerOption__Output);
    'userData'?: (_pando_api_UserMetadata__Output);
}
