import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';
export interface InstanceESignSignatoryCreated {
    'instanceId'?: (string);
    'signatoryId'?: (string);
    'userData'?: (_pando_api_UserMetadata | null);
}
export interface InstanceESignSignatoryCreated__Output {
    'instanceId'?: (string);
    'signatoryId'?: (string);
    'userData'?: (_pando_api_UserMetadata__Output);
}
