import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';
export interface InstanceOwnerCreated {
    'instanceId'?: (string);
    'userIds'?: (string)[];
    'clientIds'?: (string)[];
    'userData'?: (_pando_api_UserMetadata | null);
}
export interface InstanceOwnerCreated__Output {
    'instanceId'?: (string);
    'userIds'?: (string)[];
    'clientIds'?: (string)[];
    'userData'?: (_pando_api_UserMetadata__Output);
}
