import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';
export interface InstanceOwnerDeleted {
    'instanceId'?: (string);
    'userIds'?: (string)[];
    'clientIds'?: (string)[];
    'userData'?: (_pando_api_UserMetadata | null);
}
export interface InstanceOwnerDeleted__Output {
    'instanceId'?: (string);
    'userIds'?: (string)[];
    'clientIds'?: (string)[];
    'userData'?: (_pando_api_UserMetadata__Output);
}
