import type { PlaybackData as _pando_api_survey_v1_PlaybackData, PlaybackData__Output as _pando_api_survey_v1_PlaybackData__Output } from '../../../../../pando/api/survey/v1/PlaybackData';
import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';
export interface PlaybackDataChanged {
    'playbackDataId'?: (string);
    'questionId'?: (string);
    'oldModel'?: (_pando_api_survey_v1_PlaybackData | null);
    'newModel'?: (_pando_api_survey_v1_PlaybackData | null);
    'userData'?: (_pando_api_UserMetadata | null);
}
export interface PlaybackDataChanged__Output {
    'playbackDataId'?: (string);
    'questionId'?: (string);
    'oldModel'?: (_pando_api_survey_v1_PlaybackData__Output);
    'newModel'?: (_pando_api_survey_v1_PlaybackData__Output);
    'userData'?: (_pando_api_UserMetadata__Output);
}
