import type { ContactType as _pando_api_ContactType } from '../../../../../pando/api/ContactType';
import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';
export interface SendSurveyRequest {
    'userId'?: (string);
    'instanceGuid'?: (string);
    'contactType'?: (_pando_api_ContactType | keyof typeof _pando_api_ContactType);
    'contactInfo'?: (string);
    'contactOverridden'?: (boolean);
    'requestedBy'?: (_pando_api_UserMetadata | null);
}
export interface SendSurveyRequest__Output {
    'userId'?: (string);
    'instanceGuid'?: (string);
    'contactType'?: (_pando_api_ContactType);
    'contactInfo'?: (string);
    'contactOverridden'?: (boolean);
    'requestedBy'?: (_pando_api_UserMetadata__Output);
}
