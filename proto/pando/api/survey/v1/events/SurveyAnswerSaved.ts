// Original file: proto/pando/api/survey/v1/events.proto

import type { AnswerType as _pando_api_survey_v1_AnswerType } from '../../../../../pando/api/survey/v1/AnswerType';
import type { AnswerAction as _pando_api_survey_v1_AnswerAction } from '../../../../../pando/api/survey/v1/AnswerAction';
import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';

export interface SurveyAnswerSaved {
  'answerId'?: (string);
  'answerText'?: (string);
  'answerType'?: (_pando_api_survey_v1_AnswerType | keyof typeof _pando_api_survey_v1_AnswerType);
  'answerAction'?: (_pando_api_survey_v1_AnswerAction | keyof typeof _pando_api_survey_v1_AnswerAction);
  'userData'?: (_pando_api_UserMetadata | null);
}

export interface SurveyAnswerSaved__Output {
  'answerId'?: (string);
  'answerText'?: (string);
  'answerType'?: (_pando_api_survey_v1_AnswerType);
  'answerAction'?: (_pando_api_survey_v1_AnswerAction);
  'userData'?: (_pando_api_UserMetadata__Output);
}
