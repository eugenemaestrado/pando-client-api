// Original file: proto/pando/api/survey/v1/events.proto

import type { Survey as _pando_api_survey_v1_Survey, Survey__Output as _pando_api_survey_v1_Survey__Output } from '../../../../../pando/api/survey/v1/Survey';
import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';

export interface SurveyChanged {
  'surveyId'?: (string);
  'oldModel'?: (_pando_api_survey_v1_Survey | null);
  'newModel'?: (_pando_api_survey_v1_Survey | null);
  'userData'?: (_pando_api_UserMetadata | null);
}

export interface SurveyChanged__Output {
  'surveyId'?: (string);
  'oldModel'?: (_pando_api_survey_v1_Survey__Output);
  'newModel'?: (_pando_api_survey_v1_Survey__Output);
  'userData'?: (_pando_api_UserMetadata__Output);
}
