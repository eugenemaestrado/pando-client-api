import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';
export interface SurveyCreated {
    'surveyId'?: (string);
    'userData'?: (_pando_api_UserMetadata | null);
}
export interface SurveyCreated__Output {
    'surveyId'?: (string);
    'userData'?: (_pando_api_UserMetadata__Output);
}
