// Original file: proto/pando/api/survey/v1/events.proto

import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';

export interface SurveyMediaUploaded {
  'surveyId'?: (string);
  'mediaId'?: (string);
  'userData'?: (_pando_api_UserMetadata | null);
}

export interface SurveyMediaUploaded__Output {
  'surveyId'?: (string);
  'mediaId'?: (string);
  'userData'?: (_pando_api_UserMetadata__Output);
}
