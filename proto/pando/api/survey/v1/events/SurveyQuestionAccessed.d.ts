import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';
export interface SurveyQuestionAccessed {
    'questionId'?: (string);
    'userData'?: (_pando_api_UserMetadata | null);
}
export interface SurveyQuestionAccessed__Output {
    'questionId'?: (string);
    'userData'?: (_pando_api_UserMetadata__Output);
}
