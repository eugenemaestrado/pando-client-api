import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';
export interface SurveyQuestionDestinationDeleted {
    'surveyQuestionDestinationId'?: (string);
    'userData'?: (_pando_api_UserMetadata | null);
}
export interface SurveyQuestionDestinationDeleted__Output {
    'surveyQuestionDestinationId'?: (string);
    'userData'?: (_pando_api_UserMetadata__Output);
}
