import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';
export interface SurveyVariableDeleted {
    'variableId'?: (string);
    'surveyId'?: (string);
    'userData'?: (_pando_api_UserMetadata | null);
}
export interface SurveyVariableDeleted__Output {
    'variableId'?: (string);
    'surveyId'?: (string);
    'userData'?: (_pando_api_UserMetadata__Output);
}
