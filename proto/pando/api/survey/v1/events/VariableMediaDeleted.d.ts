import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';
export interface VariableMediaDeleted {
    'variableId'?: (string);
    'mediaId'?: (string);
    'variableValue'?: (string);
    'userData'?: (_pando_api_UserMetadata | null);
}
export interface VariableMediaDeleted__Output {
    'variableId'?: (string);
    'mediaId'?: (string);
    'variableValue'?: (string);
    'userData'?: (_pando_api_UserMetadata__Output);
}
