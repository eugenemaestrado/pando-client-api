import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';
export interface VariableRetrieved {
    'variableId'?: (string);
    'userData'?: (_pando_api_UserMetadata | null);
}
export interface VariableRetrieved__Output {
    'variableId'?: (string);
    'userData'?: (_pando_api_UserMetadata__Output);
}
