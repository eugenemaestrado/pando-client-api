import type * as grpc from '@grpc/grpc-js';
import type { EnumTypeDefinition, MessageTypeDefinition } from '@grpc/proto-loader';

import type { PreviewServiceClient as _pando_api_survey_v1_PreviewServiceClient, PreviewServiceDefinition as _pando_api_survey_v1_PreviewServiceDefinition } from './pando/api/survey/v1/PreviewService';

type SubtypeConstructor<Constructor extends new (...args: any) => any, Subtype> = {
  new(...args: ConstructorParameters<Constructor>): Subtype;
};

export interface ProtoGrpcType {
  google: {
    api: {
      CustomHttpPattern: MessageTypeDefinition
      Http: MessageTypeDefinition
      HttpRule: MessageTypeDefinition
    }
    protobuf: {
      DescriptorProto: MessageTypeDefinition
      EnumDescriptorProto: MessageTypeDefinition
      EnumOptions: MessageTypeDefinition
      EnumValueDescriptorProto: MessageTypeDefinition
      EnumValueOptions: MessageTypeDefinition
      FieldDescriptorProto: MessageTypeDefinition
      FieldOptions: MessageTypeDefinition
      FileDescriptorProto: MessageTypeDefinition
      FileDescriptorSet: MessageTypeDefinition
      FileOptions: MessageTypeDefinition
      GeneratedCodeInfo: MessageTypeDefinition
      MessageOptions: MessageTypeDefinition
      MethodDescriptorProto: MessageTypeDefinition
      MethodOptions: MessageTypeDefinition
      OneofDescriptorProto: MessageTypeDefinition
      OneofOptions: MessageTypeDefinition
      ServiceDescriptorProto: MessageTypeDefinition
      ServiceOptions: MessageTypeDefinition
      SourceCodeInfo: MessageTypeDefinition
      Timestamp: MessageTypeDefinition
      UninterpretedOption: MessageTypeDefinition
    }
  }
  pando: {
    api: {
      Chunk: MessageTypeDefinition
      ClaimMetadata: MessageTypeDefinition
      ContactInfo: MessageTypeDefinition
      ContactType: EnumTypeDefinition
      DataRecord: MessageTypeDefinition
      DecimalValue: MessageTypeDefinition
      Empty: MessageTypeDefinition
      FileChunk: MessageTypeDefinition
      FileMetadata: MessageTypeDefinition
      FileType: EnumTypeDefinition
      MaskType: EnumTypeDefinition
      MessageMetadata: MessageTypeDefinition
      PaginationResult: MessageTypeDefinition
      ScopeMetadata: MessageTypeDefinition
      ServiceMetadata: MessageTypeDefinition
      UserContactInfo: MessageTypeDefinition
      UserMetadata: MessageTypeDefinition
      VariableType: EnumTypeDefinition
      survey: {
        v1: {
          Action: EnumTypeDefinition
          Answer: MessageTypeDefinition
          AnswerAction: EnumTypeDefinition
          AnswerMediaMetadata: MessageTypeDefinition
          AnswerMediaUploadRequest: MessageTypeDefinition
          AnswerOption: MessageTypeDefinition
          AnswerOptionList: MessageTypeDefinition
          AnswerTimestamp: MessageTypeDefinition
          AnswerType: EnumTypeDefinition
          Attempt: MessageTypeDefinition
          AttemptList: MessageTypeDefinition
          CompletionType: EnumTypeDefinition
          GetNextQuestionPreviewRequest: MessageTypeDefinition
          GetNextQuestionPreviewResponse: MessageTypeDefinition
          Instance: MessageTypeDefinition
          InstanceList: MessageTypeDefinition
          Media: MessageTypeDefinition
          MediaInfo: MessageTypeDefinition
          MediaList: MessageTypeDefinition
          MediaLocation: EnumTypeDefinition
          PlaybackData: MessageTypeDefinition
          PlaybackDataList: MessageTypeDefinition
          PreviewService: SubtypeConstructor<typeof grpc.Client, _pando_api_survey_v1_PreviewServiceClient> & { service: _pando_api_survey_v1_PreviewServiceDefinition }
          Question: MessageTypeDefinition
          QuestionWithPlaybackData: MessageTypeDefinition
          SelectionType: EnumTypeDefinition
          Status: EnumTypeDefinition
          Survey: MessageTypeDefinition
          SurveyDetail: MessageTypeDefinition
          SurveyDetailList: MessageTypeDefinition
          SurveyDocumentList: MessageTypeDefinition
          SurveyList: MessageTypeDefinition
          SurveyMediaMetadata: MessageTypeDefinition
          SurveyQuestionDestination: MessageTypeDefinition
          SurveyQuestionDestinationList: MessageTypeDefinition
          SurveyQuestionList: MessageTypeDefinition
          Variable: MessageTypeDefinition
          VariableList: MessageTypeDefinition
          Version: MessageTypeDefinition
        }
      }
    }
  }
}

