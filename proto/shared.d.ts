import type { EnumTypeDefinition, MessageTypeDefinition } from '@grpc/proto-loader';
export interface ProtoGrpcType {
    pando: {
        api: {
            Chunk: MessageTypeDefinition;
            ContactInfo: MessageTypeDefinition;
            ContactType: EnumTypeDefinition;
            DataRecord: MessageTypeDefinition;
            DecimalValue: MessageTypeDefinition;
            Empty: MessageTypeDefinition;
            FileChunk: MessageTypeDefinition;
            FileMetadata: MessageTypeDefinition;
            FileType: EnumTypeDefinition;
            MaskType: EnumTypeDefinition;
            PaginationResult: MessageTypeDefinition;
            UserContactInfo: MessageTypeDefinition;
            UserMetadata: MessageTypeDefinition;
            VariableType: EnumTypeDefinition;
        };
    };
}
