import express from "express";
import dotenv from "dotenv";
import cors from "cors";

import { startSocketServer } from "./socket";

dotenv.config();
const env = process.env;

// (() => {
//   const app = express();
//   app.set("trust proxy", true);
//   app.use(express.urlencoded({ extended: false }));
//   app.use(express.json());

//   const server = app.listen(env.NODE_PORT, () =>
//     console.log(`server is running on PORT:${env.NODE_PORT}`)
//   );
//   const io = intializeSocketServer(server);
//   console.log("IO:", io);
// })();

export const startServer = () => {
  const app = express();
  app.set("trust proxy", true);
  app.use(express.urlencoded({ extended: false }));
  app.use(express.json());

  const server = app.listen(env.NODE_PORT, () =>
    console.log(`server is running on PORT:${env.NODE_PORT}`)
  );
  // const io = startSocketServer(server);
  // console.log("IO:", io);
  return server && server;
  // const io = intializeSocketServer(server);
  // console.log("IO:", io);
};
