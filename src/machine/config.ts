import { AnyStateNodeDefinition, MachineConfig } from "xstate";
import { IContext, IMachineEvents } from "./types";
const config: MachineConfig<IContext, AnyStateNodeDefinition, IMachineEvents> =
  {
    id: "pano-client-api",
    initial: "grpc_client",
    states: {
      grpc_client: {
        id: "grpc-client",
        invoke: {
          id: "wait-for-grpc-client",
          src: "waitForGRPCclient",
        },
        on: {
          GRPC_CLIENT_READY: {
            actions: ["setGRPCclientReadyToTrue"],
            target: "express_server",
          },
          GRPC_CLIENT_ERROR: {},
        },
      },
      express_server: {
        id: "express-server",
        invoke: {
          id: "intialize-express-server",
          src: "initializeExpressServer",
        },
        on: {
          EXPRESS_SERVER_INITIALIZED: {
            actions: ["assignExpressServer"],
            target: "socket_server",
          },
        },
      },
      socket_server: {
        id: "socket-server",
        invoke: {
          id: "initialize-socket-connection",
          src: "initializeSocketConnection",
        },
        on: {
          SOCKET_CONNECTION_INITIALIZED: {
            actions: [
              "assignSocketConnection",
              (e) => console.log("startSocketServer", e.socket!.id),
            ],
          },
        },
      },
    },
  };

export default config;
