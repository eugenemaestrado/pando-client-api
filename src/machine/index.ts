import { createMachine, interpret } from "xstate";
import { IContext } from "./types";
import config from "./config";
import options from "./options";

import { fields } from "./data";

const default_context: IContext = {
  server: "",
  is_grpc_client_ready: false,
};

export const spawn = (context: Partial<IContext> = {}) => {
  const machine_config = {
    ...config,
    context: {
      ...default_context,
      ...context,
    },
  };
  return createMachine(machine_config, options);
};

(() => {
  const machine = spawn();
  const service = interpret(machine)
    .onTransition((state: any): any =>
      console.log("state interpret", state.value)
    )
    .start();
})();

// export const Interpret = (context: Partial<IContext>) => {
//   const machine = spawn(context);
//   const service = interpret(machine).onTransition((state: any): any =>
//     console.log("state interpret", state.value)
//   );
//   service.start();
//   service.send("ON_FIELD_UPDATE");
//   return service;
// };
// Interpret({});

export * from "./types";
