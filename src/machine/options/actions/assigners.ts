/* eslint-disable indent */
/* eslint-disable arrow-body-style */
import { Socket } from "socket.io";
import { ActionFunctionMap, assign, AnyEventObject } from "xstate";
import { IContext } from "../../types";

const actions: ActionFunctionMap<IContext, any> = {
  assignExpressServer: assign({
    server: (_, { payload }) => payload,
  }),
  assignSocketConnection: assign({
    socket: (_, { payload }) => payload,
  }),
  setGRPCclientReadyToTrue: assign({
    is_grpc_client_ready: (_) => true,
  }),
};

export default actions;
