import { ConditionPredicate } from "xstate";
import { IContext } from "../types";
import { IRecord } from "../types";

const guards: IRecord<ConditionPredicate<IContext, any>> = {
  isGRPCclientReady: ({ is_grpc_client_ready }) => !!is_grpc_client_ready,
};

export default guards;
