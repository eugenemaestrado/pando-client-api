import { ServiceConfig } from "xstate";
import { IContext, IMachineEvents } from "../../types";
import { IRecord } from "../../types";
import { Socket } from "socket.io";

import { startServer } from "../../../app";
import { startSocketServer } from "../../../socket";

import { client, getSurveys } from "../../../survey/client";
import { deadline } from "../../../utils";
import { send } from "xstate/lib/actionTypes";
import { Server } from "@grpc/grpc-js";

import { listenToSocketEvents } from "../../../socket";

const services: IRecord<ServiceConfig<IContext, IMachineEvents>> = {
  initializeExpressServer: () => (send) => {
    const server = startServer();
    server &&
      send({
        type: "EXPRESS_SERVER_INITIALIZED",
        payload: server,
      });
  },
  initializeSocketConnection:
    ({ server, is_grpc_client_ready }) =>
    (send) => {
      server &&
        startSocketServer(server, is_grpc_client_ready, (socket: Socket) => {
          send({
            type: "SOCKET_CONNECTION_INITIALIZED",
            payload: socket,
          });
        });
    },
  waitForGRPCclient: () => (send) => {
    console.log("WAITING FOR GRPC CLIENT");
    client.waitForReady(deadline, (error) => {
      if (error) {
        console.log("!!!!!! ====== CLIENT ERROR:", error);
        send({
          type: "GRPC_CLIENT_ERROR",
        });
        return;
      }
      console.log("<<<<<< ====== GRPC CLIENT RUNNING ====== >>>>>>");
      send({
        type: "GRPC_CLIENT_READY",
      });
    });
  },
};

export default services;
