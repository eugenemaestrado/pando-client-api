import { Socket } from "socket.io";
import { Express } from "express";

export interface IContext {
  server?: Express | any;
  socket?: Socket;
  is_grpc_client_ready: boolean;
}

export interface IRecord<TEntry> {
  [key: string]: TEntry;
}
