import { Socket } from "socket.io";

export interface IExpressServerInitialized {
  type: "EXPRESS_SERVER_INITIALIZED";
  payload: any;
}
export interface ISocketConnectionInitialized {
  type: "SOCKET_CONNECTION_INITIALIZED";
  payload: Socket;
}

export interface IClientDisconnected {
  type: "CLIENT_DISCONNECTED";
}

export interface IGRPCclientReady {
  type: "GRPC_CLIENT_READY";
}

export interface IGRPCclientError {
  type: "GRPC_CLIENT_ERROR";
}

export type IMachineEvents =
  | IExpressServerInitialized
  | ISocketConnectionInitialized
  | IClientDisconnected
  | IGRPCclientReady
  | IGRPCclientError;
