const dotenv = require("dotenv");
const io = require("socket.io-client");

dotenv.config();
const env = process.env;

console.log("NODE PORT: ", env.NODE_PORT);
const socket = io(`http://localhost:${process.env.NODE_PORT}`, {
  transports: ["websocket"],
  forceNew: true,
});

socket.on("chatFromServer", (name) => {
  console.log("YOOOOO:", name);
});

socket.emit("chatFromBrowser", {
  message: "hey yooo",
});

socket.emit("survey", {
  message: "getSurveys",
  params: {
    organizationCode: "alliance",
    resultsPerPage: 2,
  },
});

socket.on("surveyResult", (surveys) => {
  console.log("SURVEY RESULTS:", surveys);
});

socket.on("disconnect", () => {
  console.log("server disconnected");
});
