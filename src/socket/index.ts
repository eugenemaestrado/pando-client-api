import { Server, Socket } from "socket.io";
import { getSurveys } from "../survey/client";

export const startSocketServer = (
  server: any,
  is_grpc_client_ready: boolean,
  callback: Function
) => {
  const io = server && new Server(server, {});
  io.on("connection", (socket: Socket) => {
    console.log(`USER ${socket.id} is online`);
    callback(socket);

    socket.emit("chatFromServer", {
      message: "testing",
    });

    socket.on("survey", ({ message, params }: any) => {
      message === "getSurveys" &&
        getSurveys({
          ...params,
        })
          .then(
            (result) =>
              result !== undefined && socket.emit("surveyResult", result)
          )
          .catch((error) => console.log("GETTING SURVEYS ERROR:", error));
    });
    socket.on("disconnect", () => {
      console.log("yooo client DISCONNECTED");
    });
  });
};

export const listenToSocketEvents = (
  socket: Socket,
  action: any,
  send: any
) => {
  socket.on("survey", ({ message, params }: any) => {
    message === "getSurveys" &&
      getSurveys({
        ...params,
      });
  });
  socket.on("disconnect", () => {
    send({
      type: "CLIENT_DISCONNECTED",
    });
    console.log("yooo client DISCONNECTED");
  });
};
