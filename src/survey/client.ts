import path from "path";
import * as grpc from "@grpc/grpc-js";
import * as protoLoader from "@grpc/proto-loader";
//@ts-ignore
import { ProtoGrpcType } from "../../proto/admin";
import { GetSurveysRequest } from "../../proto/pando/api/survey/v1/GetSurveysRequest";
import dotenv from "dotenv";

dotenv.config();

const env = process.env;
const PROTO_FILE = "../../proto/pando/api/survey/v1/admin.proto";
const packageDef = protoLoader.loadSync(path.resolve(__dirname, PROTO_FILE));
const grpcObj = grpc.loadPackageDefinition(
  packageDef
) as unknown as ProtoGrpcType;

// const rootCert = fs.readFileSync("../../cert/ca-cert.pem");

const surveyPackage = grpcObj.pando.api.survey.v1;
const channelCreds = grpc.credentials.createSsl();
const metaCallback = (_params: any, callback: any) => {
  const meta = new grpc.Metadata();
  meta.add(`authorization`, `Bearer ${env.TOKEN}`);
  return callback(null, meta);
};
const callCreds = grpc.credentials.createFromMetadataGenerator(metaCallback);
const combCreds = grpc.credentials.combineChannelCredentials(
  channelCreds,
  callCreds
);

export const client = new surveyPackage.SurveyAdminService(
  `${env.HOST}:${env.PORT}`,
  combCreds
);

export const getSurveys = (params: GetSurveysRequest) =>
  new Promise((resolve: any, reject: any) => {
    client.getSurveys(params, (err, result) => {
      if (err) {
        console.log("GETTING SURVEYS ERROR:", err);
        reject(err);
        return;
      }
      result && resolve(result);
    });
  });

export const onClientReady = () => {};
